﻿using MT.GUPAS.Infrastructure;
using MT.GUPAS.Models;
using MT.GUPAS.Pages;
using MT.GUPAS.Reporting;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Telerik.Reporting;
using Telerik.Reporting.Processing;
using Telerik.WinControls;
using Telerik.WinControls.Themes;
using Telerik.WinControls.UI;

namespace MT.GUPAS
{
    public partial class Main : Telerik.WinControls.UI.RadForm
    {

        [DllImport("User32.dll")]
        private static extern bool GetLastInputInfo(ref LASTINPUTINFO plii);

        [DllImport("Kernel32.dll")]
        private static extern uint GetLastError();

        private RocheDataEntities rocheDBContext;
        private SystemM system = null;
        private List<ProtoModel> rocheProto;
        private List<ListModel> rocheList;

        private bool invoked = false;
        private static System.Timers.Timer aTimer;
        internal struct LASTINPUTINFO
        {
            public uint cbSize;

            public uint dwTime;
        }

        string mySqlQueryWlist = @" SELECT Komp.Name as KName, Komp.ArtNr, Komp.GenesysNr, Kabine.Name AS RName, AKopf.Besteller, [User].Name, AKopf.ANr, AKopf.ZeitNew, ALot.Lot, AKopf.Nummer as KNummer, ALot.Nummer as LNummer, ADetail.Soll, ADetail.Nummer as DNummer, AKopf.BArt
                                    FROM ADetail ADetail   
                                    INNER JOIN ALot ALot ON ADetail.Lot=ALot.Nummer 
                                    INNER JOIN AKopf AKopf ON ALot.Auftrag=AKopf.Nummer
                                    INNER JOIN Komp Komp ON AKopf.MG=Komp.Nummer
                                    INNER JOIN [User] [User] ON AKopf.Ersteller=[User].Nummer
                                    INNER JOIN Kabine Kabine ON AKopf.Kabine=Kabine.Nummer
 
                                    WHERE  ALot.Nummer = @ALotNr
                                    ORDER BY ADetail.Nummer";

        string mySqlQueryWProto = @"SELECT Komp.Name as KName, Komp.ArtNr, Komp.GenesysNr, Kabine.Name as RName, AKopf.Besteller, AKopf.ANr, AKopf.ZeitNew, ALot.Lot, ALot.Nummer as LNummer, [User].Tag, Waagen.Name As WName, ADetail.VonEti, ADetail.BisEti, ADetail.Status, ADetail.Nummer as DNummer, AKopf.BArt, ADetail.Soll, ADetail.Netto, ADetail.Tara, ADetail.Brutto, ADetail.Zeit, ADetail.Hand, ADetail.Tara2, Waagen.Precision, ADetail.ZielLot, Waagen.Unit, AKopf.AnzZeilen, [User_1].Name
                                    FROM   [User][User_1]
                                    INNER JOIN Kabine Kabine
                                    INNER JOIN Waagen Waagen
                                    INNER JOIN ADetail ADetail ON Waagen.Nummer= ADetail.Waage  ON Kabine.Nummer= ADetail.Kabine
                                    INNER JOIN ALot ALot ON ADetail.Lot= ALot.Nummer
                                    INNER JOIN [User][User] ON ADetail.Verwieger=[User].Nummer
                                    INNER JOIN AKopf AKopf ON ALot.Auftrag= AKopf.Nummer ON[User_1].Nummer= AKopf.Ersteller
                                    INNER JOIN Komp Komp ON AKopf.MG= Komp.Nummer
                                    WHERE ALot.Nummer= @ALotNr
                                    ORDER BY ADetail.Zeit";

        string mySqlAKopfNeuList = @"SELECT AKopf.Nummer, AKopf.ANr, Komp.Name, AKopf.BArt, AKopf.Besteller, AKopf.AnzZeilenTotal                    
                                     FROM AKopf
                                     INNER JOIN Komp ON AKopf.MG = Komp.Nummer
						             WHERE AnzZeilen = AnzZeilenTotal";

        string mySqlAKopfAltList = @"SELECT TOP(@Anzahl)  AKopf.Nummer, AKopf.ANr, Komp.Name, AKopf.Besteller,Akopf.Komplett, AKopf.AnzZeilenTotal,AKopf.AnzZeilen, AKopf.AnzPrint
                                    FROM AKopf
                                    INNER JOIN Komp ON AKopf.MG = Komp.Nummer
                                    WHERE AnzZeilen <> AnzZeilenTotal
                                    ORDER BY ZeitNew DESC";

        public Main()
        {
            InitializeComponent();
            rocheDBContext = new RocheDataEntities();
        }

        private void radMenuItem5_Click(object sender, EventArgs e)
        {
            AboutBox ab = new AboutBox();
            ab.ShowDialog();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            //rocheDBContext = new RocheDataEntities();

            var uf = new LoginBox(false);
            DialogResult result = uf.ShowDialog();
            if (result == DialogResult.Cancel)
                Application.Exit();
            else
            {
                if (Globals.User.Level < 3)
                {
                    MessageBox.Show("Kein Zugriff erlaubt!", "Anmeldung", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    Application.Exit();
                }
                else
                {
                    ManageAuditTail.WriteAuditTrial(AuditKlasse.Applikation, AuditAction.Starten, "Login", Globals.User.Name);
                    mnuTabellen.Enabled = Globals.User.Level > 3;
                    mnuEtiDrucker.Enabled = Globals.User.Level > 3;
                    Text = $"DSM Nutritional Products - { Globals.User.Name.Trim()} - Level: {Globals.User.Level}";
                    aTimer = new System.Timers.Timer(2000);
                    aTimer.Elapsed += ATimer_Elapsed;
                    aTimer.AutoReset = true;
                    aTimer.Enabled = true;
                }
            }

        }

        private void ATimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            uint lastAction = GetIdleTime();
            Debug.WriteLine(lastAction);
            if ((lastAction > Globals.LogoutTime) && (invoked == false))
            {
                invoked = true;
                this.Invoke(new Action(() =>
                {
                    var uf = new LoginBox(true);  // same User
                    DialogResult result = uf.ShowDialog();
                    if (result == DialogResult.Cancel)
                        Application.Exit();

                    invoked = false;
                }
                ));
            }
        }

        private void ReloadGridViewNew()
        {
            rdGridNeu.DataSource = null;
            rdGridNeu.DataSource = rocheDBContext.Database.SqlQuery<KopfNeuModel>(mySqlAKopfNeuList).ToList();

            foreach (GridViewColumn column in rdGridNeu.Columns)
            {
                column.AutoSizeMode = BestFitColumnMode.AllCells;
                column.BestFit();
                column.TextAlignment = ContentAlignment.MiddleLeft;
            }
            rdGridNeu.ClearSelection();
        }

        private void ReloadGridViewOld()
        {


            rdGridAlt.DataSource = null;
            rdGridAlt.DataSource = rocheDBContext.Database.SqlQuery<KopfAltModel>(mySqlAKopfAltList, new SqlParameter("@Anzahl", 25)).ToList();

            foreach (GridViewColumn column in rdGridAlt.Columns)
            {
                column.AutoSizeMode = BestFitColumnMode.AllCells;
                column.BestFit();
                column.TextAlignment = ContentAlignment.MiddleLeft;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Office2019GrayTheme theme = new Office2019GrayTheme();
            ThemeResolutionService.ApplicationThemeName = "Office2019Gray";
            ReloadGridViewOld();
            ReloadGridViewNew();
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            ManageAuditTail.WriteAuditTrial(AuditKlasse.Applikation, AuditAction.Beenden, "Normales Beenden", "");
            aTimer.Enabled = false;
            Close();
        }

        private void mnuQuit_Click(object sender, EventArgs e)
        {
            ManageAuditTail.WriteAuditTrial(AuditKlasse.Applikation, AuditAction.Beenden, "Normales Beenden", "");
            aTimer.Enabled = false;
            Close();
        }

        public static uint GetIdleTime()
        {
            LASTINPUTINFO lastInPut = new LASTINPUTINFO();
            lastInPut.cbSize = (uint)System.Runtime.InteropServices.Marshal.SizeOf(lastInPut);
            GetLastInputInfo(ref lastInPut);

            return ((uint)Environment.TickCount - lastInPut.dwTime);
        }

        #region event

        private void btNewOrder_Click(object sender, EventArgs e)
        {

            cbEtikette uf = new cbEtikette();
            uf.ShowDialog();
            ReloadGridViewOld();
            ReloadGridViewNew();
        }

        private void mnuUser_Click(object sender, EventArgs e)
        {
            UserForm uf = new UserForm();
            uf.ShowDialog();
        }

        private void mnuSperre_Click(object sender, EventArgs e)
        {
            SperrForm sf = new SperrForm();
            sf.ShowDialog();
        }

        private void mnuAArt_Click(object sender, EventArgs e)
        {
            AArtForm uf = new AArtForm();
            uf.ShowDialog();
        }

        private void mnuArtikel_Click(object sender, EventArgs e)
        {
            ArtikelForm uf = new ArtikelForm();
            uf.ShowDialog();
        }

        private void mnuToleranzen_Click(object sender, EventArgs e)
        {
            TolForm uf = new TolForm();
            uf.ShowDialog();
        }

        private void mnuAudit_Click(object sender, EventArgs e)
        {
            AuditForm uf = new AuditForm();
            uf.ShowDialog();

        }

        private void mnuJournal_Click(object sender, EventArgs e)
        {
            JournalForm uf = new JournalForm();
            uf.ShowDialog();
            ReloadGridViewOld();
            ReloadGridViewNew();
        }

        private void btOverview_Click(object sender, EventArgs e)
        {
            OrderOverview uf = new OrderOverview();
            uf.ShowDialog();
            ReloadGridViewOld();
            ReloadGridViewNew();
        }
        #endregion

        private void button5_Click(object sender, EventArgs e)
        {
            if (rdGridAlt.Rows.Count > 0)
            {
                KopfAltModel current = rdGridAlt.CurrentRow.DataBoundItem as KopfAltModel;
                if (current != null)
                {
                    List<ALot> lot = rocheDBContext.ALots.Where(d => d.Auftrag == current.Nummer).ToList();
                    foreach (var item in lot)
                    {
                        InstanceReportSource source = new InstanceReportSource();
                        PrinterSettings printerSettings = new PrinterSettings();
                        WProto report = new WProto();
                        rocheProto = rocheDBContext.Database.SqlQuery<ProtoModel>(mySqlQueryWProto, new SqlParameter("@ALotNr", item.Nummer)).ToList(); ;

                        report.DataSource = rocheProto;
                        source.ReportDocument = report;
                        ReportProcessor rp = new ReportProcessor();
#if !DEBUG
                        rp.PrintReport(source, printerSettings);
#else
                        var hugo = new ReportViewer(source);
                        hugo.Text = "Journal";
                        hugo.ShowDialog();
#endif
                        ManageAuditTail.WriteAuditTrial(AuditKlasse.Protokoll, AuditAction.Ausdrucken, $"Lot# {item.Nummer}", "");
                    }

                    AKopf kopf = rocheDBContext.AKopfs.FirstOrDefault(d => d.Nummer == current.Nummer);
                    kopf.AnzPrint++;
                    rocheDBContext.SaveChanges();

                }
            }
        }


        private void btPrintOrder_Click(object sender, EventArgs e)
        {
            if (rdGridNeu.Rows.Count > 0)
            {
                KopfNeuModel current = rdGridNeu.CurrentRow.DataBoundItem as KopfNeuModel;

                if (current != null)
                {
                    List<ALot> lot = rocheDBContext.ALots.Where(d => d.Auftrag == current.Nummer).ToList();
                    foreach (var item in lot)
                    {
                        PrintWlist(item.Nummer);
                    }
                }
            }
        }

        private void rdGridAlt_RowFormatting(object sender, RowFormattingEventArgs e)
        {

            if ((bool)e.RowElement.RowInfo.Cells["Komplett"].Value == true)
            {
                if ((int)e.RowElement.RowInfo.Cells["AnzPrint"].Value == 0)
                    e.RowElement.BackColor = Color.LightBlue;
                else
                    e.RowElement.BackColor = Color.LightGreen;
            }
            else
            {
                e.RowElement.BackColor = Color.LightPink;
            }
            e.RowElement.DrawFill = true;
        }

        private void btDeleteOrder_Click(object sender, EventArgs e)
        {
            KopfNeuModel current = rdGridNeu.CurrentRow.DataBoundItem as KopfNeuModel;

            if (current != null)
            {
                DialogResult res = MessageBox.Show(this, "Wollen Sie diesen Eintrag löschen?", "Löschen", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (res == DialogResult.Yes)
                {
                    AKopf kopf = rocheDBContext.AKopfs.Find(current.Nummer);
                    List<ALot> lot = rocheDBContext.ALots.Where(d => d.Auftrag == kopf.Nummer).ToList();
                    foreach (var item in lot)
                    {
                        List<ADetail> detail = rocheDBContext.ADetails.Where(d => d.Lot == item.Nummer).ToList();
                        foreach (var item2 in detail)
                        {
                            List<Gebinde> gebinde = rocheDBContext.Gebindes.Where(d => d.Detail == item2.Nummer).ToList();
                            foreach (var item3 in gebinde)
                            {
                                rocheDBContext.Gebindes.Remove(item3);
                            }
                            rocheDBContext.ADetails.Remove(item2);
                        }
                        rocheDBContext.ALots.Remove(item);
                    }
                    rocheDBContext.AKopfs.Remove(kopf);
                    rocheDBContext.SaveChanges();
                    ManageAuditTail.WriteAuditTrial(AuditKlasse.Auftrag, AuditAction.Loeschen, $"Auftrag# {kopf.Nummer}", "");

                    ReloadGridViewNew();
                }
            }
        }

        public void PrintWlist(int lotNr)
        {
            InstanceReportSource source = new InstanceReportSource();
            PrinterSettings printerSettings = new PrinterSettings();

            WList report = new WList();
            rocheList = rocheDBContext.Database.SqlQuery<ListModel>(mySqlQueryWlist, new SqlParameter("@ALotNr", lotNr)).ToList(); ;

            report.DataSource = rocheList;
            source.ReportDocument = report;
#if !DEBUG
            ReportProcessor rp = new ReportProcessor();
            rp.PrintReport(source, printerSettings);
#else
            var hugo = new ReportViewer(source);
            hugo.Text = "Journal";
            hugo.ShowDialog();
#endif
            ManageAuditTail.WriteAuditTrial(AuditKlasse.Auftrag, AuditAction.Ausdrucken, $"Lot# {lotNr}", "");

        }

        private void mnuEtikette_Click(object sender, EventArgs e)
        {
            if (system != null)
                rocheDBContext.Entry(system).Reload();
            else
                system = rocheDBContext.SystemMs.First();
                
            Komp current = rocheDBContext.Komps.FirstOrDefault(d => d.Nummer == system.Komp);
            if (current != null)
            {
                var box = new InputBox(current);
                box.ShowDialog();
            }
            
        }

        private void radMenuItem1_Click(object sender, EventArgs e)
        {
            string ipAddress;
            IPAddress iPAddress;
            SystemM system = rocheDBContext.SystemMs.First();

            InputBoxS usf = new InputBoxS("IP oder DNS Name", "Etikettendrucker", system.TLPName.Trim(), false);
            if (usf.ShowDialog() == DialogResult.OK)
            {
                ipAddress = Convert.ToString(usf.Tag).ToUpper();
                if (IPAddress.TryParse(ipAddress, out iPAddress) == true)
                {
                    system.TLPName = ipAddress;
                }
                else
                {
                    try
                    {
                        var entry = Dns.GetHostEntry(ipAddress);
                        ipAddress = entry.AddressList[0].ToString();
                    }
                    catch
                    {
                        MessageBox.Show($"IP-Adresse falsch oder DNS-Eintrag nicht gefunden!");
                    }

                    system.TLPName = ipAddress;
                }
                rocheDBContext.SaveChanges();
            }
        }

        private void btActualNew_Click(object sender, EventArgs e)
        {
            ReloadGridViewNew();
            ReloadGridViewOld();
        }

        private void btActualOld_Click(object sender, EventArgs e)
        {
            ReloadGridViewNew();
            ReloadGridViewOld();
        }
    }
}

