﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.Reporting;

namespace MT.GUPAS.Reporting
{
    public partial class ReportViewer : Form
    {
        public ReportViewer(ReportSource report)
        {
            InitializeComponent();
            reportViewer1.ReportSource = report;
            reportViewer1.RefreshReport();
        }

        private void ReportViewer_Load(object sender, EventArgs e)
        {
            this.reportViewer1.RefreshReport();
        }
    }
}
