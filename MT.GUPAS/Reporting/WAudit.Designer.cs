namespace MT.GUPAS.Reporting
{
    partial class WAudit
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WAudit));
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.ReportHeaderArea1 = new Telerik.Reporting.ReportHeaderSection();
            this.PageHeaderArea1 = new Telerik.Reporting.PageHeaderSection();
            this.Text1 = new Telerik.Reporting.TextBox();
            this.Text2 = new Telerik.Reporting.TextBox();
            this.Druckzeit1 = new Telerik.Reporting.TextBox();
            this.Druckdatum1 = new Telerik.Reporting.TextBox();
            this.Text5 = new Telerik.Reporting.TextBox();
            this.Text6 = new Telerik.Reporting.TextBox();
            this.Text7 = new Telerik.Reporting.TextBox();
            this.Text8 = new Telerik.Reporting.TextBox();
            this.Text3 = new Telerik.Reporting.TextBox();
            this.Text4 = new Telerik.Reporting.TextBox();
            this.Line1 = new Telerik.Reporting.Shape();
            this.Text9 = new Telerik.Reporting.TextBox();
            this.Ersteller2 = new Telerik.Reporting.TextBox();
            this.Picture1 = new Telerik.Reporting.PictureBox();
            this.DetailArea1 = new Telerik.Reporting.DetailSection();
            this.Zeit1 = new Telerik.Reporting.TextBox();
            this.Ersteller1 = new Telerik.Reporting.TextBox();
            this.Para21 = new Telerik.Reporting.TextBox();
            this.Klasse1 = new Telerik.Reporting.TextBox();
            this.Aktion1 = new Telerik.Reporting.TextBox();
            this.Para1 = new Telerik.Reporting.TextBox();
            this.ReportFooterArea1 = new Telerik.Reporting.ReportFooterSection();
            this.PageFooterArea1 = new Telerik.Reporting.PageFooterSection();
            this.SeiteNvonM1 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // ReportHeaderArea1
            // 
            this.ReportHeaderArea1.Height = Telerik.Reporting.Drawing.Unit.Cm(0.4D);
            this.ReportHeaderArea1.KeepTogether = false;
            this.ReportHeaderArea1.Name = "ReportHeaderArea1";
            this.ReportHeaderArea1.PageBreak = Telerik.Reporting.PageBreak.Before;
            this.ReportHeaderArea1.Style.BackgroundColor = System.Drawing.Color.White;
            this.ReportHeaderArea1.Style.Visible = true;
            // 
            // PageHeaderArea1
            // 
            this.PageHeaderArea1.Height = Telerik.Reporting.Drawing.Unit.Cm(2.6D);
            this.PageHeaderArea1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.Text1,
            this.Text2,
            this.Druckzeit1,
            this.Druckdatum1,
            this.Text5,
            this.Text6,
            this.Text7,
            this.Text8,
            this.Text3,
            this.Text4,
            this.Line1,
            this.Text9,
            this.Ersteller2,
            this.Picture1});
            this.PageHeaderArea1.Name = "PageHeaderArea1";
            this.PageHeaderArea1.PrintOnFirstPage = true;
            this.PageHeaderArea1.PrintOnLastPage = true;
            this.PageHeaderArea1.Style.BackgroundColor = System.Drawing.Color.White;
            this.PageHeaderArea1.Style.Visible = true;
            // 
            // Text1
            // 
            this.Text1.CanGrow = false;
            this.Text1.CanShrink = false;
            this.Text1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.686D), Telerik.Reporting.Drawing.Unit.Cm(0.212D));
            this.Text1.Name = "Text1";
            this.Text1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.715D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Text1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text1.Style.Color = System.Drawing.Color.Black;
            this.Text1.Style.Font.Bold = true;
            this.Text1.Style.Font.Italic = false;
            this.Text1.Style.Font.Name = "Arial";
            this.Text1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.Text1.Style.Font.Strikeout = false;
            this.Text1.Style.Font.Underline = true;
            this.Text1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text1.Style.Visible = true;
            this.Text1.Value = "DSM-DNP AG Sisseln \n";
            // 
            // Text2
            // 
            this.Text2.CanGrow = false;
            this.Text2.CanShrink = false;
            this.Text2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.686D), Telerik.Reporting.Drawing.Unit.Cm(1.111D));
            this.Text2.Name = "Text2";
            this.Text2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.214D), Telerik.Reporting.Drawing.Unit.Cm(0.423D));
            this.Text2.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text2.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text2.Style.Color = System.Drawing.Color.Black;
            this.Text2.Style.Font.Bold = false;
            this.Text2.Style.Font.Italic = false;
            this.Text2.Style.Font.Name = "Arial";
            this.Text2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text2.Style.Font.Strikeout = false;
            this.Text2.Style.Font.Underline = false;
            this.Text2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text2.Style.Visible = true;
            this.Text2.Value = "Ausdruck vom\n\n";
            // 
            // Druckzeit1
            // 
            this.Druckzeit1.CanGrow = false;
            this.Druckzeit1.CanShrink = false;
            this.Druckzeit1.Format = "{0:T}";
            this.Druckzeit1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.8D), Telerik.Reporting.Drawing.Unit.Cm(1.121D));
            this.Druckzeit1.Name = "Druckzeit1";
            this.Druckzeit1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.905D), Telerik.Reporting.Drawing.Unit.Cm(0.487D));
            this.Druckzeit1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Druckzeit1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Druckzeit1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Druckzeit1.Style.Color = System.Drawing.Color.Black;
            this.Druckzeit1.Style.Font.Bold = false;
            this.Druckzeit1.Style.Font.Italic = false;
            this.Druckzeit1.Style.Font.Name = "Arial";
            this.Druckzeit1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Druckzeit1.Style.Font.Strikeout = false;
            this.Druckzeit1.Style.Font.Underline = false;
            this.Druckzeit1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Druckzeit1.Style.Visible = true;
            this.Druckzeit1.Value = "=Now()";
            // 
            // Druckdatum1
            // 
            this.Druckdatum1.CanGrow = false;
            this.Druckdatum1.CanShrink = false;
            this.Druckdatum1.Format = "{0:d}";
            this.Druckdatum1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.175D), Telerik.Reporting.Drawing.Unit.Cm(1.111D));
            this.Druckdatum1.Name = "Druckdatum1";
            this.Druckdatum1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.328D), Telerik.Reporting.Drawing.Unit.Cm(0.487D));
            this.Druckdatum1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Druckdatum1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Druckdatum1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Druckdatum1.Style.Color = System.Drawing.Color.Black;
            this.Druckdatum1.Style.Font.Bold = false;
            this.Druckdatum1.Style.Font.Italic = false;
            this.Druckdatum1.Style.Font.Name = "Arial";
            this.Druckdatum1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Druckdatum1.Style.Font.Strikeout = false;
            this.Druckdatum1.Style.Font.Underline = false;
            this.Druckdatum1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Druckdatum1.Style.Visible = true;
            this.Druckdatum1.Value = "=Now()";
            // 
            // Text5
            // 
            this.Text5.CanGrow = false;
            this.Text5.CanShrink = false;
            this.Text5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.5D), Telerik.Reporting.Drawing.Unit.Cm(1.888D));
            this.Text5.Name = "Text5";
            this.Text5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.992D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Text5.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text5.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text5.Style.Color = System.Drawing.Color.Black;
            this.Text5.Style.Font.Bold = false;
            this.Text5.Style.Font.Italic = false;
            this.Text5.Style.Font.Name = "Arial";
            this.Text5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text5.Style.Font.Strikeout = false;
            this.Text5.Style.Font.Underline = true;
            this.Text5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text5.Style.Visible = true;
            this.Text5.Value = "Zeit";
            // 
            // Text6
            // 
            this.Text6.CanGrow = false;
            this.Text6.CanShrink = false;
            this.Text6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.7D), Telerik.Reporting.Drawing.Unit.Cm(1.888D));
            this.Text6.Name = "Text6";
            this.Text6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.358D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Text6.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text6.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text6.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text6.Style.Color = System.Drawing.Color.Black;
            this.Text6.Style.Font.Bold = false;
            this.Text6.Style.Font.Italic = false;
            this.Text6.Style.Font.Name = "Arial";
            this.Text6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text6.Style.Font.Strikeout = false;
            this.Text6.Style.Font.Underline = true;
            this.Text6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text6.Style.Visible = true;
            this.Text6.Value = "Ersteller";
            // 
            // Text7
            // 
            this.Text7.CanGrow = false;
            this.Text7.CanShrink = false;
            this.Text7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.9D), Telerik.Reporting.Drawing.Unit.Cm(1.888D));
            this.Text7.Name = "Text7";
            this.Text7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.505D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Text7.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text7.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text7.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text7.Style.Color = System.Drawing.Color.Black;
            this.Text7.Style.Font.Bold = false;
            this.Text7.Style.Font.Italic = false;
            this.Text7.Style.Font.Name = "Arial";
            this.Text7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text7.Style.Font.Strikeout = false;
            this.Text7.Style.Font.Underline = true;
            this.Text7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text7.Style.Visible = true;
            this.Text7.Value = "Parameter";
            // 
            // Text8
            // 
            this.Text8.CanGrow = false;
            this.Text8.CanShrink = false;
            this.Text8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.9D), Telerik.Reporting.Drawing.Unit.Cm(1.888D));
            this.Text8.Name = "Text8";
            this.Text8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Text8.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text8.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text8.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text8.Style.Color = System.Drawing.Color.Black;
            this.Text8.Style.Font.Bold = false;
            this.Text8.Style.Font.Italic = false;
            this.Text8.Style.Font.Name = "Arial";
            this.Text8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text8.Style.Font.Strikeout = false;
            this.Text8.Style.Font.Underline = true;
            this.Text8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text8.Style.Visible = true;
            this.Text8.Value = "Parameter 2";
            // 
            // Text3
            // 
            this.Text3.CanGrow = false;
            this.Text3.CanShrink = false;
            this.Text3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.686D), Telerik.Reporting.Drawing.Unit.Cm(1.888D));
            this.Text3.Name = "Text3";
            this.Text3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.214D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Text3.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text3.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text3.Style.Color = System.Drawing.Color.Black;
            this.Text3.Style.Font.Bold = false;
            this.Text3.Style.Font.Italic = false;
            this.Text3.Style.Font.Name = "Arial";
            this.Text3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text3.Style.Font.Strikeout = false;
            this.Text3.Style.Font.Underline = true;
            this.Text3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text3.Style.Visible = true;
            this.Text3.Value = "Klasse";
            // 
            // Text4
            // 
            this.Text4.CanGrow = false;
            this.Text4.CanShrink = false;
            this.Text4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.175D), Telerik.Reporting.Drawing.Unit.Cm(1.888D));
            this.Text4.Name = "Text4";
            this.Text4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.825D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Text4.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text4.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text4.Style.Color = System.Drawing.Color.Black;
            this.Text4.Style.Font.Bold = false;
            this.Text4.Style.Font.Italic = false;
            this.Text4.Style.Font.Name = "Arial";
            this.Text4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text4.Style.Font.Strikeout = false;
            this.Text4.Style.Font.Underline = true;
            this.Text4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text4.Style.Visible = true;
            this.Text4.Value = "Aktion";
            // 
            // Line1
            // 
            this.Line1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.686D), Telerik.Reporting.Drawing.Unit.Cm(2.468D));
            this.Line1.Name = "Line1";
            this.Line1.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            this.Line1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(27.067D), Telerik.Reporting.Drawing.Unit.Point(3.75D));
            this.Line1.Style.Color = System.Drawing.Color.Black;
            this.Line1.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.Line1.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.Line1.Style.Visible = true;
            // 
            // Text9
            // 
            this.Text9.CanGrow = false;
            this.Text9.CanShrink = false;
            this.Text9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.147D), Telerik.Reporting.Drawing.Unit.Cm(1.111D));
            this.Text9.Name = "Text9";
            this.Text9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.345D), Telerik.Reporting.Drawing.Unit.Cm(0.497D));
            this.Text9.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text9.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text9.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text9.Style.Color = System.Drawing.Color.Black;
            this.Text9.Style.Font.Bold = false;
            this.Text9.Style.Font.Italic = false;
            this.Text9.Style.Font.Name = "Arial";
            this.Text9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text9.Style.Font.Strikeout = false;
            this.Text9.Style.Font.Underline = false;
            this.Text9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text9.Style.Visible = true;
            this.Text9.Value = "durch ";
            // 
            // Ersteller2
            // 
            this.Ersteller2.CanGrow = false;
            this.Ersteller2.CanShrink = false;
            this.Ersteller2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.111D));
            this.Ersteller2.Name = "Ersteller2";
            this.Ersteller2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.327D), Telerik.Reporting.Drawing.Unit.Cm(0.582D));
            this.Ersteller2.Style.BackgroundColor = System.Drawing.Color.White;
            this.Ersteller2.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Ersteller2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Ersteller2.Style.Color = System.Drawing.Color.Black;
            this.Ersteller2.Style.Font.Bold = false;
            this.Ersteller2.Style.Font.Italic = false;
            this.Ersteller2.Style.Font.Name = "Arial";
            this.Ersteller2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Ersteller2.Style.Font.Strikeout = false;
            this.Ersteller2.Style.Font.Underline = false;
            this.Ersteller2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Ersteller2.Style.Visible = true;
            this.Ersteller2.Value = "=Parameters.[Ersteller]";
            // 
            // Picture1
            // 
            this.Picture1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(22.2D), Telerik.Reporting.Drawing.Unit.Cm(0.017D));
            this.Picture1.MimeType = "image/jpeg";
            this.Picture1.Name = "Picture1";
            this.Picture1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.4D), Telerik.Reporting.Drawing.Unit.Cm(2.261D));
            this.Picture1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.Stretch;
            this.Picture1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Picture1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Picture1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Picture1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Picture1.Style.Visible = true;
            this.Picture1.Value = ((object)(resources.GetObject("Picture1.Value")));
            // 
            // DetailArea1
            // 
            this.DetailArea1.Height = Telerik.Reporting.Drawing.Unit.Cm(0.635D);
            this.DetailArea1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.Zeit1,
            this.Ersteller1,
            this.Para21,
            this.Klasse1,
            this.Aktion1,
            this.Para1});
            this.DetailArea1.KeepTogether = false;
            this.DetailArea1.Name = "DetailArea1";
            this.DetailArea1.PageBreak = Telerik.Reporting.PageBreak.None;
            this.DetailArea1.Style.BackgroundColor = System.Drawing.Color.White;
            this.DetailArea1.Style.Visible = true;
            // 
            // Zeit1
            // 
            this.Zeit1.CanGrow = false;
            this.Zeit1.CanShrink = false;
            this.Zeit1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.5D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.Zeit1.Name = "Zeit1";
            this.Zeit1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2D), Telerik.Reporting.Drawing.Unit.Cm(0.487D));
            this.Zeit1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Zeit1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Zeit1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Zeit1.Style.Color = System.Drawing.Color.Black;
            this.Zeit1.Style.Font.Bold = false;
            this.Zeit1.Style.Font.Italic = false;
            this.Zeit1.Style.Font.Name = "Arial";
            this.Zeit1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Zeit1.Style.Font.Strikeout = false;
            this.Zeit1.Style.Font.Underline = false;
            this.Zeit1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Zeit1.Style.Visible = true;
            this.Zeit1.Value = "=Fields.[Zeit]";
            // 
            // Ersteller1
            // 
            this.Ersteller1.CanGrow = false;
            this.Ersteller1.CanShrink = false;
            this.Ersteller1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.7D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.Ersteller1.Name = "Ersteller1";
            this.Ersteller1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.911D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Ersteller1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Ersteller1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Ersteller1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Ersteller1.Style.Color = System.Drawing.Color.Black;
            this.Ersteller1.Style.Font.Bold = false;
            this.Ersteller1.Style.Font.Italic = false;
            this.Ersteller1.Style.Font.Name = "Arial";
            this.Ersteller1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Ersteller1.Style.Font.Strikeout = false;
            this.Ersteller1.Style.Font.Underline = false;
            this.Ersteller1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Ersteller1.Style.Visible = true;
            this.Ersteller1.Value = "=Fields.[Ersteller]";
            // 
            // Para21
            // 
            this.Para21.CanGrow = false;
            this.Para21.CanShrink = false;
            this.Para21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.9D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.Para21.Name = "Para21";
            this.Para21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.853D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Para21.Style.BackgroundColor = System.Drawing.Color.White;
            this.Para21.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Para21.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Para21.Style.Color = System.Drawing.Color.Black;
            this.Para21.Style.Font.Bold = false;
            this.Para21.Style.Font.Italic = false;
            this.Para21.Style.Font.Name = "Arial";
            this.Para21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Para21.Style.Font.Strikeout = false;
            this.Para21.Style.Font.Underline = false;
            this.Para21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Para21.Style.Visible = true;
            this.Para21.Value = "=Fields.[Para2]";
            // 
            // Klasse1
            // 
            this.Klasse1.CanGrow = false;
            this.Klasse1.CanShrink = false;
            this.Klasse1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.686D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.Klasse1.Name = "Klasse1";
            this.Klasse1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.214D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Klasse1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Klasse1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Klasse1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Klasse1.Style.Color = System.Drawing.Color.Black;
            this.Klasse1.Style.Font.Bold = false;
            this.Klasse1.Style.Font.Italic = false;
            this.Klasse1.Style.Font.Name = "Arial";
            this.Klasse1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Klasse1.Style.Font.Strikeout = false;
            this.Klasse1.Style.Font.Underline = false;
            this.Klasse1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Klasse1.Style.Visible = true;
            this.Klasse1.Value = "=Fields.[Klasse]";
            // 
            // Aktion1
            // 
            this.Aktion1.CanGrow = false;
            this.Aktion1.CanShrink = false;
            this.Aktion1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.175D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.Aktion1.Name = "Aktion1";
            this.Aktion1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.053D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Aktion1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Aktion1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Aktion1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Aktion1.Style.Color = System.Drawing.Color.Black;
            this.Aktion1.Style.Font.Bold = false;
            this.Aktion1.Style.Font.Italic = false;
            this.Aktion1.Style.Font.Name = "Arial";
            this.Aktion1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Aktion1.Style.Font.Strikeout = false;
            this.Aktion1.Style.Font.Underline = false;
            this.Aktion1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Aktion1.Style.Visible = true;
            this.Aktion1.Value = "=Fields.[Aktion]";
            // 
            // Para1
            // 
            this.Para1.CanGrow = false;
            this.Para1.CanShrink = false;
            this.Para1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.9D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.Para1.Name = "Para1";
            this.Para1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.505D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Para1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Para1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Para1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Para1.Style.Color = System.Drawing.Color.Black;
            this.Para1.Style.Font.Bold = false;
            this.Para1.Style.Font.Italic = false;
            this.Para1.Style.Font.Name = "Arial";
            this.Para1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Para1.Style.Font.Strikeout = false;
            this.Para1.Style.Font.Underline = false;
            this.Para1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Para1.Style.Visible = true;
            this.Para1.Value = "=Fields.[Para]";
            // 
            // ReportFooterArea1
            // 
            this.ReportFooterArea1.Height = Telerik.Reporting.Drawing.Unit.Cm(0.365D);
            this.ReportFooterArea1.KeepTogether = false;
            this.ReportFooterArea1.Name = "ReportFooterArea1";
            this.ReportFooterArea1.PageBreak = Telerik.Reporting.PageBreak.After;
            this.ReportFooterArea1.Style.BackgroundColor = System.Drawing.Color.White;
            this.ReportFooterArea1.Style.Visible = true;
            // 
            // PageFooterArea1
            // 
            this.PageFooterArea1.Height = Telerik.Reporting.Drawing.Unit.Cm(1.058D);
            this.PageFooterArea1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.SeiteNvonM1,
            this.textBox3,
            this.textBox2});
            this.PageFooterArea1.Name = "PageFooterArea1";
            this.PageFooterArea1.PrintOnFirstPage = true;
            this.PageFooterArea1.PrintOnLastPage = true;
            this.PageFooterArea1.Style.BackgroundColor = System.Drawing.Color.White;
            this.PageFooterArea1.Style.Visible = true;
            // 
            // SeiteNvonM1
            // 
            this.SeiteNvonM1.CanGrow = false;
            this.SeiteNvonM1.CanShrink = false;
            this.SeiteNvonM1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.SeiteNvonM1.Name = "SeiteNvonM1";
            this.SeiteNvonM1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.505D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.SeiteNvonM1.Style.BackgroundColor = System.Drawing.Color.White;
            this.SeiteNvonM1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.SeiteNvonM1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.SeiteNvonM1.Style.Color = System.Drawing.Color.Black;
            this.SeiteNvonM1.Style.Font.Bold = false;
            this.SeiteNvonM1.Style.Font.Italic = false;
            this.SeiteNvonM1.Style.Font.Name = "Arial";
            this.SeiteNvonM1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.SeiteNvonM1.Style.Font.Strikeout = false;
            this.SeiteNvonM1.Style.Font.Underline = false;
            this.SeiteNvonM1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.SeiteNvonM1.Style.Visible = true;
            this.SeiteNvonM1.Value = "=\"Seite \" + PageNumber + \" von \" + PageCount";
            // 
            // textBox3
            // 
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.6D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.638D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.textBox3.Value = "Created with Telerik Reports  3.44";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(27.24D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.61D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.textBox2.Value = "Ver . 1.0";
            // 
            // WAudit
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.ReportHeaderArea1,
            this.PageHeaderArea1,
            this.DetailArea1,
            this.ReportFooterArea1,
            this.PageFooterArea1});
            this.Name = "WTrail";
            this.PageSettings.Landscape = true;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Cm(0.423D), Telerik.Reporting.Drawing.Unit.Cm(0.427D), Telerik.Reporting.Drawing.Unit.Cm(0.423D), Telerik.Reporting.Drawing.Unit.Cm(0.427D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            reportParameter1.Name = "Ersteller";
            this.ReportParameters.Add(reportParameter1);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Cm(28.85D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.ReportHeaderSection ReportHeaderArea1;
        private Telerik.Reporting.PageHeaderSection PageHeaderArea1;
        private Telerik.Reporting.TextBox Text1;
        private Telerik.Reporting.TextBox Text2;
        private Telerik.Reporting.TextBox Druckzeit1;
        private Telerik.Reporting.TextBox Druckdatum1;
        private Telerik.Reporting.TextBox Text5;
        private Telerik.Reporting.TextBox Text6;
        private Telerik.Reporting.TextBox Text7;
        private Telerik.Reporting.TextBox Text8;
        private Telerik.Reporting.TextBox Text3;
        private Telerik.Reporting.TextBox Text4;
        private Telerik.Reporting.Shape Line1;
        private Telerik.Reporting.TextBox Text9;
        private Telerik.Reporting.TextBox Ersteller2;
        private Telerik.Reporting.PictureBox Picture1;
        private Telerik.Reporting.DetailSection DetailArea1;
        private Telerik.Reporting.TextBox Zeit1;
        private Telerik.Reporting.TextBox Ersteller1;
        private Telerik.Reporting.TextBox Para21;
        private Telerik.Reporting.TextBox Klasse1;
        private Telerik.Reporting.TextBox Aktion1;
        private Telerik.Reporting.TextBox Para1;
        private Telerik.Reporting.ReportFooterSection ReportFooterArea1;
        private Telerik.Reporting.PageFooterSection PageFooterArea1;
        private Telerik.Reporting.TextBox SeiteNvonM1;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox2;
    }
}