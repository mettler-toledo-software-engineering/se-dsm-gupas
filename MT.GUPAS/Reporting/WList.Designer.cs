namespace MT.GUPAS.Reporting
{
    partial class WList
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.Barcodes.Code39Encoder code39Encoder1 = new Telerik.Reporting.Barcodes.Code39Encoder();
            Telerik.Reporting.TypeReportSource typeReportSource1 = new Telerik.Reporting.TypeReportSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WList));
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.ReportHeaderArea1 = new Telerik.Reporting.ReportHeaderSection();
            this.DetailArea1 = new Telerik.Reporting.DetailSection();
            this.Datensatznummer1 = new Telerik.Reporting.TextBox();
            this.Text21 = new Telerik.Reporting.TextBox();
            this.Soll1 = new Telerik.Reporting.TextBox();
            this.Nummer2 = new Telerik.Reporting.TextBox();
            this.barcode1 = new Telerik.Reporting.Barcode();
            this.Line1 = new Telerik.Reporting.Shape();
            this.subReport1 = new Telerik.Reporting.SubReport();
            this.ReportFooterArea1 = new Telerik.Reporting.ReportFooterSection();
            this.Text11 = new Telerik.Reporting.TextBox();
            this.Text16 = new Telerik.Reporting.TextBox();
            this.Text17 = new Telerik.Reporting.TextBox();
            this.Text18 = new Telerik.Reporting.TextBox();
            this.Text19 = new Telerik.Reporting.TextBox();
            this.Line2 = new Telerik.Reporting.Shape();
            this.PageFooterArea1 = new Telerik.Reporting.PageFooterSection();
            this.SeiteNvonM1 = new Telerik.Reporting.TextBox();
            this.PageHeaderArea1 = new Telerik.Reporting.PageHeaderSection();
            this.Text2 = new Telerik.Reporting.TextBox();
            this.Text1 = new Telerik.Reporting.TextBox();
            this.Text3 = new Telerik.Reporting.TextBox();
            this.Text4 = new Telerik.Reporting.TextBox();
            this.Text5 = new Telerik.Reporting.TextBox();
            this.Text6 = new Telerik.Reporting.TextBox();
            this.Text7 = new Telerik.Reporting.TextBox();
            this.Text8 = new Telerik.Reporting.TextBox();
            this.Name1 = new Telerik.Reporting.TextBox();
            this.ArtNr1 = new Telerik.Reporting.TextBox();
            this.GenesysNr1 = new Telerik.Reporting.TextBox();
            this.Text9 = new Telerik.Reporting.TextBox();
            this.Text10 = new Telerik.Reporting.TextBox();
            this.Text14 = new Telerik.Reporting.TextBox();
            this.Text15 = new Telerik.Reporting.TextBox();
            this.Name2 = new Telerik.Reporting.TextBox();
            this.Besteller1 = new Telerik.Reporting.TextBox();
            this.Name3 = new Telerik.Reporting.TextBox();
            this.Text20 = new Telerik.Reporting.TextBox();
            this.ANr1 = new Telerik.Reporting.TextBox();
            this.ZeitNew1 = new Telerik.Reporting.TextBox();
            this.Lot1 = new Telerik.Reporting.TextBox();
            this.Text22 = new Telerik.Reporting.TextBox();
            this.Nummer1 = new Telerik.Reporting.TextBox();
            this.Text25 = new Telerik.Reporting.TextBox();
            this.Text12 = new Telerik.Reporting.TextBox();
            this.Text13 = new Telerik.Reporting.TextBox();
            this.BArt1 = new Telerik.Reporting.TextBox();
            this.Picture1 = new Telerik.Reporting.PictureBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // ReportHeaderArea1
            // 
            this.ReportHeaderArea1.Height = Telerik.Reporting.Drawing.Unit.Cm(0.132D);
            this.ReportHeaderArea1.KeepTogether = false;
            this.ReportHeaderArea1.Name = "ReportHeaderArea1";
            this.ReportHeaderArea1.PageBreak = Telerik.Reporting.PageBreak.Before;
            this.ReportHeaderArea1.Style.BackgroundColor = System.Drawing.Color.White;
            this.ReportHeaderArea1.Style.Visible = true;
            // 
            // DetailArea1
            // 
            this.DetailArea1.Height = Telerik.Reporting.Drawing.Unit.Cm(1.599D);
            this.DetailArea1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.Datensatznummer1,
            this.Text21,
            this.Soll1,
            this.Nummer2,
            this.barcode1,
            this.Line1,
            this.subReport1});
            this.DetailArea1.KeepTogether = false;
            this.DetailArea1.Name = "DetailArea1";
            this.DetailArea1.PageBreak = Telerik.Reporting.PageBreak.None;
            this.DetailArea1.Style.BackgroundColor = System.Drawing.Color.White;
            this.DetailArea1.Style.Visible = true;
            // 
            // Datensatznummer1
            // 
            this.Datensatznummer1.CanGrow = false;
            this.Datensatznummer1.CanShrink = false;
            this.Datensatznummer1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.661D), Telerik.Reporting.Drawing.Unit.Cm(0.599D));
            this.Datensatznummer1.Name = "Datensatznummer1";
            this.Datensatznummer1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.058D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Datensatznummer1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Datensatznummer1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Datensatznummer1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Datensatznummer1.Style.Color = System.Drawing.Color.Black;
            this.Datensatznummer1.Style.Font.Bold = false;
            this.Datensatznummer1.Style.Font.Italic = false;
            this.Datensatznummer1.Style.Font.Name = "Arial";
            this.Datensatznummer1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Datensatznummer1.Style.Font.Strikeout = false;
            this.Datensatznummer1.Style.Font.Underline = false;
            this.Datensatznummer1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.Datensatznummer1.Style.Visible = true;
            this.Datensatznummer1.Value = "=RowNumber()";
            // 
            // Text21
            // 
            this.Text21.CanGrow = false;
            this.Text21.CanShrink = false;
            this.Text21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.17D), Telerik.Reporting.Drawing.Unit.Cm(0.599D));
            this.Text21.Name = "Text21";
            this.Text21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.635D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Text21.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text21.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text21.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text21.Style.Color = System.Drawing.Color.Black;
            this.Text21.Style.Font.Bold = false;
            this.Text21.Style.Font.Italic = false;
            this.Text21.Style.Font.Name = "Arial";
            this.Text21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text21.Style.Font.Strikeout = false;
            this.Text21.Style.Font.Underline = false;
            this.Text21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.Text21.Style.Visible = true;
            this.Text21.Value = "kg";
            // 
            // Soll1
            // 
            this.Soll1.CanGrow = false;
            this.Soll1.CanShrink = false;
            this.Soll1.Bindings.Add(new Telerik.Reporting.Binding("Format", "=\"{0:N3}\""));
            this.Soll1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.927D), Telerik.Reporting.Drawing.Unit.Cm(0.599D));
            this.Soll1.Name = "Soll1";
            this.Soll1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.977D), Telerik.Reporting.Drawing.Unit.Cm(0.487D));
            this.Soll1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Soll1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Soll1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Soll1.Style.Color = System.Drawing.Color.Black;
            this.Soll1.Style.Font.Bold = false;
            this.Soll1.Style.Font.Italic = false;
            this.Soll1.Style.Font.Name = "Arial";
            this.Soll1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Soll1.Style.Font.Strikeout = false;
            this.Soll1.Style.Font.Underline = false;
            this.Soll1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.Soll1.Style.Visible = true;
            this.Soll1.Value = "=Fields.[Soll]";
            // 
            // Nummer2
            // 
            this.Nummer2.CanGrow = false;
            this.Nummer2.CanShrink = false;
            this.Nummer2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.372D), Telerik.Reporting.Drawing.Unit.Cm(0.599D));
            this.Nummer2.Name = "Nummer2";
            this.Nummer2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.942D), Telerik.Reporting.Drawing.Unit.Cm(0.487D));
            this.Nummer2.Style.BackgroundColor = System.Drawing.Color.White;
            this.Nummer2.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Nummer2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Nummer2.Style.Color = System.Drawing.Color.Black;
            this.Nummer2.Style.Font.Bold = false;
            this.Nummer2.Style.Font.Italic = false;
            this.Nummer2.Style.Font.Name = "Arial";
            this.Nummer2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Nummer2.Style.Font.Strikeout = false;
            this.Nummer2.Style.Font.Underline = false;
            this.Nummer2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.Nummer2.Style.Visible = true;
            this.Nummer2.Value = "=Fields.[DNummer]";
            // 
            // barcode1
            // 
            this.barcode1.Checksum = false;
            code39Encoder1.ShowText = false;
            this.barcode1.Encoder = code39Encoder1;
            this.barcode1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.077D), Telerik.Reporting.Drawing.Unit.Cm(0.352D));
            this.barcode1.Name = "barcode1";
            this.barcode1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.barcode1.Value = "=Fields.DNummer";
            // 
            // Line1
            // 
            this.Line1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.847D), Telerik.Reporting.Drawing.Unit.Cm(0.071D));
            this.Line1.Name = "Line1";
            this.Line1.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            this.Line1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.96D), Telerik.Reporting.Drawing.Unit.Point(4D));
            this.Line1.Style.Color = System.Drawing.Color.Black;
            this.Line1.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.Line1.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.Line1.Style.Visible = true;
            // 
            // subReport1
            // 
            this.subReport1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.352D), Telerik.Reporting.Drawing.Unit.Cm(0.599D));
            this.subReport1.Name = "subReport1";
            typeReportSource1.Parameters.Add(new Telerik.Reporting.Parameter("DetailNummer", "=Fields.[DNummer]"));
            typeReportSource1.TypeName = "MT.GUPAS.Reporting.WSubList, MT.GUPAS, Version=1.0.0.0, Culture=neutral, PublicKe" +
    "yToken=null";
            this.subReport1.ReportSource = typeReportSource1;
            this.subReport1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.575D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            // 
            // ReportFooterArea1
            // 
            this.ReportFooterArea1.Height = Telerik.Reporting.Drawing.Unit.Cm(13.674D);
            this.ReportFooterArea1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.Text11,
            this.Text16,
            this.Text17,
            this.Text18,
            this.Text19,
            this.Line2});
            this.ReportFooterArea1.KeepTogether = false;
            this.ReportFooterArea1.Name = "ReportFooterArea1";
            this.ReportFooterArea1.PageBreak = Telerik.Reporting.PageBreak.After;
            this.ReportFooterArea1.Style.BackgroundColor = System.Drawing.Color.White;
            this.ReportFooterArea1.Style.Visible = true;
            // 
            // Text11
            // 
            this.Text11.CanGrow = false;
            this.Text11.CanShrink = false;
            this.Text11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.267D), Telerik.Reporting.Drawing.Unit.Cm(0.423D));
            this.Text11.Name = "Text11";
            this.Text11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.62D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Text11.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text11.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text11.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text11.Style.Color = System.Drawing.Color.Black;
            this.Text11.Style.Font.Bold = false;
            this.Text11.Style.Font.Italic = false;
            this.Text11.Style.Font.Name = "Arial";
            this.Text11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text11.Style.Font.Strikeout = false;
            this.Text11.Style.Font.Underline = false;
            this.Text11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text11.Style.Visible = true;
            this.Text11.Value = "Bereitstellungskontrolle erfolgt :";
            // 
            // Text16
            // 
            this.Text16.CanGrow = false;
            this.Text16.CanShrink = false;
            this.Text16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.635D), Telerik.Reporting.Drawing.Unit.Cm(1.27D));
            this.Text16.Name = "Text16";
            this.Text16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.832D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Text16.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text16.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text16.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text16.Style.Color = System.Drawing.Color.Black;
            this.Text16.Style.Font.Bold = false;
            this.Text16.Style.Font.Italic = false;
            this.Text16.Style.Font.Name = "Arial";
            this.Text16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text16.Style.Font.Strikeout = false;
            this.Text16.Style.Font.Underline = false;
            this.Text16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text16.Style.Visible = true;
            this.Text16.Value = "Datum:__________________________";
            // 
            // Text17
            // 
            this.Text17.CanGrow = false;
            this.Text17.CanShrink = false;
            this.Text17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.162D), Telerik.Reporting.Drawing.Unit.Cm(1.27D));
            this.Text17.Name = "Text17";
            this.Text17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.832D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Text17.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text17.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text17.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text17.Style.Color = System.Drawing.Color.Black;
            this.Text17.Style.Font.Bold = false;
            this.Text17.Style.Font.Italic = false;
            this.Text17.Style.Font.Name = "Arial";
            this.Text17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text17.Style.Font.Strikeout = false;
            this.Text17.Style.Font.Underline = false;
            this.Text17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text17.Style.Visible = true;
            this.Text17.Value = "Visum:__________________________";
            // 
            // Text18
            // 
            this.Text18.CanGrow = false;
            this.Text18.CanShrink = false;
            this.Text18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.635D), Telerik.Reporting.Drawing.Unit.Cm(2.54D));
            this.Text18.Name = "Text18";
            this.Text18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.832D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Text18.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text18.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text18.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text18.Style.Color = System.Drawing.Color.Black;
            this.Text18.Style.Font.Bold = false;
            this.Text18.Style.Font.Italic = false;
            this.Text18.Style.Font.Name = "Arial";
            this.Text18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text18.Style.Font.Strikeout = false;
            this.Text18.Style.Font.Underline = false;
            this.Text18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text18.Style.Visible = true;
            this.Text18.Value = "Datum:__________________________";
            // 
            // Text19
            // 
            this.Text19.CanGrow = false;
            this.Text19.CanShrink = false;
            this.Text19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.162D), Telerik.Reporting.Drawing.Unit.Cm(2.54D));
            this.Text19.Name = "Text19";
            this.Text19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.832D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Text19.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text19.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text19.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text19.Style.Color = System.Drawing.Color.Black;
            this.Text19.Style.Font.Bold = false;
            this.Text19.Style.Font.Italic = false;
            this.Text19.Style.Font.Name = "Arial";
            this.Text19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text19.Style.Font.Strikeout = false;
            this.Text19.Style.Font.Underline = false;
            this.Text19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text19.Style.Visible = true;
            this.Text19.Value = "Visum:__________________________";
            // 
            // Line2
            // 
            this.Line2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.847D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.Line2.Name = "Line2";
            this.Line2.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            this.Line2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.907D), Telerik.Reporting.Drawing.Unit.Point(3.75D));
            this.Line2.Style.Color = System.Drawing.Color.Black;
            this.Line2.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.Line2.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.Line2.Style.Visible = true;
            // 
            // PageFooterArea1
            // 
            this.PageFooterArea1.Height = Telerik.Reporting.Drawing.Unit.Cm(0.526D);
            this.PageFooterArea1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.SeiteNvonM1,
            this.textBox2,
            this.textBox1});
            this.PageFooterArea1.Name = "PageFooterArea1";
            this.PageFooterArea1.PrintOnFirstPage = true;
            this.PageFooterArea1.PrintOnLastPage = true;
            this.PageFooterArea1.Style.BackgroundColor = System.Drawing.Color.White;
            this.PageFooterArea1.Style.Visible = true;
            // 
            // SeiteNvonM1
            // 
            this.SeiteNvonM1.CanGrow = false;
            this.SeiteNvonM1.CanShrink = false;
            this.SeiteNvonM1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.SeiteNvonM1.Name = "SeiteNvonM1";
            this.SeiteNvonM1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.SeiteNvonM1.Style.BackgroundColor = System.Drawing.Color.White;
            this.SeiteNvonM1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.SeiteNvonM1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.SeiteNvonM1.Style.Color = System.Drawing.Color.Black;
            this.SeiteNvonM1.Style.Font.Bold = false;
            this.SeiteNvonM1.Style.Font.Italic = false;
            this.SeiteNvonM1.Style.Font.Name = "Arial";
            this.SeiteNvonM1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.SeiteNvonM1.Style.Font.Strikeout = false;
            this.SeiteNvonM1.Style.Font.Underline = false;
            this.SeiteNvonM1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.SeiteNvonM1.Style.Visible = true;
            this.SeiteNvonM1.Value = "=\"Seite \" + PageNumber + \" of \" + PageCount";
            // 
            // PageHeaderArea1
            // 
            this.PageHeaderArea1.Height = Telerik.Reporting.Drawing.Unit.Cm(6.369D);
            this.PageHeaderArea1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.Text2,
            this.Text1,
            this.Text3,
            this.Text4,
            this.Text5,
            this.Text6,
            this.Text7,
            this.Text8,
            this.Name1,
            this.ArtNr1,
            this.GenesysNr1,
            this.Text9,
            this.Text10,
            this.Text14,
            this.Text15,
            this.Name2,
            this.Besteller1,
            this.Name3,
            this.Text20,
            this.ANr1,
            this.ZeitNew1,
            this.Lot1,
            this.Text22,
            this.Nummer1,
            this.Text25,
            this.Text12,
            this.Text13,
            this.BArt1,
            this.Picture1});
            this.PageHeaderArea1.Name = "PageHeaderArea1";
            this.PageHeaderArea1.PrintOnFirstPage = true;
            this.PageHeaderArea1.PrintOnLastPage = true;
            this.PageHeaderArea1.Style.BackgroundColor = System.Drawing.Color.White;
            this.PageHeaderArea1.Style.Visible = true;
            // 
            // Text2
            // 
            this.Text2.CanGrow = false;
            this.Text2.CanShrink = false;
            this.Text2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.635D), Telerik.Reporting.Drawing.Unit.Cm(0.994D));
            this.Text2.Name = "Text2";
            this.Text2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.387D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Text2.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text2.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text2.Style.Color = System.Drawing.Color.Black;
            this.Text2.Style.Font.Bold = false;
            this.Text2.Style.Font.Italic = false;
            this.Text2.Style.Font.Name = "Arial";
            this.Text2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text2.Style.Font.Strikeout = false;
            this.Text2.Style.Font.Underline = false;
            this.Text2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text2.Style.Visible = true;
            this.Text2.Value = "Auftrags-Nr:";
            // 
            // Text1
            // 
            this.Text1.CanGrow = false;
            this.Text1.CanShrink = false;
            this.Text1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.635D), Telerik.Reporting.Drawing.Unit.Cm(0.212D));
            this.Text1.Name = "Text1";
            this.Text1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.043D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Text1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text1.Style.Color = System.Drawing.Color.Black;
            this.Text1.Style.Font.Bold = true;
            this.Text1.Style.Font.Italic = false;
            this.Text1.Style.Font.Name = "Arial";
            this.Text1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.Text1.Style.Font.Strikeout = false;
            this.Text1.Style.Font.Underline = true;
            this.Text1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text1.Style.Visible = true;
            this.Text1.Value = "DSM-DNP AG Sisseln \n";
            // 
            // Text3
            // 
            this.Text3.CanGrow = false;
            this.Text3.CanShrink = false;
            this.Text3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.661D), Telerik.Reporting.Drawing.Unit.Cm(1.693D));
            this.Text3.Name = "Text3";
            this.Text3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.387D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Text3.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text3.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text3.Style.Color = System.Drawing.Color.Black;
            this.Text3.Style.Font.Bold = false;
            this.Text3.Style.Font.Italic = false;
            this.Text3.Style.Font.Name = "Arial";
            this.Text3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text3.Style.Font.Strikeout = false;
            this.Text3.Style.Font.Underline = false;
            this.Text3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text3.Style.Visible = true;
            this.Text3.Value = "Auftragsart:";
            // 
            // Text4
            // 
            this.Text4.CanGrow = false;
            this.Text4.CanShrink = false;
            this.Text4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.162D), Telerik.Reporting.Drawing.Unit.Cm(2.328D));
            this.Text4.Name = "Text4";
            this.Text4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.905D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Text4.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text4.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text4.Style.Color = System.Drawing.Color.Black;
            this.Text4.Style.Font.Bold = false;
            this.Text4.Style.Font.Italic = false;
            this.Text4.Style.Font.Name = "Arial";
            this.Text4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text4.Style.Font.Strikeout = false;
            this.Text4.Style.Font.Underline = false;
            this.Text4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text4.Style.Visible = true;
            this.Text4.Value = "Besteller";
            // 
            // Text5
            // 
            this.Text5.CanGrow = false;
            this.Text5.CanShrink = false;
            this.Text5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.162D), Telerik.Reporting.Drawing.Unit.Cm(3.598D));
            this.Text5.Name = "Text5";
            this.Text5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.905D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Text5.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text5.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text5.Style.Color = System.Drawing.Color.Black;
            this.Text5.Style.Font.Bold = false;
            this.Text5.Style.Font.Italic = false;
            this.Text5.Style.Font.Name = "Arial";
            this.Text5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text5.Style.Font.Strikeout = false;
            this.Text5.Style.Font.Underline = false;
            this.Text5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text5.Style.Visible = true;
            this.Text5.Value = "Kabine";
            // 
            // Text6
            // 
            this.Text6.CanGrow = false;
            this.Text6.CanShrink = false;
            this.Text6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.635D), Telerik.Reporting.Drawing.Unit.Cm(2.328D));
            this.Text6.Name = "Text6";
            this.Text6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.387D), Telerik.Reporting.Drawing.Unit.Cm(0.423D));
            this.Text6.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text6.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text6.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text6.Style.Color = System.Drawing.Color.Black;
            this.Text6.Style.Font.Bold = false;
            this.Text6.Style.Font.Italic = false;
            this.Text6.Style.Font.Name = "Arial";
            this.Text6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text6.Style.Font.Strikeout = false;
            this.Text6.Style.Font.Underline = false;
            this.Text6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text6.Style.Visible = true;
            this.Text6.Value = "MG Name:";
            // 
            // Text7
            // 
            this.Text7.CanGrow = false;
            this.Text7.CanShrink = false;
            this.Text7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.635D), Telerik.Reporting.Drawing.Unit.Cm(3.598D));
            this.Text7.Name = "Text7";
            this.Text7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.117D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Text7.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text7.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text7.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text7.Style.Color = System.Drawing.Color.Black;
            this.Text7.Style.Font.Bold = false;
            this.Text7.Style.Font.Italic = false;
            this.Text7.Style.Font.Name = "Arial";
            this.Text7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text7.Style.Font.Strikeout = false;
            this.Text7.Style.Font.Underline = false;
            this.Text7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text7.Style.Visible = true;
            this.Text7.Value = "MG Art-Nr.";
            // 
            // Text8
            // 
            this.Text8.CanGrow = false;
            this.Text8.CanShrink = false;
            this.Text8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.635D), Telerik.Reporting.Drawing.Unit.Cm(2.963D));
            this.Text8.Name = "Text8";
            this.Text8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.387D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Text8.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text8.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text8.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text8.Style.Color = System.Drawing.Color.Black;
            this.Text8.Style.Font.Bold = false;
            this.Text8.Style.Font.Italic = false;
            this.Text8.Style.Font.Name = "Arial";
            this.Text8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text8.Style.Font.Strikeout = false;
            this.Text8.Style.Font.Underline = false;
            this.Text8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text8.Style.Visible = true;
            this.Text8.Value = "MG Genesys-Nr";
            // 
            // Name1
            // 
            this.Name1.CanGrow = false;
            this.Name1.CanShrink = false;
            this.Name1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.3D), Telerik.Reporting.Drawing.Unit.Cm(2.328D));
            this.Name1.Name = "Name1";
            this.Name1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.815D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Name1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Name1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Name1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Name1.Style.Color = System.Drawing.Color.Black;
            this.Name1.Style.Font.Bold = false;
            this.Name1.Style.Font.Italic = false;
            this.Name1.Style.Font.Name = "Arial";
            this.Name1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Name1.Style.Font.Strikeout = false;
            this.Name1.Style.Font.Underline = false;
            this.Name1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Name1.Style.Visible = true;
            this.Name1.Value = "=Fields.[KName]";
            // 
            // ArtNr1
            // 
            this.ArtNr1.CanGrow = false;
            this.ArtNr1.CanShrink = false;
            this.ArtNr1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.3D), Telerik.Reporting.Drawing.Unit.Cm(3.598D));
            this.ArtNr1.Name = "ArtNr1";
            this.ArtNr1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.505D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.ArtNr1.Style.BackgroundColor = System.Drawing.Color.White;
            this.ArtNr1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.ArtNr1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.ArtNr1.Style.Color = System.Drawing.Color.Black;
            this.ArtNr1.Style.Font.Bold = false;
            this.ArtNr1.Style.Font.Italic = false;
            this.ArtNr1.Style.Font.Name = "Arial";
            this.ArtNr1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.ArtNr1.Style.Font.Strikeout = false;
            this.ArtNr1.Style.Font.Underline = false;
            this.ArtNr1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.ArtNr1.Style.Visible = true;
            this.ArtNr1.Value = "=Fields.[ArtNr]";
            // 
            // GenesysNr1
            // 
            this.GenesysNr1.CanGrow = false;
            this.GenesysNr1.CanShrink = false;
            this.GenesysNr1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.3D), Telerik.Reporting.Drawing.Unit.Cm(2.963D));
            this.GenesysNr1.Name = "GenesysNr1";
            this.GenesysNr1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.505D), Telerik.Reporting.Drawing.Unit.Cm(0.423D));
            this.GenesysNr1.Style.BackgroundColor = System.Drawing.Color.White;
            this.GenesysNr1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.GenesysNr1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.GenesysNr1.Style.Color = System.Drawing.Color.Black;
            this.GenesysNr1.Style.Font.Bold = false;
            this.GenesysNr1.Style.Font.Italic = false;
            this.GenesysNr1.Style.Font.Name = "Arial";
            this.GenesysNr1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.GenesysNr1.Style.Font.Strikeout = false;
            this.GenesysNr1.Style.Font.Underline = false;
            this.GenesysNr1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.GenesysNr1.Style.Visible = true;
            this.GenesysNr1.Value = "=Fields.[GenesysNr]";
            // 
            // Text9
            // 
            this.Text9.CanGrow = false;
            this.Text9.CanShrink = false;
            this.Text9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.162D), Telerik.Reporting.Drawing.Unit.Cm(4.233D));
            this.Text9.Name = "Text9";
            this.Text9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.54D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Text9.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text9.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text9.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text9.Style.Color = System.Drawing.Color.Black;
            this.Text9.Style.Font.Bold = false;
            this.Text9.Style.Font.Italic = false;
            this.Text9.Style.Font.Name = "Arial";
            this.Text9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text9.Style.Font.Strikeout = false;
            this.Text9.Style.Font.Underline = false;
            this.Text9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text9.Style.Visible = true;
            this.Text9.Value = "Erstellt am";
            // 
            // Text10
            // 
            this.Text10.CanGrow = false;
            this.Text10.CanShrink = false;
            this.Text10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.635D), Telerik.Reporting.Drawing.Unit.Cm(5.08D));
            this.Text10.Name = "Text10";
            this.Text10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.043D), Telerik.Reporting.Drawing.Unit.Cm(0.568D));
            this.Text10.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text10.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text10.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text10.Style.Color = System.Drawing.Color.Black;
            this.Text10.Style.Font.Bold = false;
            this.Text10.Style.Font.Italic = false;
            this.Text10.Style.Font.Name = "Arial";
            this.Text10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text10.Style.Font.Strikeout = false;
            this.Text10.Style.Font.Underline = true;
            this.Text10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text10.Style.Visible = true;
            this.Text10.Value = "Wiegeaufträge\nRocheAG, Sisseln GUPAS\n";
            // 
            // Text14
            // 
            this.Text14.CanGrow = false;
            this.Text14.CanShrink = false;
            this.Text14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.077D), Telerik.Reporting.Drawing.Unit.Cm(5.715D));
            this.Text14.Name = "Text14";
            this.Text14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.505D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Text14.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text14.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text14.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text14.Style.Color = System.Drawing.Color.Black;
            this.Text14.Style.Font.Bold = false;
            this.Text14.Style.Font.Italic = false;
            this.Text14.Style.Font.Name = "Arial";
            this.Text14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text14.Style.Font.Strikeout = false;
            this.Text14.Style.Font.Underline = false;
            this.Text14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text14.Style.Visible = true;
            this.Text14.Value = "Barcode";
            // 
            // Text15
            // 
            this.Text15.CanGrow = false;
            this.Text15.CanShrink = false;
            this.Text15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.661D), Telerik.Reporting.Drawing.Unit.Cm(5.72D));
            this.Text15.Name = "Text15";
            this.Text15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.058D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Text15.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text15.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text15.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text15.Style.Color = System.Drawing.Color.Black;
            this.Text15.Style.Font.Bold = false;
            this.Text15.Style.Font.Italic = false;
            this.Text15.Style.Font.Name = "Arial";
            this.Text15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text15.Style.Font.Strikeout = false;
            this.Text15.Style.Font.Underline = false;
            this.Text15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.Text15.Style.Visible = true;
            this.Text15.Value = "Pos.";
            // 
            // Name2
            // 
            this.Name2.CanGrow = false;
            this.Name2.CanShrink = false;
            this.Name2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.913D), Telerik.Reporting.Drawing.Unit.Cm(3.598D));
            this.Name2.Name = "Name2";
            this.Name2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.371D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Name2.Style.BackgroundColor = System.Drawing.Color.White;
            this.Name2.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Name2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Name2.Style.Color = System.Drawing.Color.Black;
            this.Name2.Style.Font.Bold = false;
            this.Name2.Style.Font.Italic = false;
            this.Name2.Style.Font.Name = "Arial";
            this.Name2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Name2.Style.Font.Strikeout = false;
            this.Name2.Style.Font.Underline = false;
            this.Name2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Name2.Style.Visible = true;
            this.Name2.Value = "=Fields.[RName]";
            // 
            // Besteller1
            // 
            this.Besteller1.CanGrow = false;
            this.Besteller1.CanShrink = false;
            this.Besteller1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.913D), Telerik.Reporting.Drawing.Unit.Cm(2.328D));
            this.Besteller1.Name = "Besteller1";
            this.Besteller1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.505D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Besteller1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Besteller1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Besteller1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Besteller1.Style.Color = System.Drawing.Color.Black;
            this.Besteller1.Style.Font.Bold = false;
            this.Besteller1.Style.Font.Italic = false;
            this.Besteller1.Style.Font.Name = "Arial";
            this.Besteller1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Besteller1.Style.Font.Strikeout = false;
            this.Besteller1.Style.Font.Underline = false;
            this.Besteller1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Besteller1.Style.Visible = true;
            this.Besteller1.Value = "=Fields.[Besteller]";
            // 
            // Name3
            // 
            this.Name3.CanGrow = false;
            this.Name3.CanShrink = false;
            this.Name3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.913D), Telerik.Reporting.Drawing.Unit.Cm(2.963D));
            this.Name3.Name = "Name3";
            this.Name3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.505D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Name3.Style.BackgroundColor = System.Drawing.Color.White;
            this.Name3.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Name3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Name3.Style.Color = System.Drawing.Color.Black;
            this.Name3.Style.Font.Bold = false;
            this.Name3.Style.Font.Italic = false;
            this.Name3.Style.Font.Name = "Arial";
            this.Name3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Name3.Style.Font.Strikeout = false;
            this.Name3.Style.Font.Underline = false;
            this.Name3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Name3.Style.Visible = true;
            this.Name3.Value = "=Fields.[Name]";
            // 
            // Text20
            // 
            this.Text20.CanGrow = false;
            this.Text20.CanShrink = false;
            this.Text20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.162D), Telerik.Reporting.Drawing.Unit.Cm(2.963D));
            this.Text20.Name = "Text20";
            this.Text20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.905D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Text20.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text20.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text20.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text20.Style.Color = System.Drawing.Color.Black;
            this.Text20.Style.Font.Bold = false;
            this.Text20.Style.Font.Italic = false;
            this.Text20.Style.Font.Name = "Arial";
            this.Text20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text20.Style.Font.Strikeout = false;
            this.Text20.Style.Font.Underline = false;
            this.Text20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text20.Style.Visible = true;
            this.Text20.Value = "Ersteller";
            // 
            // ANr1
            // 
            this.ANr1.CanGrow = false;
            this.ANr1.CanShrink = false;
            this.ANr1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.3D), Telerik.Reporting.Drawing.Unit.Cm(1.058D));
            this.ANr1.Name = "ANr1";
            this.ANr1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.505D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.ANr1.Style.BackgroundColor = System.Drawing.Color.White;
            this.ANr1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.ANr1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.ANr1.Style.Color = System.Drawing.Color.Black;
            this.ANr1.Style.Font.Bold = false;
            this.ANr1.Style.Font.Italic = false;
            this.ANr1.Style.Font.Name = "Arial";
            this.ANr1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.ANr1.Style.Font.Strikeout = false;
            this.ANr1.Style.Font.Underline = false;
            this.ANr1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.ANr1.Style.Visible = true;
            this.ANr1.Value = "=Fields.[ANr]";
            // 
            // ZeitNew1
            // 
            this.ZeitNew1.CanGrow = false;
            this.ZeitNew1.CanShrink = false;
            this.ZeitNew1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.913D), Telerik.Reporting.Drawing.Unit.Cm(4.233D));
            this.ZeitNew1.Name = "ZeitNew1";
            this.ZeitNew1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.445D), Telerik.Reporting.Drawing.Unit.Cm(0.487D));
            this.ZeitNew1.Style.BackgroundColor = System.Drawing.Color.White;
            this.ZeitNew1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.ZeitNew1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.ZeitNew1.Style.Color = System.Drawing.Color.Black;
            this.ZeitNew1.Style.Font.Bold = false;
            this.ZeitNew1.Style.Font.Italic = false;
            this.ZeitNew1.Style.Font.Name = "Arial";
            this.ZeitNew1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.ZeitNew1.Style.Font.Strikeout = false;
            this.ZeitNew1.Style.Font.Underline = false;
            this.ZeitNew1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.ZeitNew1.Style.Visible = true;
            this.ZeitNew1.Value = "=Fields.[ZeitNew]";
            // 
            // Lot1
            // 
            this.Lot1.CanGrow = false;
            this.Lot1.CanShrink = false;
            this.Lot1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.3D), Telerik.Reporting.Drawing.Unit.Cm(4.233D));
            this.Lot1.Name = "Lot1";
            this.Lot1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.505D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Lot1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Lot1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Lot1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Lot1.Style.Color = System.Drawing.Color.Black;
            this.Lot1.Style.Font.Bold = false;
            this.Lot1.Style.Font.Italic = false;
            this.Lot1.Style.Font.Name = "Arial";
            this.Lot1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Lot1.Style.Font.Strikeout = false;
            this.Lot1.Style.Font.Underline = false;
            this.Lot1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Lot1.Style.Visible = true;
            this.Lot1.Value = "=Fields.[Lot]";
            // 
            // Text22
            // 
            this.Text22.CanGrow = false;
            this.Text22.CanShrink = false;
            this.Text22.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.635D), Telerik.Reporting.Drawing.Unit.Cm(4.233D));
            this.Text22.Name = "Text22";
            this.Text22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.117D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Text22.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text22.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text22.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text22.Style.Color = System.Drawing.Color.Black;
            this.Text22.Style.Font.Bold = false;
            this.Text22.Style.Font.Italic = false;
            this.Text22.Style.Font.Name = "Arial";
            this.Text22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text22.Style.Font.Strikeout = false;
            this.Text22.Style.Font.Underline = false;
            this.Text22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text22.Style.Visible = true;
            this.Text22.Value = "MG Lot-Nr.";
            // 
            // Nummer1
            // 
            this.Nummer1.CanGrow = false;
            this.Nummer1.CanShrink = false;
            this.Nummer1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.449D), Telerik.Reporting.Drawing.Unit.Cm(1.058D));
            this.Nummer1.Name = "Nummer1";
            this.Nummer1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.617D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Nummer1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Nummer1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Nummer1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Nummer1.Style.Color = System.Drawing.Color.Black;
            this.Nummer1.Style.Font.Bold = false;
            this.Nummer1.Style.Font.Italic = false;
            this.Nummer1.Style.Font.Name = "Arial";
            this.Nummer1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Nummer1.Style.Font.Strikeout = false;
            this.Nummer1.Style.Font.Underline = false;
            this.Nummer1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Nummer1.Style.Visible = false;
            this.Nummer1.Value = "=Fields.[DNummer]";
            // 
            // Text25
            // 
            this.Text25.CanGrow = false;
            this.Text25.CanShrink = false;
            this.Text25.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.352D), Telerik.Reporting.Drawing.Unit.Cm(5.715D));
            this.Text25.Name = "Text25";
            this.Text25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.963D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Text25.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text25.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text25.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text25.Style.Color = System.Drawing.Color.Black;
            this.Text25.Style.Font.Bold = false;
            this.Text25.Style.Font.Italic = false;
            this.Text25.Style.Font.Name = "Arial";
            this.Text25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text25.Style.Font.Strikeout = false;
            this.Text25.Style.Font.Underline = false;
            this.Text25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text25.Style.Visible = true;
            this.Text25.Value = "Gebinde";
            // 
            // Text12
            // 
            this.Text12.CanGrow = false;
            this.Text12.CanShrink = false;
            this.Text12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.927D), Telerik.Reporting.Drawing.Unit.Cm(5.715D));
            this.Text12.Name = "Text12";
            this.Text12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.977D), Telerik.Reporting.Drawing.Unit.Cm(0.423D));
            this.Text12.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text12.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text12.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text12.Style.Color = System.Drawing.Color.Black;
            this.Text12.Style.Font.Bold = false;
            this.Text12.Style.Font.Italic = false;
            this.Text12.Style.Font.Name = "Arial";
            this.Text12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text12.Style.Font.Strikeout = false;
            this.Text12.Style.Font.Underline = false;
            this.Text12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.Text12.Style.Visible = true;
            this.Text12.Value = "Soll";
            // 
            // Text13
            // 
            this.Text13.CanGrow = false;
            this.Text13.CanShrink = false;
            this.Text13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.372D), Telerik.Reporting.Drawing.Unit.Cm(5.715D));
            this.Text13.Name = "Text13";
            this.Text13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.942D), Telerik.Reporting.Drawing.Unit.Cm(0.395D));
            this.Text13.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text13.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text13.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text13.Style.Color = System.Drawing.Color.Black;
            this.Text13.Style.Font.Bold = false;
            this.Text13.Style.Font.Italic = false;
            this.Text13.Style.Font.Name = "Arial";
            this.Text13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text13.Style.Font.Strikeout = false;
            this.Text13.Style.Font.Underline = false;
            this.Text13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.Text13.Style.Visible = true;
            this.Text13.Value = "Nummer";
            // 
            // BArt1
            // 
            this.BArt1.CanGrow = false;
            this.BArt1.CanShrink = false;
            this.BArt1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.3D), Telerik.Reporting.Drawing.Unit.Cm(1.693D));
            this.BArt1.Name = "BArt1";
            this.BArt1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.175D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.BArt1.Style.BackgroundColor = System.Drawing.Color.White;
            this.BArt1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.BArt1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.BArt1.Style.Color = System.Drawing.Color.Black;
            this.BArt1.Style.Font.Bold = false;
            this.BArt1.Style.Font.Italic = false;
            this.BArt1.Style.Font.Name = "Arial";
            this.BArt1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.BArt1.Style.Font.Strikeout = false;
            this.BArt1.Style.Font.Underline = false;
            this.BArt1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.BArt1.Style.Visible = true;
            this.BArt1.Value = "=Fields.[BArt]";
            // 
            // Picture1
            // 
            this.Picture1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.072D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.Picture1.MimeType = "image/jpeg";
            this.Picture1.Name = "Picture1";
            this.Picture1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.007D), Telerik.Reporting.Drawing.Unit.Cm(2.328D));
            this.Picture1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.Stretch;
            this.Picture1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Picture1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Picture1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Picture1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Picture1.Style.Visible = true;
            this.Picture1.Value = ((object)(resources.GetObject("Picture1.Value")));
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.4D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.679D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.textBox2.Value = " Ver . 1.0";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.3D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.714D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.textBox1.Value = "Created with Telerik Reports  3.44 ";
            // 
            // WList
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.ReportHeaderArea1,
            this.PageHeaderArea1,
            this.DetailArea1,
            this.ReportFooterArea1,
            this.PageFooterArea1});
            this.Name = "WList";
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Cm(1.501D), Telerik.Reporting.Drawing.Unit.Cm(0.42D), Telerik.Reporting.Drawing.Unit.Cm(1.501D), Telerik.Reporting.Drawing.Unit.Cm(0.57D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Cm(19.079D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.ReportHeaderSection ReportHeaderArea1;
        private Telerik.Reporting.DetailSection DetailArea1;
        private Telerik.Reporting.TextBox Datensatznummer1;
        private Telerik.Reporting.TextBox Text21;
        private Telerik.Reporting.TextBox Soll1;
        private Telerik.Reporting.TextBox Nummer2;
        private Telerik.Reporting.ReportFooterSection ReportFooterArea1;
        private Telerik.Reporting.TextBox Text11;
        private Telerik.Reporting.TextBox Text16;
        private Telerik.Reporting.TextBox Text17;
        private Telerik.Reporting.TextBox Text18;
        private Telerik.Reporting.TextBox Text19;
        private Telerik.Reporting.PageFooterSection PageFooterArea1;
        private Telerik.Reporting.TextBox SeiteNvonM1;
        private Telerik.Reporting.PageHeaderSection PageHeaderArea1;
        private Telerik.Reporting.TextBox Text2;
        private Telerik.Reporting.TextBox Text1;
        private Telerik.Reporting.TextBox Text3;
        private Telerik.Reporting.TextBox Text4;
        private Telerik.Reporting.TextBox Text5;
        private Telerik.Reporting.TextBox Text6;
        private Telerik.Reporting.TextBox Text7;
        private Telerik.Reporting.TextBox Text8;
        private Telerik.Reporting.TextBox Name1;
        private Telerik.Reporting.TextBox ArtNr1;
        private Telerik.Reporting.TextBox GenesysNr1;
        private Telerik.Reporting.TextBox Text9;
        private Telerik.Reporting.TextBox Text10;
        private Telerik.Reporting.TextBox Text14;
        private Telerik.Reporting.TextBox Text15;
        private Telerik.Reporting.TextBox Name2;
        private Telerik.Reporting.TextBox Besteller1;
        private Telerik.Reporting.TextBox Name3;
        private Telerik.Reporting.TextBox Text20;
        private Telerik.Reporting.TextBox ANr1;
        private Telerik.Reporting.TextBox ZeitNew1;
        private Telerik.Reporting.TextBox Lot1;
        private Telerik.Reporting.TextBox Text22;
        private Telerik.Reporting.TextBox Nummer1;
        private Telerik.Reporting.TextBox Text25;
        private Telerik.Reporting.TextBox Text12;
        private Telerik.Reporting.TextBox Text13;
        private Telerik.Reporting.TextBox BArt1;
        private Telerik.Reporting.PictureBox Picture1;
        private Telerik.Reporting.Barcode barcode1;
        private Telerik.Reporting.Shape Line1;
        private Telerik.Reporting.SubReport subReport1;
        private Telerik.Reporting.Shape Line2;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox1;
    }
}