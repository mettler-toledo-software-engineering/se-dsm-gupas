namespace MT.GUPAS.Reporting
{
    partial class WJournal
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WJournal));
            Telerik.Reporting.Group group1 = new Telerik.Reporting.Group();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.GroupFooterArea1 = new Telerik.Reporting.GroupFooterSection();
            this.totalNetto1 = new Telerik.Reporting.TextBox();
            this.RTotal01 = new Telerik.Reporting.TextBox();
            this.GroupHeaderArea1 = new Telerik.Reporting.GroupHeaderSection();
            this.GroupNameZeitdaily1 = new Telerik.Reporting.TextBox();
            this.ReportHeaderArea1 = new Telerik.Reporting.ReportHeaderSection();
            this.PageHeaderArea1 = new Telerik.Reporting.PageHeaderSection();
            this.Line1 = new Telerik.Reporting.Shape();
            this.Seitenzahl1 = new Telerik.Reporting.TextBox();
            this.Text2 = new Telerik.Reporting.TextBox();
            this.Text9 = new Telerik.Reporting.TextBox();
            this.Text12 = new Telerik.Reporting.TextBox();
            this.Text15 = new Telerik.Reporting.TextBox();
            this.Text13 = new Telerik.Reporting.TextBox();
            this.Text14 = new Telerik.Reporting.TextBox();
            this.Text8 = new Telerik.Reporting.TextBox();
            this.Text7 = new Telerik.Reporting.TextBox();
            this.Text6 = new Telerik.Reporting.TextBox();
            this.Text4 = new Telerik.Reporting.TextBox();
            this.Text5 = new Telerik.Reporting.TextBox();
            this.Picture1 = new Telerik.Reporting.PictureBox();
            this.DetailArea1 = new Telerik.Reporting.DetailSection();
            this.Zeit1 = new Telerik.Reporting.TextBox();
            this.Lot1 = new Telerik.Reporting.TextBox();
            this.ANr1 = new Telerik.Reporting.TextBox();
            this.Name1 = new Telerik.Reporting.TextBox();
            this.BArt1 = new Telerik.Reporting.TextBox();
            this.Brutto1 = new Telerik.Reporting.TextBox();
            this.Name3 = new Telerik.Reporting.TextBox();
            this.ArtNr1 = new Telerik.Reporting.TextBox();
            this.Netto1 = new Telerik.Reporting.TextBox();
            this.Name2 = new Telerik.Reporting.TextBox();
            this.Tara1 = new Telerik.Reporting.TextBox();
            this.ReportFooterArea1 = new Telerik.Reporting.ReportFooterSection();
            this.Text10 = new Telerik.Reporting.TextBox();
            this.Text11 = new Telerik.Reporting.TextBox();
            this.Druckdatum1 = new Telerik.Reporting.TextBox();
            this.Text3 = new Telerik.Reporting.TextBox();
            this.PageFooterArea1 = new Telerik.Reporting.PageFooterSection();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.SeiteNvonM1 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // GroupFooterArea1
            // 
            this.GroupFooterArea1.Height = Telerik.Reporting.Drawing.Unit.Cm(0.7D);
            this.GroupFooterArea1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.totalNetto1,
            this.RTotal01});
            this.GroupFooterArea1.KeepTogether = false;
            this.GroupFooterArea1.Name = "GroupFooterArea1";
            this.GroupFooterArea1.PageBreak = Telerik.Reporting.PageBreak.None;
            this.GroupFooterArea1.Style.BackgroundColor = System.Drawing.Color.White;
            this.GroupFooterArea1.Style.Visible = true;
            // 
            // totalNetto1
            // 
            this.totalNetto1.CanGrow = false;
            this.totalNetto1.CanShrink = false;
            this.totalNetto1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.809D), Telerik.Reporting.Drawing.Unit.Cm(0.088D));
            this.totalNetto1.Name = "totalNetto1";
            this.totalNetto1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.7D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.totalNetto1.Style.BackgroundColor = System.Drawing.Color.White;
            this.totalNetto1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.totalNetto1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.totalNetto1.Style.Color = System.Drawing.Color.Black;
            this.totalNetto1.Style.Font.Bold = false;
            this.totalNetto1.Style.Font.Italic = false;
            this.totalNetto1.Style.Font.Name = "Arial";
            this.totalNetto1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.totalNetto1.Style.Font.Strikeout = false;
            this.totalNetto1.Style.Font.Underline = false;
            this.totalNetto1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.totalNetto1.Style.Visible = false;
            this.totalNetto1.Value = "=";
            // 
            // RTotal01
            // 
            this.RTotal01.CanGrow = false;
            this.RTotal01.CanShrink = false;
            this.RTotal01.Format = "{0:N4}";
            this.RTotal01.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12D), Telerik.Reporting.Drawing.Unit.Cm(0.088D));
            this.RTotal01.Name = "RTotal01";
            this.RTotal01.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.RTotal01.Style.BackgroundColor = System.Drawing.Color.White;
            this.RTotal01.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.RTotal01.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.RTotal01.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.RTotal01.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.RTotal01.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.RTotal01.Style.Color = System.Drawing.Color.Black;
            this.RTotal01.Style.Font.Bold = false;
            this.RTotal01.Style.Font.Italic = false;
            this.RTotal01.Style.Font.Name = "Arial";
            this.RTotal01.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.RTotal01.Style.Font.Strikeout = false;
            this.RTotal01.Style.Font.Underline = false;
            this.RTotal01.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.RTotal01.Style.Visible = true;
            this.RTotal01.Value = "=IIf(Trim(Fields.[Unit]) = \"g\", (Round(Sum(Fields.[Netto]),4) * 1000), (Round(Sum" +
    "(Fields.[Netto]),4))) + \' \' + Fields.[Unit]";
            // 
            // GroupHeaderArea1
            // 
            this.GroupHeaderArea1.Height = Telerik.Reporting.Drawing.Unit.Cm(0.6D);
            this.GroupHeaderArea1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.GroupNameZeitdaily1});
            this.GroupHeaderArea1.KeepTogether = false;
            this.GroupHeaderArea1.Name = "GroupHeaderArea1";
            this.GroupHeaderArea1.PageBreak = Telerik.Reporting.PageBreak.Before;
            this.GroupHeaderArea1.Style.BackgroundColor = System.Drawing.Color.White;
            this.GroupHeaderArea1.Style.Visible = true;
            // 
            // GroupNameZeitdaily1
            // 
            this.GroupNameZeitdaily1.CanGrow = false;
            this.GroupNameZeitdaily1.CanShrink = false;
            this.GroupNameZeitdaily1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.GroupNameZeitdaily1.Name = "GroupNameZeitdaily1";
            this.GroupNameZeitdaily1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.309D), Telerik.Reporting.Drawing.Unit.Cm(0.474D));
            this.GroupNameZeitdaily1.Style.BackgroundColor = System.Drawing.Color.White;
            this.GroupNameZeitdaily1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.GroupNameZeitdaily1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.GroupNameZeitdaily1.Style.Color = System.Drawing.Color.Black;
            this.GroupNameZeitdaily1.Style.Font.Bold = true;
            this.GroupNameZeitdaily1.Style.Font.Italic = false;
            this.GroupNameZeitdaily1.Style.Font.Name = "Arial";
            this.GroupNameZeitdaily1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.GroupNameZeitdaily1.Style.Font.Strikeout = false;
            this.GroupNameZeitdaily1.Style.Font.Underline = false;
            this.GroupNameZeitdaily1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.GroupNameZeitdaily1.Style.Visible = true;
            this.GroupNameZeitdaily1.Value = "=Fields.[ANr]";
            // 
            // ReportHeaderArea1
            // 
            this.ReportHeaderArea1.Height = Telerik.Reporting.Drawing.Unit.Cm(0.345D);
            this.ReportHeaderArea1.KeepTogether = false;
            this.ReportHeaderArea1.Name = "ReportHeaderArea1";
            this.ReportHeaderArea1.PageBreak = Telerik.Reporting.PageBreak.Before;
            this.ReportHeaderArea1.Style.BackgroundColor = System.Drawing.Color.White;
            this.ReportHeaderArea1.Style.Visible = true;
            // 
            // PageHeaderArea1
            // 
            this.PageHeaderArea1.Height = Telerik.Reporting.Drawing.Unit.Cm(2.755D);
            this.PageHeaderArea1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.Line1,
            this.Seitenzahl1,
            this.Text2,
            this.Text9,
            this.Text12,
            this.Text15,
            this.Text13,
            this.Text14,
            this.Text8,
            this.Text7,
            this.Text6,
            this.Text4,
            this.Text5,
            this.Picture1});
            this.PageHeaderArea1.Name = "PageHeaderArea1";
            this.PageHeaderArea1.PrintOnFirstPage = true;
            this.PageHeaderArea1.PrintOnLastPage = true;
            this.PageHeaderArea1.Style.BackgroundColor = System.Drawing.Color.White;
            this.PageHeaderArea1.Style.Visible = true;
            // 
            // Line1
            // 
            this.Line1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.688D), Telerik.Reporting.Drawing.Unit.Cm(2.4D));
            this.Line1.Name = "Line1";
            this.Line1.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            this.Line1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(27.067D), Telerik.Reporting.Drawing.Unit.Point(4D));
            this.Line1.Style.Color = System.Drawing.Color.Black;
            this.Line1.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.Line1.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.Line1.Style.Visible = true;
            // 
            // Seitenzahl1
            // 
            this.Seitenzahl1.CanGrow = false;
            this.Seitenzahl1.CanShrink = false;
            this.Seitenzahl1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.688D), Telerik.Reporting.Drawing.Unit.Cm(0.474D));
            this.Seitenzahl1.Name = "Seitenzahl1";
            this.Seitenzahl1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(24.112D), Telerik.Reporting.Drawing.Unit.Cm(0.584D));
            this.Seitenzahl1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Seitenzahl1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Seitenzahl1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Seitenzahl1.Style.Color = System.Drawing.Color.Black;
            this.Seitenzahl1.Style.Font.Bold = true;
            this.Seitenzahl1.Style.Font.Italic = false;
            this.Seitenzahl1.Style.Font.Name = "Arial";
            this.Seitenzahl1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.Seitenzahl1.Style.Font.Strikeout = false;
            this.Seitenzahl1.Style.Font.Underline = true;
            this.Seitenzahl1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Seitenzahl1.Style.Visible = true;
            this.Seitenzahl1.Value = "=Format(\"W�gejournal                               DSM-DNP AG Sisseln Bau 320    " +
    "                                  Seite {0}\", PageNumber)";
            // 
            // Text2
            // 
            this.Text2.CanGrow = false;
            this.Text2.CanShrink = false;
            this.Text2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.4D), Telerik.Reporting.Drawing.Unit.Cm(1.692D));
            this.Text2.Name = "Text2";
            this.Text2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.395D), Telerik.Reporting.Drawing.Unit.Cm(0.474D));
            this.Text2.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text2.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text2.Style.Color = System.Drawing.Color.Black;
            this.Text2.Style.Font.Bold = false;
            this.Text2.Style.Font.Italic = false;
            this.Text2.Style.Font.Name = "Arial";
            this.Text2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text2.Style.Font.Strikeout = false;
            this.Text2.Style.Font.Underline = false;
            this.Text2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text2.Style.Visible = true;
            this.Text2.Value = "Zeit";
            // 
            // Text9
            // 
            this.Text9.CanGrow = false;
            this.Text9.CanShrink = false;
            this.Text9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.087D), Telerik.Reporting.Drawing.Unit.Cm(1.693D));
            this.Text9.Name = "Text9";
            this.Text9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.013D), Telerik.Reporting.Drawing.Unit.Cm(0.474D));
            this.Text9.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text9.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text9.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text9.Style.Color = System.Drawing.Color.Black;
            this.Text9.Style.Font.Bold = false;
            this.Text9.Style.Font.Italic = false;
            this.Text9.Style.Font.Name = "Arial";
            this.Text9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text9.Style.Font.Strikeout = false;
            this.Text9.Style.Font.Underline = false;
            this.Text9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text9.Style.Visible = true;
            this.Text9.Value = "Lot-Nr.";
            // 
            // Text12
            // 
            this.Text12.CanGrow = false;
            this.Text12.CanShrink = false;
            this.Text12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.838D), Telerik.Reporting.Drawing.Unit.Cm(1.692D));
            this.Text12.Name = "Text12";
            this.Text12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.54D), Telerik.Reporting.Drawing.Unit.Cm(0.474D));
            this.Text12.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text12.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text12.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text12.Style.Color = System.Drawing.Color.Black;
            this.Text12.Style.Font.Bold = false;
            this.Text12.Style.Font.Italic = false;
            this.Text12.Style.Font.Name = "Arial";
            this.Text12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text12.Style.Font.Strikeout = false;
            this.Text12.Style.Font.Underline = false;
            this.Text12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text12.Style.Visible = true;
            this.Text12.Value = "AuftragsNr.";
            // 
            // Text15
            // 
            this.Text15.CanGrow = false;
            this.Text15.CanShrink = false;
            this.Text15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(25.4D), Telerik.Reporting.Drawing.Unit.Cm(1.693D));
            this.Text15.Name = "Text15";
            this.Text15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.117D), Telerik.Reporting.Drawing.Unit.Cm(0.474D));
            this.Text15.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text15.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text15.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text15.Style.Color = System.Drawing.Color.Black;
            this.Text15.Style.Font.Bold = false;
            this.Text15.Style.Font.Italic = false;
            this.Text15.Style.Font.Name = "Arial";
            this.Text15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text15.Style.Font.Strikeout = false;
            this.Text15.Style.Font.Underline = false;
            this.Text15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text15.Style.Visible = true;
            this.Text15.Value = "Kabine";
            // 
            // Text13
            // 
            this.Text13.CanGrow = false;
            this.Text13.CanShrink = false;
            this.Text13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(21.378D), Telerik.Reporting.Drawing.Unit.Cm(1.693D));
            this.Text13.Name = "Text13";
            this.Text13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.905D), Telerik.Reporting.Drawing.Unit.Cm(0.474D));
            this.Text13.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text13.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text13.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text13.Style.Color = System.Drawing.Color.Black;
            this.Text13.Style.Font.Bold = false;
            this.Text13.Style.Font.Italic = false;
            this.Text13.Style.Font.Name = "Arial";
            this.Text13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text13.Style.Font.Strikeout = false;
            this.Text13.Style.Font.Underline = false;
            this.Text13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text13.Style.Visible = true;
            this.Text13.Value = "Auf.-Art";
            // 
            // Text14
            // 
            this.Text14.CanGrow = false;
            this.Text14.CanShrink = false;
            this.Text14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(23.283D), Telerik.Reporting.Drawing.Unit.Cm(1.693D));
            this.Text14.Name = "Text14";
            this.Text14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.117D), Telerik.Reporting.Drawing.Unit.Cm(0.474D));
            this.Text14.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text14.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text14.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text14.Style.Color = System.Drawing.Color.Black;
            this.Text14.Style.Font.Bold = false;
            this.Text14.Style.Font.Italic = false;
            this.Text14.Style.Font.Name = "Arial";
            this.Text14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text14.Style.Font.Strikeout = false;
            this.Text14.Style.Font.Underline = false;
            this.Text14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text14.Style.Visible = true;
            this.Text14.Value = "Abf�ller";
            // 
            // Text8
            // 
            this.Text8.CanGrow = false;
            this.Text8.CanShrink = false;
            this.Text8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.548D), Telerik.Reporting.Drawing.Unit.Cm(1.692D));
            this.Text8.Name = "Text8";
            this.Text8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.252D), Telerik.Reporting.Drawing.Unit.Cm(0.474D));
            this.Text8.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text8.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text8.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text8.Style.Color = System.Drawing.Color.Black;
            this.Text8.Style.Font.Bold = false;
            this.Text8.Style.Font.Italic = false;
            this.Text8.Style.Font.Name = "Arial";
            this.Text8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text8.Style.Font.Strikeout = false;
            this.Text8.Style.Font.Underline = false;
            this.Text8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.Text8.Style.Visible = true;
            this.Text8.Value = "Tara";
            // 
            // Text7
            // 
            this.Text7.CanGrow = false;
            this.Text7.CanShrink = false;
            this.Text7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.078D), Telerik.Reporting.Drawing.Unit.Cm(1.693D));
            this.Text7.Name = "Text7";
            this.Text7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.522D), Telerik.Reporting.Drawing.Unit.Cm(0.474D));
            this.Text7.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text7.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text7.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text7.Style.Color = System.Drawing.Color.Black;
            this.Text7.Style.Font.Bold = false;
            this.Text7.Style.Font.Italic = false;
            this.Text7.Style.Font.Name = "Arial";
            this.Text7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text7.Style.Font.Strikeout = false;
            this.Text7.Style.Font.Underline = false;
            this.Text7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.Text7.Style.Visible = true;
            this.Text7.Value = "Netto";
            // 
            // Text6
            // 
            this.Text6.CanGrow = false;
            this.Text6.CanShrink = false;
            this.Text6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.809D), Telerik.Reporting.Drawing.Unit.Cm(1.693D));
            this.Text6.Name = "Text6";
            this.Text6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.324D), Telerik.Reporting.Drawing.Unit.Cm(0.474D));
            this.Text6.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text6.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text6.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text6.Style.Color = System.Drawing.Color.Black;
            this.Text6.Style.Font.Bold = false;
            this.Text6.Style.Font.Italic = false;
            this.Text6.Style.Font.Name = "Arial";
            this.Text6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text6.Style.Font.Strikeout = false;
            this.Text6.Style.Font.Underline = false;
            this.Text6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.Text6.Style.Visible = true;
            this.Text6.Value = "Brutto";
            // 
            // Text4
            // 
            this.Text4.CanGrow = false;
            this.Text4.CanShrink = false;
            this.Text4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.027D), Telerik.Reporting.Drawing.Unit.Cm(1.693D));
            this.Text4.Name = "Text4";
            this.Text4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.273D), Telerik.Reporting.Drawing.Unit.Cm(0.474D));
            this.Text4.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text4.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text4.Style.Color = System.Drawing.Color.Black;
            this.Text4.Style.Font.Bold = false;
            this.Text4.Style.Font.Italic = false;
            this.Text4.Style.Font.Name = "Arial";
            this.Text4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text4.Style.Font.Strikeout = false;
            this.Text4.Style.Font.Underline = false;
            this.Text4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text4.Style.Visible = true;
            this.Text4.Value = "Artikelname";
            // 
            // Text5
            // 
            this.Text5.CanGrow = false;
            this.Text5.CanShrink = false;
            this.Text5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.349D), Telerik.Reporting.Drawing.Unit.Cm(1.692D));
            this.Text5.Name = "Text5";
            this.Text5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.058D), Telerik.Reporting.Drawing.Unit.Cm(0.474D));
            this.Text5.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text5.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text5.Style.Color = System.Drawing.Color.Black;
            this.Text5.Style.Font.Bold = false;
            this.Text5.Style.Font.Italic = false;
            this.Text5.Style.Font.Name = "Arial";
            this.Text5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text5.Style.Font.Strikeout = false;
            this.Text5.Style.Font.Underline = false;
            this.Text5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text5.Style.Visible = true;
            this.Text5.Value = "Art-Nr.";
            // 
            // Picture1
            // 
            this.Picture1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(24.8D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.Picture1.MimeType = "image/jpeg";
            this.Picture1.Name = "Picture1";
            this.Picture1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.898D), Telerik.Reporting.Drawing.Unit.Cm(1.693D));
            this.Picture1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.Stretch;
            this.Picture1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Picture1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Picture1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Picture1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Picture1.Style.Visible = true;
            this.Picture1.Value = ((object)(resources.GetObject("Picture1.Value")));
            // 
            // DetailArea1
            // 
            this.DetailArea1.Height = Telerik.Reporting.Drawing.Unit.Cm(0.6D);
            this.DetailArea1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.Zeit1,
            this.Lot1,
            this.ANr1,
            this.Name1,
            this.BArt1,
            this.Brutto1,
            this.Name3,
            this.ArtNr1,
            this.Netto1,
            this.Name2,
            this.Tara1});
            this.DetailArea1.KeepTogether = true;
            this.DetailArea1.Name = "DetailArea1";
            this.DetailArea1.PageBreak = Telerik.Reporting.PageBreak.None;
            this.DetailArea1.Style.BackgroundColor = System.Drawing.Color.White;
            this.DetailArea1.Style.Visible = true;
            // 
            // Zeit1
            // 
            this.Zeit1.CanGrow = false;
            this.Zeit1.CanShrink = false;
            this.Zeit1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.4D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.Zeit1.Name = "Zeit1";
            this.Zeit1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.949D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.Zeit1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Zeit1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Zeit1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Zeit1.Style.Color = System.Drawing.Color.Black;
            this.Zeit1.Style.Font.Bold = false;
            this.Zeit1.Style.Font.Italic = false;
            this.Zeit1.Style.Font.Name = "Arial";
            this.Zeit1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Zeit1.Style.Font.Strikeout = false;
            this.Zeit1.Style.Font.Underline = false;
            this.Zeit1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Zeit1.Style.Visible = true;
            this.Zeit1.Value = "=Fields.[Zeit]";
            // 
            // Lot1
            // 
            this.Lot1.CanGrow = false;
            this.Lot1.CanShrink = false;
            this.Lot1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.087D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.Lot1.Name = "Lot1";
            this.Lot1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.751D), Telerik.Reporting.Drawing.Unit.Cm(0.474D));
            this.Lot1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Lot1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Lot1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Lot1.Style.Color = System.Drawing.Color.Black;
            this.Lot1.Style.Font.Bold = false;
            this.Lot1.Style.Font.Italic = false;
            this.Lot1.Style.Font.Name = "Arial";
            this.Lot1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Lot1.Style.Font.Strikeout = false;
            this.Lot1.Style.Font.Underline = false;
            this.Lot1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Lot1.Style.Visible = true;
            this.Lot1.Value = "=Fields.[Lot]";
            // 
            // ANr1
            // 
            this.ANr1.CanGrow = false;
            this.ANr1.CanShrink = false;
            this.ANr1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.838D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.ANr1.Name = "ANr1";
            this.ANr1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.54D), Telerik.Reporting.Drawing.Unit.Cm(0.474D));
            this.ANr1.Style.BackgroundColor = System.Drawing.Color.White;
            this.ANr1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.ANr1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.ANr1.Style.Color = System.Drawing.Color.Black;
            this.ANr1.Style.Font.Bold = false;
            this.ANr1.Style.Font.Italic = false;
            this.ANr1.Style.Font.Name = "Arial";
            this.ANr1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.ANr1.Style.Font.Strikeout = false;
            this.ANr1.Style.Font.Underline = false;
            this.ANr1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.ANr1.Style.Visible = true;
            this.ANr1.Value = "=Fields.[ANr]";
            // 
            // Name1
            // 
            this.Name1.CanGrow = false;
            this.Name1.CanShrink = false;
            this.Name1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(25.638D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.Name1.Name = "Name1";
            this.Name1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.117D), Telerik.Reporting.Drawing.Unit.Cm(0.474D));
            this.Name1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Name1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Name1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Name1.Style.Color = System.Drawing.Color.Black;
            this.Name1.Style.Font.Bold = false;
            this.Name1.Style.Font.Italic = false;
            this.Name1.Style.Font.Name = "Arial";
            this.Name1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Name1.Style.Font.Strikeout = false;
            this.Name1.Style.Font.Underline = false;
            this.Name1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Name1.Style.Visible = true;
            this.Name1.Value = "=Fields.[Name]";
            // 
            // BArt1
            // 
            this.BArt1.CanGrow = false;
            this.BArt1.CanShrink = false;
            this.BArt1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(21.378D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.BArt1.Name = "BArt1";
            this.BArt1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.905D), Telerik.Reporting.Drawing.Unit.Cm(0.474D));
            this.BArt1.Style.BackgroundColor = System.Drawing.Color.White;
            this.BArt1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.BArt1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.BArt1.Style.Color = System.Drawing.Color.Black;
            this.BArt1.Style.Font.Bold = false;
            this.BArt1.Style.Font.Italic = false;
            this.BArt1.Style.Font.Name = "Arial";
            this.BArt1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.BArt1.Style.Font.Strikeout = false;
            this.BArt1.Style.Font.Underline = false;
            this.BArt1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.BArt1.Style.Visible = true;
            this.BArt1.Value = "=Fields.[BArt]";
            // 
            // Brutto1
            // 
            this.Brutto1.CanGrow = false;
            this.Brutto1.CanShrink = false;
            this.Brutto1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.995D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.Brutto1.Name = "Brutto1";
            this.Brutto1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.138D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.Brutto1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Brutto1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Brutto1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Brutto1.Style.Color = System.Drawing.Color.Black;
            this.Brutto1.Style.Font.Bold = false;
            this.Brutto1.Style.Font.Italic = false;
            this.Brutto1.Style.Font.Name = "Arial";
            this.Brutto1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Brutto1.Style.Font.Strikeout = false;
            this.Brutto1.Style.Font.Underline = false;
            this.Brutto1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.Brutto1.Style.Visible = true;
            this.Brutto1.Value = "=IIf(Trim(Fields.[Unit]) = \"g\", (Fields.Brutto * 1000), (Fields.Brutto))  + \' \' +" +
    " Fields.[Unit]";
            // 
            // Name3
            // 
            this.Name3.CanGrow = false;
            this.Name3.CanShrink = false;
            this.Name3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.027D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.Name3.Name = "Name3";
            this.Name3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.968D), Telerik.Reporting.Drawing.Unit.Cm(0.474D));
            this.Name3.Style.BackgroundColor = System.Drawing.Color.White;
            this.Name3.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Name3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Name3.Style.Color = System.Drawing.Color.Black;
            this.Name3.Style.Font.Bold = false;
            this.Name3.Style.Font.Italic = false;
            this.Name3.Style.Font.Name = "Arial";
            this.Name3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Name3.Style.Font.Strikeout = false;
            this.Name3.Style.Font.Underline = false;
            this.Name3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Name3.Style.Visible = true;
            this.Name3.Value = "=Fields.[Artikelname]";
            // 
            // ArtNr1
            // 
            this.ArtNr1.CanGrow = false;
            this.ArtNr1.CanShrink = false;
            this.ArtNr1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.349D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.ArtNr1.Name = "ArtNr1";
            this.ArtNr1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.655D), Telerik.Reporting.Drawing.Unit.Cm(0.474D));
            this.ArtNr1.Style.BackgroundColor = System.Drawing.Color.White;
            this.ArtNr1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.ArtNr1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.ArtNr1.Style.Color = System.Drawing.Color.Black;
            this.ArtNr1.Style.Font.Bold = false;
            this.ArtNr1.Style.Font.Italic = false;
            this.ArtNr1.Style.Font.Name = "Arial";
            this.ArtNr1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.ArtNr1.Style.Font.Strikeout = false;
            this.ArtNr1.Style.Font.Underline = false;
            this.ArtNr1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.ArtNr1.Style.Visible = true;
            this.ArtNr1.Value = "=Fields.[ArtNr]";
            // 
            // Netto1
            // 
            this.Netto1.CanGrow = false;
            this.Netto1.CanShrink = false;
            this.Netto1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.3D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.Netto1.Name = "Netto1";
            this.Netto1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.Netto1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Netto1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Netto1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Netto1.Style.Color = System.Drawing.Color.Black;
            this.Netto1.Style.Font.Bold = false;
            this.Netto1.Style.Font.Italic = false;
            this.Netto1.Style.Font.Name = "Arial";
            this.Netto1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Netto1.Style.Font.Strikeout = false;
            this.Netto1.Style.Font.Underline = false;
            this.Netto1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.Netto1.Style.Visible = true;
            this.Netto1.Value = "=IIf(Trim(Fields.[Unit]) = \"g\", (Fields.Netto * 1000), (Fields.Netto)) + \' \' + Fi" +
    "elds.[Unit]";
            // 
            // Name2
            // 
            this.Name2.CanGrow = false;
            this.Name2.CanShrink = false;
            this.Name2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(23.283D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.Name2.Name = "Name2";
            this.Name2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.117D), Telerik.Reporting.Drawing.Unit.Cm(0.474D));
            this.Name2.Style.BackgroundColor = System.Drawing.Color.White;
            this.Name2.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Name2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Name2.Style.Color = System.Drawing.Color.Black;
            this.Name2.Style.Font.Bold = false;
            this.Name2.Style.Font.Italic = false;
            this.Name2.Style.Font.Name = "Arial";
            this.Name2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Name2.Style.Font.Strikeout = false;
            this.Name2.Style.Font.Underline = false;
            this.Name2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Name2.Style.Visible = true;
            this.Name2.Value = "=Fields.[Abfueller]";
            // 
            // Tara1
            // 
            this.Tara1.CanGrow = false;
            this.Tara1.CanShrink = false;
            this.Tara1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.7D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.Tara1.Name = "Tara1";
            this.Tara1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1D), Telerik.Reporting.Drawing.Unit.Cm(0.402D));
            this.Tara1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Tara1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Tara1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Tara1.Style.Color = System.Drawing.Color.Black;
            this.Tara1.Style.Font.Bold = false;
            this.Tara1.Style.Font.Italic = false;
            this.Tara1.Style.Font.Name = "Arial";
            this.Tara1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Tara1.Style.Font.Strikeout = false;
            this.Tara1.Style.Font.Underline = false;
            this.Tara1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.Tara1.Style.Visible = true;
            this.Tara1.Value = "=IIf(Trim(Fields.[Unit]) = \"g\", (Fields.Tara * 1000), (Fields.Tara))  + \' \' + Fie" +
    "lds.[Unit]";
            // 
            // ReportFooterArea1
            // 
            this.ReportFooterArea1.Height = Telerik.Reporting.Drawing.Unit.Cm(2.93D);
            this.ReportFooterArea1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.Text10,
            this.Text11,
            this.Druckdatum1,
            this.Text3});
            this.ReportFooterArea1.KeepTogether = false;
            this.ReportFooterArea1.Name = "ReportFooterArea1";
            this.ReportFooterArea1.PageBreak = Telerik.Reporting.PageBreak.After;
            this.ReportFooterArea1.Style.BackgroundColor = System.Drawing.Color.White;
            this.ReportFooterArea1.Style.Visible = true;
            // 
            // Text10
            // 
            this.Text10.CanGrow = false;
            this.Text10.CanShrink = false;
            this.Text10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.686D), Telerik.Reporting.Drawing.Unit.Cm(0.423D));
            this.Text10.Name = "Text10";
            this.Text10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(22.908D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Text10.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text10.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text10.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text10.Style.Color = System.Drawing.Color.Black;
            this.Text10.Style.Font.Bold = false;
            this.Text10.Style.Font.Italic = false;
            this.Text10.Style.Font.Name = "Arial";
            this.Text10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text10.Style.Font.Strikeout = false;
            this.Text10.Style.Font.Underline = false;
            this.Text10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text10.Style.Visible = true;
            this.Text10.Value = "Bemerkung: _________________________________________________________";
            // 
            // Text11
            // 
            this.Text11.CanGrow = false;
            this.Text11.CanShrink = false;
            this.Text11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.686D), Telerik.Reporting.Drawing.Unit.Cm(1.27D));
            this.Text11.Name = "Text11";
            this.Text11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(22.908D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Text11.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text11.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text11.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text11.Style.Color = System.Drawing.Color.Black;
            this.Text11.Style.Font.Bold = false;
            this.Text11.Style.Font.Italic = false;
            this.Text11.Style.Font.Name = "Arial";
            this.Text11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text11.Style.Font.Strikeout = false;
            this.Text11.Style.Font.Underline = false;
            this.Text11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text11.Style.Visible = true;
            this.Text11.Value = "Visum:         _________________________________________________________";
            // 
            // Druckdatum1
            // 
            this.Druckdatum1.CanGrow = false;
            this.Druckdatum1.CanShrink = false;
            this.Druckdatum1.Format = "";
            this.Druckdatum1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.401D), Telerik.Reporting.Drawing.Unit.Cm(2.117D));
            this.Druckdatum1.Name = "Druckdatum1";
            this.Druckdatum1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.099D), Telerik.Reporting.Drawing.Unit.Cm(0.474D));
            this.Druckdatum1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Druckdatum1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Druckdatum1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Druckdatum1.Style.Color = System.Drawing.Color.Black;
            this.Druckdatum1.Style.Font.Bold = false;
            this.Druckdatum1.Style.Font.Italic = false;
            this.Druckdatum1.Style.Font.Name = "Arial";
            this.Druckdatum1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Druckdatum1.Style.Font.Strikeout = false;
            this.Druckdatum1.Style.Font.Underline = false;
            this.Druckdatum1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Druckdatum1.Style.Visible = true;
            this.Druckdatum1.Value = "=Format(\"{0:d} {0:T}\",Now(),Now())";
            // 
            // Text3
            // 
            this.Text3.CanGrow = false;
            this.Text3.CanShrink = false;
            this.Text3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.686D), Telerik.Reporting.Drawing.Unit.Cm(2.117D));
            this.Text3.Name = "Text3";
            this.Text3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.277D), Telerik.Reporting.Drawing.Unit.Cm(0.474D));
            this.Text3.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text3.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text3.Style.Color = System.Drawing.Color.Black;
            this.Text3.Style.Font.Bold = false;
            this.Text3.Style.Font.Italic = false;
            this.Text3.Style.Font.Name = "Arial";
            this.Text3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text3.Style.Font.Strikeout = false;
            this.Text3.Style.Font.Underline = false;
            this.Text3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text3.Style.Visible = true;
            this.Text3.Value = "Erstellt am";
            // 
            // PageFooterArea1
            // 
            this.PageFooterArea1.Height = Telerik.Reporting.Drawing.Unit.Cm(1.058D);
            this.PageFooterArea1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox3,
            this.SeiteNvonM1,
            this.textBox2});
            this.PageFooterArea1.Name = "PageFooterArea1";
            this.PageFooterArea1.PrintOnFirstPage = true;
            this.PageFooterArea1.PrintOnLastPage = true;
            this.PageFooterArea1.Style.BackgroundColor = System.Drawing.Color.White;
            this.PageFooterArea1.Style.Visible = true;
            // 
            // textBox3
            // 
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.1D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.638D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.textBox3.Value = "Created with Telerik Reports  3.44";
            // 
            // SeiteNvonM1
            // 
            this.SeiteNvonM1.CanGrow = false;
            this.SeiteNvonM1.CanShrink = false;
            this.SeiteNvonM1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.SeiteNvonM1.Name = "SeiteNvonM1";
            this.SeiteNvonM1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.505D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.SeiteNvonM1.Style.BackgroundColor = System.Drawing.Color.White;
            this.SeiteNvonM1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.SeiteNvonM1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.SeiteNvonM1.Style.Color = System.Drawing.Color.Black;
            this.SeiteNvonM1.Style.Font.Bold = false;
            this.SeiteNvonM1.Style.Font.Italic = false;
            this.SeiteNvonM1.Style.Font.Name = "Arial";
            this.SeiteNvonM1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.SeiteNvonM1.Style.Font.Strikeout = false;
            this.SeiteNvonM1.Style.Font.Underline = false;
            this.SeiteNvonM1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.SeiteNvonM1.Style.Visible = true;
            this.SeiteNvonM1.Value = "=\"Seite \" + PageNumber + \" von \" + PageCount";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(27.088D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.61D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.textBox2.Value = "Ver . 1.0";
            // 
            // WJournal
            // 
            group1.GroupFooter = this.GroupFooterArea1;
            group1.GroupHeader = this.GroupHeaderArea1;
            group1.Groupings.Add(new Telerik.Reporting.Grouping("=Fields.[ANR]"));
            group1.Sortings.Add(new Telerik.Reporting.Sorting("=Fields.[ANr]", Telerik.Reporting.SortDirection.Asc));
            this.Groups.AddRange(new Telerik.Reporting.Group[] {
            group1});
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.GroupHeaderArea1,
            this.GroupFooterArea1,
            this.ReportHeaderArea1,
            this.PageHeaderArea1,
            this.DetailArea1,
            this.ReportFooterArea1,
            this.PageFooterArea1});
            this.Name = "WJour";
            this.PageSettings.Landscape = true;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Cm(0.501D), Telerik.Reporting.Drawing.Unit.Cm(0.501D), Telerik.Reporting.Drawing.Unit.Cm(1.501D), Telerik.Reporting.Drawing.Unit.Cm(0.42D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Cm(28.698D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.ReportHeaderSection ReportHeaderArea1;
        private Telerik.Reporting.PageHeaderSection PageHeaderArea1;
        private Telerik.Reporting.Shape Line1;
        private Telerik.Reporting.TextBox Seitenzahl1;
        private Telerik.Reporting.TextBox Text2;
        private Telerik.Reporting.TextBox Text9;
        private Telerik.Reporting.TextBox Text12;
        private Telerik.Reporting.TextBox Text15;
        private Telerik.Reporting.TextBox Text13;
        private Telerik.Reporting.TextBox Text14;
        private Telerik.Reporting.TextBox Text8;
        private Telerik.Reporting.TextBox Text7;
        private Telerik.Reporting.TextBox Text6;
        private Telerik.Reporting.TextBox Text4;
        private Telerik.Reporting.TextBox Text5;
        private Telerik.Reporting.DetailSection DetailArea1;
        private Telerik.Reporting.TextBox Zeit1;
        private Telerik.Reporting.TextBox Lot1;
        private Telerik.Reporting.TextBox ANr1;
        private Telerik.Reporting.TextBox Name1;
        private Telerik.Reporting.TextBox BArt1;
        private Telerik.Reporting.TextBox Name2;
        private Telerik.Reporting.TextBox Tara1;
        private Telerik.Reporting.TextBox Brutto1;
        private Telerik.Reporting.TextBox Name3;
        private Telerik.Reporting.TextBox ArtNr1;
        private Telerik.Reporting.TextBox Netto1;
        private Telerik.Reporting.TextBox totalNetto1;
        private Telerik.Reporting.ReportFooterSection ReportFooterArea1;
        private Telerik.Reporting.TextBox Text10;
        private Telerik.Reporting.TextBox Text11;
        private Telerik.Reporting.TextBox Druckdatum1;
        private Telerik.Reporting.TextBox Text3;
        private Telerik.Reporting.PageFooterSection PageFooterArea1;
        private Telerik.Reporting.GroupHeaderSection GroupHeaderArea1;
        private Telerik.Reporting.TextBox GroupNameZeitdaily1;
        private Telerik.Reporting.GroupFooterSection GroupFooterArea1;
        private Telerik.Reporting.TextBox RTotal01;
        private Telerik.Reporting.PictureBox Picture1;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox SeiteNvonM1;
        private Telerik.Reporting.TextBox textBox2;
    }
}