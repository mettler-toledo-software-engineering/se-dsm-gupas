namespace MT.GUPAS.Reporting
{
    partial class WProto
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.TextBox Text11;
            Telerik.Reporting.Drawing.FormattingRule formattingRule1 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.TextBox Text37;
            Telerik.Reporting.Drawing.FormattingRule formattingRule2 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.InstanceReportSource instanceReportSource1 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.Drawing.FormattingRule formattingRule3 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule4 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule5 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule6 = new Telerik.Reporting.Drawing.FormattingRule();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WProto));
            Telerik.Reporting.Drawing.FormattingRule formattingRule7 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule8 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.wSubProto2 = new MT.GUPAS.Reporting.WSubProto();
            this.ReportHeaderArea1 = new Telerik.Reporting.ReportHeaderSection();
            this.DetailArea1 = new Telerik.Reporting.DetailSection();
            this.Line1 = new Telerik.Reporting.Shape();
            this.Tag1 = new Telerik.Reporting.TextBox();
            this.Name4 = new Telerik.Reporting.TextBox();
            this.Name2 = new Telerik.Reporting.TextBox();
            this.Subreport1 = new Telerik.Reporting.SubReport();
            this.Text27 = new Telerik.Reporting.TextBox();
            this.Text28 = new Telerik.Reporting.TextBox();
            this.Zeit1 = new Telerik.Reporting.TextBox();
            this.VonEti1 = new Telerik.Reporting.TextBox();
            this.BisEti1 = new Telerik.Reporting.TextBox();
            this.Text29 = new Telerik.Reporting.TextBox();
            this.ZielLot1 = new Telerik.Reporting.TextBox();
            this.KorrNetto1 = new Telerik.Reporting.TextBox();
            this.Unit1 = new Telerik.Reporting.TextBox();
            this.Unit3 = new Telerik.Reporting.TextBox();
            this.KorrSoll1 = new Telerik.Reporting.TextBox();
            this.KorrTara1 = new Telerik.Reporting.TextBox();
            this.Unit4 = new Telerik.Reporting.TextBox();
            this.KorrBrutto1 = new Telerik.Reporting.TextBox();
            this.Nummer1 = new Telerik.Reporting.TextBox();
            this.hugo1 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.ReportFooterArea1 = new Telerik.Reporting.ReportFooterSection();
            this.Text16 = new Telerik.Reporting.TextBox();
            this.Text17 = new Telerik.Reporting.TextBox();
            this.Line2 = new Telerik.Reporting.Shape();
            this.Text25 = new Telerik.Reporting.TextBox();
            this.Lot2 = new Telerik.Reporting.TextBox();
            this.Text26 = new Telerik.Reporting.TextBox();
            this.RTotal01 = new Telerik.Reporting.TextBox();
            this.PageFooterArea1 = new Telerik.Reporting.PageFooterSection();
            this.SeiteNvonM1 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.PageHeaderArea1 = new Telerik.Reporting.PageHeaderSection();
            this.Text2 = new Telerik.Reporting.TextBox();
            this.Text1 = new Telerik.Reporting.TextBox();
            this.Text3 = new Telerik.Reporting.TextBox();
            this.Text4 = new Telerik.Reporting.TextBox();
            this.Text5 = new Telerik.Reporting.TextBox();
            this.Text6 = new Telerik.Reporting.TextBox();
            this.Text7 = new Telerik.Reporting.TextBox();
            this.Text8 = new Telerik.Reporting.TextBox();
            this.Name1 = new Telerik.Reporting.TextBox();
            this.ArtNr1 = new Telerik.Reporting.TextBox();
            this.GenesysNr1 = new Telerik.Reporting.TextBox();
            this.Text9 = new Telerik.Reporting.TextBox();
            this.Text10 = new Telerik.Reporting.TextBox();
            this.Besteller1 = new Telerik.Reporting.TextBox();
            this.Text20 = new Telerik.Reporting.TextBox();
            this.ANr1 = new Telerik.Reporting.TextBox();
            this.ZeitNew1 = new Telerik.Reporting.TextBox();
            this.Text22 = new Telerik.Reporting.TextBox();
            this.Text36 = new Telerik.Reporting.TextBox();
            this.Text35 = new Telerik.Reporting.TextBox();
            this.Lot1 = new Telerik.Reporting.TextBox();
            this.BArt1 = new Telerik.Reporting.TextBox();
            this.Text24 = new Telerik.Reporting.TextBox();
            this.Text30 = new Telerik.Reporting.TextBox();
            this.Text33 = new Telerik.Reporting.TextBox();
            this.Text34 = new Telerik.Reporting.TextBox();
            this.Text38 = new Telerik.Reporting.TextBox();
            this.Name3 = new Telerik.Reporting.TextBox();
            this.Text13 = new Telerik.Reporting.TextBox();
            this.Picture1 = new Telerik.Reporting.PictureBox();
            this.shape1 = new Telerik.Reporting.Shape();
            this.shape2 = new Telerik.Reporting.Shape();
            this.wTextBox = new Telerik.Reporting.TextBox();
            this.tara2TextBox = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.objectDataSource1 = new Telerik.Reporting.ObjectDataSource();
            Text11 = new Telerik.Reporting.TextBox();
            Text37 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.wSubProto2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Text11
            // 
            Text11.CanGrow = false;
            Text11.CanShrink = false;
            formattingRule1.Filters.Add(new Telerik.Reporting.Filter("=Fields.AnzZeilen", Telerik.Reporting.FilterOperator.Equal, "=0"));
            formattingRule1.Style.Visible = false;
            Text11.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule1});
            Text11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(23.072D), Telerik.Reporting.Drawing.Unit.Cm(4.207D));
            Text11.Name = "Text11";
            Text11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.332D), Telerik.Reporting.Drawing.Unit.Cm(0.476D));
            Text11.Style.BackgroundColor = System.Drawing.Color.White;
            Text11.Style.BorderColor.Default = System.Drawing.Color.Black;
            Text11.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            Text11.Style.Color = System.Drawing.Color.Black;
            Text11.Style.Font.Bold = false;
            Text11.Style.Font.Italic = false;
            Text11.Style.Font.Name = "Arial";
            Text11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            Text11.Style.Font.Strikeout = false;
            Text11.Style.Font.Underline = false;
            Text11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            Text11.Style.Visible = true;
            Text11.Value = "Teilprotokoll !!";
            // 
            // Text37
            // 
            Text37.CanGrow = false;
            Text37.CanShrink = false;
            formattingRule2.Filters.Add(new Telerik.Reporting.Filter("=Fields.Status", Telerik.Reporting.FilterOperator.Equal, "=1"));
            formattingRule2.Style.Visible = false;
            Text37.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule2});
            Text37.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.025D), Telerik.Reporting.Drawing.Unit.Cm(2.033D));
            Text37.Name = "Text37";
            Text37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(25.532D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            Text37.Style.BackgroundColor = System.Drawing.Color.White;
            Text37.Style.BorderColor.Default = System.Drawing.Color.Black;
            Text37.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            Text37.Style.Color = System.Drawing.Color.Black;
            Text37.Style.Font.Bold = false;
            Text37.Style.Font.Italic = false;
            Text37.Style.Font.Name = "Arial";
            Text37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            Text37.Style.Font.Strikeout = false;
            Text37.Style.Font.Underline = false;
            Text37.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            Text37.Style.Visible = true;
            Text37.Value = "Unregelmässigkeit ! Grund:_______________________________________________________" +
    "________ Visum:__________\n";
            // 
            // wSubProto2
            // 
            this.wSubProto2.Name = "WSubProto";
            // 
            // ReportHeaderArea1
            // 
            this.ReportHeaderArea1.Height = Telerik.Reporting.Drawing.Unit.Cm(0.132D);
            this.ReportHeaderArea1.KeepTogether = false;
            this.ReportHeaderArea1.Name = "ReportHeaderArea1";
            this.ReportHeaderArea1.PageBreak = Telerik.Reporting.PageBreak.Before;
            this.ReportHeaderArea1.Style.BackgroundColor = System.Drawing.Color.White;
            this.ReportHeaderArea1.Style.Visible = true;
            // 
            // DetailArea1
            // 
            this.DetailArea1.CanShrink = true;
            this.DetailArea1.Height = Telerik.Reporting.Drawing.Unit.Cm(4.368D);
            this.DetailArea1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.Line1,
            this.Tag1,
            this.Name4,
            this.Name2,
            this.Subreport1,
            this.Text27,
            this.Text28,
            Text37,
            this.Zeit1,
            this.VonEti1,
            this.BisEti1,
            this.Text29,
            this.ZielLot1,
            this.KorrNetto1,
            this.Unit1,
            this.Unit3,
            this.KorrSoll1,
            this.KorrTara1,
            this.Unit4,
            this.KorrBrutto1,
            this.Nummer1,
            this.hugo1,
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.textBox1,
            this.textBox7,
            this.textBox10});
            this.DetailArea1.KeepTogether = true;
            this.DetailArea1.Name = "DetailArea1";
            this.DetailArea1.PageBreak = Telerik.Reporting.PageBreak.None;
            this.DetailArea1.Style.BackgroundColor = System.Drawing.Color.White;
            this.DetailArea1.Style.Visible = true;
            // 
            // Line1
            // 
            this.Line1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.635D), Telerik.Reporting.Drawing.Unit.Cm(0.148D));
            this.Line1.Name = "Line1";
            this.Line1.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            this.Line1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(27.702D), Telerik.Reporting.Drawing.Unit.Point(4D));
            this.Line1.Style.Color = System.Drawing.Color.Black;
            this.Line1.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.Line1.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.Line1.Style.Visible = true;
            // 
            // Tag1
            // 
            this.Tag1.CanGrow = false;
            this.Tag1.CanShrink = false;
            this.Tag1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(26.389D), Telerik.Reporting.Drawing.Unit.Cm(0.503D));
            this.Tag1.Name = "Tag1";
            this.Tag1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.948D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Tag1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Tag1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Tag1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Tag1.Style.Color = System.Drawing.Color.Black;
            this.Tag1.Style.Font.Bold = false;
            this.Tag1.Style.Font.Italic = false;
            this.Tag1.Style.Font.Name = "Arial";
            this.Tag1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Tag1.Style.Font.Strikeout = false;
            this.Tag1.Style.Font.Underline = false;
            this.Tag1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Tag1.Style.Visible = true;
            this.Tag1.Value = "=Fields.[Tag]";
            // 
            // Name4
            // 
            this.Name4.CanGrow = false;
            this.Name4.CanShrink = false;
            this.Name4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(24.129D), Telerik.Reporting.Drawing.Unit.Cm(0.503D));
            this.Name4.Name = "Name4";
            this.Name4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.956D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Name4.Style.BackgroundColor = System.Drawing.Color.White;
            this.Name4.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Name4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Name4.Style.Color = System.Drawing.Color.Black;
            this.Name4.Style.Font.Bold = false;
            this.Name4.Style.Font.Italic = false;
            this.Name4.Style.Font.Name = "Arial";
            this.Name4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Name4.Style.Font.Strikeout = false;
            this.Name4.Style.Font.Underline = false;
            this.Name4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Name4.Style.Visible = true;
            this.Name4.Value = "=Fields.[WName]";
            // 
            // Name2
            // 
            this.Name2.CanGrow = false;
            this.Name2.CanShrink = false;
            this.Name2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(21.519D), Telerik.Reporting.Drawing.Unit.Cm(0.503D));
            this.Name2.Name = "Name2";
            this.Name2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.371D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Name2.Style.BackgroundColor = System.Drawing.Color.White;
            this.Name2.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Name2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Name2.Style.Color = System.Drawing.Color.Black;
            this.Name2.Style.Font.Bold = false;
            this.Name2.Style.Font.Italic = false;
            this.Name2.Style.Font.Name = "Arial";
            this.Name2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Name2.Style.Font.Strikeout = false;
            this.Name2.Style.Font.Underline = false;
            this.Name2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Name2.Style.Visible = true;
            this.Name2.Value = "=Trim(Fields.[RName])";
            // 
            // Subreport1
            // 
            this.Subreport1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.5D), Telerik.Reporting.Drawing.Unit.Cm(2.668D));
            this.Subreport1.Name = "Subreport1";
            instanceReportSource1.Parameters.Add(new Telerik.Reporting.Parameter("DetailNummer", "=Fields.[DNummer]"));
            instanceReportSource1.Parameters.Add(new Telerik.Reporting.Parameter("UOM", "=Fields.[Unit]"));
            instanceReportSource1.ReportDocument = this.wSubProto2;
            this.Subreport1.ReportSource = instanceReportSource1;
            this.Subreport1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.196D), Telerik.Reporting.Drawing.Unit.Cm(1.7D));
            this.Subreport1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Subreport1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Subreport1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.Subreport1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Subreport1.Style.Visible = true;
            // 
            // Text27
            // 
            this.Text27.CanGrow = false;
            this.Text27.CanShrink = false;
            this.Text27.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.069D), Telerik.Reporting.Drawing.Unit.Cm(2.974D));
            this.Text27.Name = "Text27";
            this.Text27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.599D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Text27.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text27.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text27.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text27.Style.Color = System.Drawing.Color.Black;
            this.Text27.Style.Font.Bold = false;
            this.Text27.Style.Font.Italic = false;
            this.Text27.Style.Font.Name = "Arial";
            this.Text27.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text27.Style.Font.Strikeout = false;
            this.Text27.Style.Font.Underline = false;
            this.Text27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text27.Style.Visible = true;
            this.Text27.Value = "Etikette-Nr.";
            // 
            // Text28
            // 
            this.Text28.CanGrow = false;
            this.Text28.CanShrink = false;
            this.Text28.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.468D), Telerik.Reporting.Drawing.Unit.Cm(2.974D));
            this.Text28.Name = "Text28";
            this.Text28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Text28.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text28.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text28.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text28.Style.Color = System.Drawing.Color.Black;
            this.Text28.Style.Font.Bold = false;
            this.Text28.Style.Font.Italic = false;
            this.Text28.Style.Font.Name = "Arial";
            this.Text28.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text28.Style.Font.Strikeout = false;
            this.Text28.Style.Font.Underline = false;
            this.Text28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text28.Style.Visible = true;
            this.Text28.Value = "bis";
            // 
            // Zeit1
            // 
            this.Zeit1.CanGrow = false;
            this.Zeit1.CanShrink = false;
            this.Zeit1.Format = "{0:G}";
            this.Zeit1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.516D), Telerik.Reporting.Drawing.Unit.Cm(0.503D));
            this.Zeit1.Name = "Zeit1";
            this.Zeit1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.894D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Zeit1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Zeit1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Zeit1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Zeit1.Style.Color = System.Drawing.Color.Black;
            this.Zeit1.Style.Font.Bold = false;
            this.Zeit1.Style.Font.Italic = false;
            this.Zeit1.Style.Font.Name = "Arial";
            this.Zeit1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Zeit1.Style.Font.Strikeout = false;
            this.Zeit1.Style.Font.Underline = false;
            this.Zeit1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Zeit1.Style.Visible = true;
            this.Zeit1.Value = "= Fields.Zeit";
            // 
            // VonEti1
            // 
            this.VonEti1.CanGrow = false;
            this.VonEti1.CanShrink = false;
            this.VonEti1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.668D), Telerik.Reporting.Drawing.Unit.Cm(2.974D));
            this.VonEti1.Name = "VonEti1";
            this.VonEti1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.236D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.VonEti1.Style.BackgroundColor = System.Drawing.Color.White;
            this.VonEti1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.VonEti1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.VonEti1.Style.Color = System.Drawing.Color.Black;
            this.VonEti1.Style.Font.Bold = false;
            this.VonEti1.Style.Font.Italic = false;
            this.VonEti1.Style.Font.Name = "Arial";
            this.VonEti1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.VonEti1.Style.Font.Strikeout = false;
            this.VonEti1.Style.Font.Underline = false;
            this.VonEti1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.VonEti1.Style.Visible = true;
            this.VonEti1.Value = "=Fields.[VonEti]";
            // 
            // BisEti1
            // 
            this.BisEti1.CanGrow = false;
            this.BisEti1.CanShrink = false;
            this.BisEti1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(19.468D), Telerik.Reporting.Drawing.Unit.Cm(2.974D));
            this.BisEti1.Name = "BisEti1";
            this.BisEti1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.942D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.BisEti1.Style.BackgroundColor = System.Drawing.Color.White;
            this.BisEti1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.BisEti1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.BisEti1.Style.Color = System.Drawing.Color.Black;
            this.BisEti1.Style.Font.Bold = false;
            this.BisEti1.Style.Font.Italic = false;
            this.BisEti1.Style.Font.Name = "Arial";
            this.BisEti1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.BisEti1.Style.Font.Strikeout = false;
            this.BisEti1.Style.Font.Underline = false;
            this.BisEti1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.BisEti1.Style.Visible = true;
            this.BisEti1.Value = "=Fields.[BisEti]";
            // 
            // Text29
            // 
            this.Text29.CanGrow = false;
            this.Text29.CanShrink = false;
            this.Text29.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.1D), Telerik.Reporting.Drawing.Unit.Cm(0.503D));
            this.Text29.Name = "Text29";
            this.Text29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.455D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Text29.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text29.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text29.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text29.Style.Color = System.Drawing.Color.Black;
            this.Text29.Style.Font.Bold = false;
            this.Text29.Style.Font.Italic = false;
            this.Text29.Style.Font.Name = "Arial";
            this.Text29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text29.Style.Font.Strikeout = false;
            this.Text29.Style.Font.Underline = false;
            this.Text29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text29.Style.Visible = true;
            this.Text29.Value = "=IIf(Fields.Hand = 1, \"H\", \" \")";
            // 
            // ZielLot1
            // 
            this.ZielLot1.CanGrow = false;
            this.ZielLot1.CanShrink = false;
            this.ZielLot1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.025D), Telerik.Reporting.Drawing.Unit.Cm(0.529D));
            this.ZielLot1.Name = "ZielLot1";
            this.ZielLot1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.531D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.ZielLot1.Style.BackgroundColor = System.Drawing.Color.White;
            this.ZielLot1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.ZielLot1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.ZielLot1.Style.Color = System.Drawing.Color.Black;
            this.ZielLot1.Style.Font.Bold = false;
            this.ZielLot1.Style.Font.Italic = false;
            this.ZielLot1.Style.Font.Name = "Arial";
            this.ZielLot1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.ZielLot1.Style.Font.Strikeout = false;
            this.ZielLot1.Style.Font.Underline = false;
            this.ZielLot1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.ZielLot1.Style.Visible = true;
            this.ZielLot1.Value = "=Fields.[Lot]";
            // 
            // KorrNetto1
            // 
            this.KorrNetto1.Bindings.Add(new Telerik.Reporting.Binding("Format", "=\"{0:N\" + Fields.Precision + \"}\""));
            this.KorrNetto1.CanGrow = false;
            this.KorrNetto1.CanShrink = false;
            this.KorrNetto1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.527D), Telerik.Reporting.Drawing.Unit.Cm(0.503D));
            this.KorrNetto1.Name = "KorrNetto1";
            this.KorrNetto1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.844D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.KorrNetto1.Style.BackgroundColor = System.Drawing.Color.White;
            this.KorrNetto1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.KorrNetto1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.KorrNetto1.Style.Color = System.Drawing.Color.Black;
            this.KorrNetto1.Style.Font.Bold = false;
            this.KorrNetto1.Style.Font.Italic = false;
            this.KorrNetto1.Style.Font.Name = "Arial";
            this.KorrNetto1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.KorrNetto1.Style.Font.Strikeout = false;
            this.KorrNetto1.Style.Font.Underline = false;
            this.KorrNetto1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.KorrNetto1.Style.Visible = true;
            this.KorrNetto1.Value = "=IIf(Trim(Fields.Unit) = \"g\", (Fields.Netto * 1000), (Fields.Netto))";
            // 
            // Unit1
            // 
            this.Unit1.CanGrow = false;
            this.Unit1.CanShrink = false;
            this.Unit1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.371D), Telerik.Reporting.Drawing.Unit.Cm(0.503D));
            this.Unit1.Name = "Unit1";
            this.Unit1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.929D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Unit1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Unit1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Unit1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Unit1.Style.Color = System.Drawing.Color.Black;
            this.Unit1.Style.Font.Bold = false;
            this.Unit1.Style.Font.Italic = false;
            this.Unit1.Style.Font.Name = "Arial";
            this.Unit1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Unit1.Style.Font.Strikeout = false;
            this.Unit1.Style.Font.Underline = false;
            this.Unit1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Unit1.Style.Visible = true;
            this.Unit1.Value = "=Fields.[Unit]";
            // 
            // Unit3
            // 
            this.Unit3.CanGrow = false;
            this.Unit3.CanShrink = false;
            this.Unit3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.143D), Telerik.Reporting.Drawing.Unit.Cm(0.503D));
            this.Unit3.Name = "Unit3";
            this.Unit3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.957D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Unit3.Style.BackgroundColor = System.Drawing.Color.White;
            this.Unit3.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Unit3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Unit3.Style.Color = System.Drawing.Color.Black;
            this.Unit3.Style.Font.Bold = false;
            this.Unit3.Style.Font.Italic = false;
            this.Unit3.Style.Font.Name = "Arial";
            this.Unit3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Unit3.Style.Font.Strikeout = false;
            this.Unit3.Style.Font.Underline = false;
            this.Unit3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Unit3.Style.Visible = true;
            this.Unit3.Value = "=Fields.[Unit]";
            // 
            // KorrSoll1
            // 
            this.KorrSoll1.Bindings.Add(new Telerik.Reporting.Binding("Format", "=\"{0:N\" + Fields.Precision + \"}\""));
            this.KorrSoll1.CanGrow = false;
            this.KorrSoll1.CanShrink = false;
            this.KorrSoll1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.609D), Telerik.Reporting.Drawing.Unit.Cm(0.503D));
            this.KorrSoll1.Name = "KorrSoll1";
            this.KorrSoll1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.534D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.KorrSoll1.Style.BackgroundColor = System.Drawing.Color.White;
            this.KorrSoll1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.KorrSoll1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.KorrSoll1.Style.Color = System.Drawing.Color.Black;
            this.KorrSoll1.Style.Font.Bold = false;
            this.KorrSoll1.Style.Font.Italic = false;
            this.KorrSoll1.Style.Font.Name = "Arial";
            this.KorrSoll1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.KorrSoll1.Style.Font.Strikeout = false;
            this.KorrSoll1.Style.Font.Underline = false;
            this.KorrSoll1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.KorrSoll1.Style.Visible = true;
            this.KorrSoll1.Value = "=IIf(Trim(Fields.Unit) = \"g\", (Fields.Soll * 1000), (Fields.Soll))";
            // 
            // KorrTara1
            // 
            this.KorrTara1.Bindings.Add(new Telerik.Reporting.Binding("Format", "=\"{0:N\" + Fields.Precision + \"}\""));
            this.KorrTara1.CanGrow = false;
            this.KorrTara1.CanShrink = false;
            this.KorrTara1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.3D), Telerik.Reporting.Drawing.Unit.Cm(0.503D));
            this.KorrTara1.Name = "KorrTara1";
            this.KorrTara1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.961D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.KorrTara1.Style.BackgroundColor = System.Drawing.Color.White;
            this.KorrTara1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.KorrTara1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.KorrTara1.Style.Color = System.Drawing.Color.Black;
            this.KorrTara1.Style.Font.Bold = false;
            this.KorrTara1.Style.Font.Italic = false;
            this.KorrTara1.Style.Font.Name = "Arial";
            this.KorrTara1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.KorrTara1.Style.Font.Strikeout = false;
            this.KorrTara1.Style.Font.Underline = false;
            this.KorrTara1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.KorrTara1.Style.Visible = true;
            this.KorrTara1.Value = "=IIf(Trim(Fields.Unit) = \"g\", (Fields.Tara * 1000), (Fields.Tara))";
            // 
            // Unit4
            // 
            this.Unit4.CanGrow = false;
            this.Unit4.CanShrink = false;
            this.Unit4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.587D), Telerik.Reporting.Drawing.Unit.Cm(0.503D));
            this.Unit4.Name = "Unit4";
            this.Unit4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.739D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Unit4.Style.BackgroundColor = System.Drawing.Color.White;
            this.Unit4.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Unit4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Unit4.Style.Color = System.Drawing.Color.Black;
            this.Unit4.Style.Font.Bold = false;
            this.Unit4.Style.Font.Italic = false;
            this.Unit4.Style.Font.Name = "Arial";
            this.Unit4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Unit4.Style.Font.Strikeout = false;
            this.Unit4.Style.Font.Underline = false;
            this.Unit4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Unit4.Style.Visible = true;
            this.Unit4.Value = "=Fields.[Unit]";
            // 
            // KorrBrutto1
            // 
            this.KorrBrutto1.Bindings.Add(new Telerik.Reporting.Binding("Format", "=\"{0:N\" + Fields.Precision + \"}\""));
            this.KorrBrutto1.CanGrow = false;
            this.KorrBrutto1.CanShrink = false;
            this.KorrBrutto1.Format = "";
            this.KorrBrutto1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.56D), Telerik.Reporting.Drawing.Unit.Cm(0.503D));
            this.KorrBrutto1.Name = "KorrBrutto1";
            this.KorrBrutto1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.911D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.KorrBrutto1.Style.BackgroundColor = System.Drawing.Color.White;
            this.KorrBrutto1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.KorrBrutto1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.KorrBrutto1.Style.Color = System.Drawing.Color.Black;
            this.KorrBrutto1.Style.Font.Bold = false;
            this.KorrBrutto1.Style.Font.Italic = false;
            this.KorrBrutto1.Style.Font.Name = "Arial";
            this.KorrBrutto1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.KorrBrutto1.Style.Font.Strikeout = false;
            this.KorrBrutto1.Style.Font.Underline = false;
            this.KorrBrutto1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.KorrBrutto1.Style.Visible = true;
            this.KorrBrutto1.Value = "=IIf(Trim(Fields.Unit) = \"g\", (Fields.Brutto * 1000), (Fields.Brutto))";
            // 
            // Nummer1
            // 
            this.Nummer1.CanGrow = false;
            this.Nummer1.CanShrink = false;
            this.Nummer1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.425D), Telerik.Reporting.Drawing.Unit.Cm(1.164D));
            this.Nummer1.Name = "Nummer1";
            this.Nummer1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.386D), Telerik.Reporting.Drawing.Unit.Cm(0.487D));
            this.Nummer1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Nummer1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Nummer1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Nummer1.Style.Color = System.Drawing.Color.Black;
            this.Nummer1.Style.Font.Bold = false;
            this.Nummer1.Style.Font.Italic = false;
            this.Nummer1.Style.Font.Name = "Arial";
            this.Nummer1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Nummer1.Style.Font.Strikeout = false;
            this.Nummer1.Style.Font.Underline = false;
            this.Nummer1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.Nummer1.Style.Visible = true;
            this.Nummer1.Value = "=Fields.[DNummer]";
            // 
            // hugo1
            // 
            this.hugo1.CanGrow = false;
            this.hugo1.CanShrink = false;
            this.hugo1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.329D), Telerik.Reporting.Drawing.Unit.Cm(0.529D));
            this.hugo1.Name = "hugo1";
            this.hugo1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.482D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.hugo1.Style.BackgroundColor = System.Drawing.Color.White;
            this.hugo1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.hugo1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.hugo1.Style.Color = System.Drawing.Color.Black;
            this.hugo1.Style.Font.Bold = false;
            this.hugo1.Style.Font.Italic = false;
            this.hugo1.Style.Font.Name = "Arial";
            this.hugo1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.hugo1.Style.Font.Strikeout = false;
            this.hugo1.Style.Font.Underline = false;
            this.hugo1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.hugo1.Style.Visible = true;
            this.hugo1.Value = "= RowNumber()";
            // 
            // textBox4
            // 
            this.textBox4.CanGrow = false;
            this.textBox4.CanShrink = true;
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.261D), Telerik.Reporting.Drawing.Unit.Cm(0.529D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.739D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.textBox4.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox4.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox4.Style.Color = System.Drawing.Color.Black;
            this.textBox4.Style.Font.Bold = false;
            this.textBox4.Style.Font.Italic = false;
            this.textBox4.Style.Font.Name = "Arial";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox4.Style.Font.Strikeout = false;
            this.textBox4.Style.Font.Underline = false;
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox4.Style.Visible = true;
            this.textBox4.Value = "=Fields.[Unit]";
            // 
            // textBox5
            // 
            this.textBox5.Bindings.Add(new Telerik.Reporting.Binding("Format", "=\"{0:N\" + Fields.Precision + \"}\""));
            this.textBox5.CanGrow = false;
            this.textBox5.CanShrink = false;
            formattingRule3.Filters.Add(new Telerik.Reporting.Filter("= Fields.Tara2", Telerik.Reporting.FilterOperator.Equal, "=0"));
            formattingRule3.Style.Visible = false;
            this.textBox5.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule3});
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.3D), Telerik.Reporting.Drawing.Unit.Cm(1.164D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.961D), Telerik.Reporting.Drawing.Unit.Cm(0.487D));
            this.textBox5.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox5.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox5.Style.Color = System.Drawing.Color.Black;
            this.textBox5.Style.Font.Bold = false;
            this.textBox5.Style.Font.Italic = false;
            this.textBox5.Style.Font.Name = "Arial";
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox5.Style.Font.Strikeout = false;
            this.textBox5.Style.Font.Underline = false;
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox5.Style.Visible = true;
            this.textBox5.Value = "=IIf(Trim(Fields.Unit) = \"g\", (Fields.Tara2 * 1000), (Fields.Tara2))";
            // 
            // textBox6
            // 
            this.textBox6.CanGrow = false;
            this.textBox6.CanShrink = true;
            formattingRule4.Filters.Add(new Telerik.Reporting.Filter("= Fields.Tara2", Telerik.Reporting.FilterOperator.Equal, "= 0"));
            formattingRule4.Style.Visible = false;
            this.textBox6.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule4});
            this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.261D), Telerik.Reporting.Drawing.Unit.Cm(1.164D));
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.739D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.textBox6.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox6.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox6.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox6.Style.Color = System.Drawing.Color.Black;
            this.textBox6.Style.Font.Bold = false;
            this.textBox6.Style.Font.Italic = false;
            this.textBox6.Style.Font.Name = "Arial";
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox6.Style.Font.Strikeout = false;
            this.textBox6.Style.Font.Underline = false;
            this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox6.Style.Visible = true;
            this.textBox6.Value = "=Fields.[Unit]";
            // 
            // textBox1
            // 
            this.textBox1.CanGrow = false;
            this.textBox1.CanShrink = true;
            formattingRule5.Filters.Add(new Telerik.Reporting.Filter("= Fields.Tara2", Telerik.Reporting.FilterOperator.Equal, "= 0"));
            formattingRule5.Style.Visible = false;
            this.textBox1.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule5});
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.587D), Telerik.Reporting.Drawing.Unit.Cm(1.164D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.739D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.textBox1.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox1.Style.Color = System.Drawing.Color.Black;
            this.textBox1.Style.Font.Bold = false;
            this.textBox1.Style.Font.Italic = false;
            this.textBox1.Style.Font.Name = "Arial";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox1.Style.Font.Strikeout = false;
            this.textBox1.Style.Font.Underline = false;
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox1.Style.Visible = true;
            this.textBox1.Value = "=Fields.[Unit]";
            // 
            // textBox7
            // 
            this.textBox7.Bindings.Add(new Telerik.Reporting.Binding("Format", "=\"{0:N\" + Fields.Precision + \"}\""));
            this.textBox7.CanGrow = false;
            this.textBox7.CanShrink = false;
            formattingRule6.Filters.Add(new Telerik.Reporting.Filter("= Fields.Tara2", Telerik.Reporting.FilterOperator.Equal, "=0"));
            formattingRule6.Style.Visible = false;
            this.textBox7.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule6});
            this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.56D), Telerik.Reporting.Drawing.Unit.Cm(1.164D));
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.911D), Telerik.Reporting.Drawing.Unit.Cm(0.487D));
            this.textBox7.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox7.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox7.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox7.Style.Color = System.Drawing.Color.Black;
            this.textBox7.Style.Font.Bold = false;
            this.textBox7.Style.Font.Italic = false;
            this.textBox7.Style.Font.Name = "Arial";
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox7.Style.Font.Strikeout = false;
            this.textBox7.Style.Font.Underline = false;
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox7.Style.Visible = true;
            this.textBox7.Value = "=IIf(Trim(Fields.Unit) = \"g\", ((Fields.Tara + Fields.Netto) * 1000), (Fields.Tara" +
    " + Fields.Netto))";
            // 
            // textBox10
            // 
            this.textBox10.CanGrow = false;
            this.textBox10.CanShrink = false;
            this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.069D), Telerik.Reporting.Drawing.Unit.Cm(3.609D));
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.865D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.textBox10.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox10.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox10.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox10.Style.Color = System.Drawing.Color.Black;
            this.textBox10.Style.Font.Bold = false;
            this.textBox10.Style.Font.Italic = false;
            this.textBox10.Style.Font.Name = "Arial";
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox10.Style.Font.Strikeout = false;
            this.textBox10.Style.Font.Underline = false;
            this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox10.Style.Visible = true;
            this.textBox10.Value = "Plomben-Nr.:";
            // 
            // ReportFooterArea1
            // 
            this.ReportFooterArea1.Height = Telerik.Reporting.Drawing.Unit.Cm(2.558D);
            this.ReportFooterArea1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.Text16,
            this.Text17,
            this.Line2,
            this.Text25,
            this.Lot2,
            this.Text26,
            this.RTotal01});
            this.ReportFooterArea1.KeepTogether = false;
            this.ReportFooterArea1.Name = "ReportFooterArea1";
            this.ReportFooterArea1.PageBreak = Telerik.Reporting.PageBreak.After;
            this.ReportFooterArea1.Style.BackgroundColor = System.Drawing.Color.White;
            this.ReportFooterArea1.Style.Visible = true;
            // 
            // Text16
            // 
            this.Text16.CanGrow = false;
            this.Text16.CanShrink = false;
            this.Text16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.635D), Telerik.Reporting.Drawing.Unit.Cm(1.923D));
            this.Text16.Name = "Text16";
            this.Text16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.832D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Text16.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text16.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text16.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text16.Style.Color = System.Drawing.Color.Black;
            this.Text16.Style.Font.Bold = false;
            this.Text16.Style.Font.Italic = false;
            this.Text16.Style.Font.Name = "Arial";
            this.Text16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text16.Style.Font.Strikeout = false;
            this.Text16.Style.Font.Underline = false;
            this.Text16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text16.Style.Visible = true;
            this.Text16.Value = "Datum:__________________________";
            // 
            // Text17
            // 
            this.Text17.CanGrow = false;
            this.Text17.CanShrink = false;
            this.Text17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.102D), Telerik.Reporting.Drawing.Unit.Cm(1.923D));
            this.Text17.Name = "Text17";
            this.Text17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.832D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Text17.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text17.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text17.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text17.Style.Color = System.Drawing.Color.Black;
            this.Text17.Style.Font.Bold = false;
            this.Text17.Style.Font.Italic = false;
            this.Text17.Style.Font.Name = "Arial";
            this.Text17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text17.Style.Font.Strikeout = false;
            this.Text17.Style.Font.Underline = false;
            this.Text17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text17.Style.Visible = true;
            this.Text17.Value = "Visum:__________________________";
            // 
            // Line2
            // 
            this.Line2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.466D), Telerik.Reporting.Drawing.Unit.Cm(0.282D));
            this.Line2.Name = "Line2";
            this.Line2.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            this.Line2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(27.279D), Telerik.Reporting.Drawing.Unit.Point(3.75D));
            this.Line2.Style.Color = System.Drawing.Color.Black;
            this.Line2.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.Line2.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.Line2.Style.Visible = true;
            // 
            // Text25
            // 
            this.Text25.CanGrow = false;
            this.Text25.CanShrink = false;
            this.Text25.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.91D), Telerik.Reporting.Drawing.Unit.Cm(0.658D));
            this.Text25.Name = "Text25";
            this.Text25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.646D), Telerik.Reporting.Drawing.Unit.Cm(0.847D));
            this.Text25.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text25.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text25.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text25.Style.Color = System.Drawing.Color.Black;
            this.Text25.Style.Font.Bold = true;
            this.Text25.Style.Font.Italic = false;
            this.Text25.Style.Font.Name = "Arial";
            this.Text25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text25.Style.Font.Strikeout = false;
            this.Text25.Style.Font.Underline = false;
            this.Text25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text25.Style.Visible = true;
            this.Text25.Value = "Total Lot-Nr.";
            // 
            // Lot2
            // 
            this.Lot2.CanGrow = false;
            this.Lot2.CanShrink = false;
            this.Lot2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.609D), Telerik.Reporting.Drawing.Unit.Cm(0.658D));
            this.Lot2.Name = "Lot2";
            this.Lot2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.918D), Telerik.Reporting.Drawing.Unit.Cm(0.847D));
            this.Lot2.Style.BackgroundColor = System.Drawing.Color.White;
            this.Lot2.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Lot2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Lot2.Style.Color = System.Drawing.Color.Black;
            this.Lot2.Style.Font.Bold = true;
            this.Lot2.Style.Font.Italic = false;
            this.Lot2.Style.Font.Name = "Arial";
            this.Lot2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Lot2.Style.Font.Strikeout = false;
            this.Lot2.Style.Font.Underline = false;
            this.Lot2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Lot2.Style.Visible = true;
            this.Lot2.Value = "=Fields.[Lot]";
            // 
            // Text26
            // 
            this.Text26.CanGrow = false;
            this.Text26.CanShrink = false;
            this.Text26.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.158D), Telerik.Reporting.Drawing.Unit.Cm(0.658D));
            this.Text26.Name = "Text26";
            this.Text26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.459D), Telerik.Reporting.Drawing.Unit.Cm(0.847D));
            this.Text26.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text26.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text26.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text26.Style.Color = System.Drawing.Color.Black;
            this.Text26.Style.Font.Bold = true;
            this.Text26.Style.Font.Italic = false;
            this.Text26.Style.Font.Name = "Arial";
            this.Text26.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text26.Style.Font.Strikeout = false;
            this.Text26.Style.Font.Underline = false;
            this.Text26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text26.Style.Visible = true;
            this.Text26.Value = "kg";
            // 
            // RTotal01
            // 
            this.RTotal01.CanGrow = false;
            this.RTotal01.CanShrink = false;
            this.RTotal01.Format = "{0:N4}";
            this.RTotal01.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.527D), Telerik.Reporting.Drawing.Unit.Cm(0.658D));
            this.RTotal01.Name = "RTotal01";
            this.RTotal01.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.351D), Telerik.Reporting.Drawing.Unit.Cm(0.499D));
            this.RTotal01.Style.BackgroundColor = System.Drawing.Color.White;
            this.RTotal01.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.RTotal01.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.RTotal01.Style.Color = System.Drawing.Color.Black;
            this.RTotal01.Style.Font.Bold = true;
            this.RTotal01.Style.Font.Italic = false;
            this.RTotal01.Style.Font.Name = "Arial";
            this.RTotal01.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.RTotal01.Style.Font.Strikeout = false;
            this.RTotal01.Style.Font.Underline = false;
            this.RTotal01.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.RTotal01.Style.Visible = true;
            this.RTotal01.Value = "= Sum(Fields.Netto)";
            // 
            // PageFooterArea1
            // 
            this.PageFooterArea1.Height = Telerik.Reporting.Drawing.Unit.Cm(1.169D);
            this.PageFooterArea1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.SeiteNvonM1,
            this.textBox2,
            this.textBox3});
            this.PageFooterArea1.Name = "PageFooterArea1";
            this.PageFooterArea1.PrintOnFirstPage = true;
            this.PageFooterArea1.PrintOnLastPage = true;
            this.PageFooterArea1.Style.BackgroundColor = System.Drawing.Color.White;
            this.PageFooterArea1.Style.Visible = true;
            // 
            // SeiteNvonM1
            // 
            this.SeiteNvonM1.CanGrow = false;
            this.SeiteNvonM1.CanShrink = false;
            this.SeiteNvonM1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.SeiteNvonM1.Name = "SeiteNvonM1";
            this.SeiteNvonM1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.505D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.SeiteNvonM1.Style.BackgroundColor = System.Drawing.Color.White;
            this.SeiteNvonM1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.SeiteNvonM1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.SeiteNvonM1.Style.Color = System.Drawing.Color.Black;
            this.SeiteNvonM1.Style.Font.Bold = false;
            this.SeiteNvonM1.Style.Font.Italic = false;
            this.SeiteNvonM1.Style.Font.Name = "Arial";
            this.SeiteNvonM1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.SeiteNvonM1.Style.Font.Strikeout = false;
            this.SeiteNvonM1.Style.Font.Underline = false;
            this.SeiteNvonM1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.SeiteNvonM1.Style.Visible = true;
            this.SeiteNvonM1.Value = "=\"Seite \" + PageNumber + \" von \" + PageCount";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(27.1D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.61D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.textBox2.Value = "Ver . 1.1";
            // 
            // textBox3
            // 
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.539D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.638D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.textBox3.Value = "Created with Telerik Reports  3.44";
            // 
            // PageHeaderArea1
            // 
            this.PageHeaderArea1.Height = Telerik.Reporting.Drawing.Unit.Cm(6.9D);
            this.PageHeaderArea1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.Text2,
            this.Text1,
            this.Text3,
            this.Text4,
            this.Text5,
            this.Text6,
            this.Text7,
            this.Text8,
            this.Name1,
            this.ArtNr1,
            this.GenesysNr1,
            this.Text9,
            this.Text10,
            this.Besteller1,
            this.Text20,
            this.ANr1,
            this.ZeitNew1,
            this.Text22,
            this.Text36,
            this.Text35,
            this.Lot1,
            this.BArt1,
            this.Text24,
            this.Text30,
            this.Text33,
            this.Text34,
            this.Text38,
            Text11,
            this.Name3,
            this.Text13,
            this.Picture1,
            this.shape1,
            this.shape2,
            this.wTextBox,
            this.tara2TextBox,
            this.textBox8,
            this.textBox9});
            this.PageHeaderArea1.Name = "PageHeaderArea1";
            this.PageHeaderArea1.PrintOnFirstPage = true;
            this.PageHeaderArea1.PrintOnLastPage = true;
            this.PageHeaderArea1.Style.BackgroundColor = System.Drawing.Color.White;
            this.PageHeaderArea1.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.PageHeaderArea1.Style.Visible = true;
            // 
            // Text2
            // 
            this.Text2.CanGrow = false;
            this.Text2.CanShrink = false;
            this.Text2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.635D), Telerik.Reporting.Drawing.Unit.Cm(1.058D));
            this.Text2.Name = "Text2";
            this.Text2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.54D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Text2.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text2.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text2.Style.Color = System.Drawing.Color.Black;
            this.Text2.Style.Font.Bold = false;
            this.Text2.Style.Font.Italic = false;
            this.Text2.Style.Font.Name = "Arial";
            this.Text2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text2.Style.Font.Strikeout = false;
            this.Text2.Style.Font.Underline = false;
            this.Text2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text2.Style.Visible = true;
            this.Text2.Value = "Auftrags-Nr:";
            // 
            // Text1
            // 
            this.Text1.CanGrow = false;
            this.Text1.CanShrink = false;
            this.Text1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.635D), Telerik.Reporting.Drawing.Unit.Cm(0.212D));
            this.Text1.Name = "Text1";
            this.Text1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.043D), Telerik.Reporting.Drawing.Unit.Cm(0.568D));
            this.Text1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text1.Style.Color = System.Drawing.Color.Black;
            this.Text1.Style.Font.Bold = true;
            this.Text1.Style.Font.Italic = false;
            this.Text1.Style.Font.Name = "Arial";
            this.Text1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.Text1.Style.Font.Strikeout = false;
            this.Text1.Style.Font.Underline = true;
            this.Text1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text1.Style.Visible = true;
            this.Text1.Value = "DSM-DNP AG Sisseln \nRocheAG, Sisseln GUPAS\n";
            // 
            // Text3
            // 
            this.Text3.CanGrow = false;
            this.Text3.CanShrink = false;
            this.Text3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.635D), Telerik.Reporting.Drawing.Unit.Cm(1.693D));
            this.Text3.Name = "Text3";
            this.Text3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.54D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Text3.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text3.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text3.Style.Color = System.Drawing.Color.Black;
            this.Text3.Style.Font.Bold = false;
            this.Text3.Style.Font.Italic = false;
            this.Text3.Style.Font.Name = "Arial";
            this.Text3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text3.Style.Font.Strikeout = false;
            this.Text3.Style.Font.Underline = false;
            this.Text3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text3.Style.Visible = true;
            this.Text3.Value = "Auftragsart:";
            // 
            // Text4
            // 
            this.Text4.CanGrow = false;
            this.Text4.CanShrink = false;
            this.Text4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.102D), Telerik.Reporting.Drawing.Unit.Cm(2.328D));
            this.Text4.Name = "Text4";
            this.Text4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.905D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Text4.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text4.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text4.Style.Color = System.Drawing.Color.Black;
            this.Text4.Style.Font.Bold = false;
            this.Text4.Style.Font.Italic = false;
            this.Text4.Style.Font.Name = "Arial";
            this.Text4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text4.Style.Font.Strikeout = false;
            this.Text4.Style.Font.Underline = false;
            this.Text4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text4.Style.Visible = true;
            this.Text4.Value = "Besteller";
            // 
            // Text5
            // 
            this.Text5.CanGrow = false;
            this.Text5.CanShrink = false;
            this.Text5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(22.4D), Telerik.Reporting.Drawing.Unit.Cm(5.768D));
            this.Text5.Name = "Text5";
            this.Text5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.49D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Text5.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text5.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text5.Style.Color = System.Drawing.Color.Black;
            this.Text5.Style.Font.Bold = false;
            this.Text5.Style.Font.Italic = false;
            this.Text5.Style.Font.Name = "Arial";
            this.Text5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text5.Style.Font.Strikeout = false;
            this.Text5.Style.Font.Underline = false;
            this.Text5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text5.Style.Visible = true;
            this.Text5.Value = "Kabine";
            // 
            // Text6
            // 
            this.Text6.CanGrow = false;
            this.Text6.CanShrink = false;
            this.Text6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.635D), Telerik.Reporting.Drawing.Unit.Cm(2.328D));
            this.Text6.Name = "Text6";
            this.Text6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.752D), Telerik.Reporting.Drawing.Unit.Cm(0.423D));
            this.Text6.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text6.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text6.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text6.Style.Color = System.Drawing.Color.Black;
            this.Text6.Style.Font.Bold = false;
            this.Text6.Style.Font.Italic = false;
            this.Text6.Style.Font.Name = "Arial";
            this.Text6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text6.Style.Font.Strikeout = false;
            this.Text6.Style.Font.Underline = false;
            this.Text6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text6.Style.Visible = true;
            this.Text6.Value = "MG Name:";
            // 
            // Text7
            // 
            this.Text7.CanGrow = false;
            this.Text7.CanShrink = false;
            this.Text7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.635D), Telerik.Reporting.Drawing.Unit.Cm(3.598D));
            this.Text7.Name = "Text7";
            this.Text7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.117D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Text7.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text7.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text7.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text7.Style.Color = System.Drawing.Color.Black;
            this.Text7.Style.Font.Bold = false;
            this.Text7.Style.Font.Italic = false;
            this.Text7.Style.Font.Name = "Arial";
            this.Text7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text7.Style.Font.Strikeout = false;
            this.Text7.Style.Font.Underline = false;
            this.Text7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text7.Style.Visible = true;
            this.Text7.Value = "MG Art-Nr.";
            // 
            // Text8
            // 
            this.Text8.CanGrow = false;
            this.Text8.CanShrink = false;
            this.Text8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.635D), Telerik.Reporting.Drawing.Unit.Cm(2.963D));
            this.Text8.Name = "Text8";
            this.Text8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.387D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Text8.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text8.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text8.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text8.Style.Color = System.Drawing.Color.Black;
            this.Text8.Style.Font.Bold = false;
            this.Text8.Style.Font.Italic = false;
            this.Text8.Style.Font.Name = "Arial";
            this.Text8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text8.Style.Font.Strikeout = false;
            this.Text8.Style.Font.Underline = false;
            this.Text8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text8.Style.Visible = true;
            this.Text8.Value = "MG Genesys-Nr";
            // 
            // Name1
            // 
            this.Name1.CanGrow = false;
            this.Name1.CanShrink = false;
            this.Name1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.022D), Telerik.Reporting.Drawing.Unit.Cm(2.328D));
            this.Name1.Name = "Name1";
            this.Name1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.346D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Name1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Name1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Name1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Name1.Style.Color = System.Drawing.Color.Black;
            this.Name1.Style.Font.Bold = false;
            this.Name1.Style.Font.Italic = false;
            this.Name1.Style.Font.Name = "Arial";
            this.Name1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Name1.Style.Font.Strikeout = false;
            this.Name1.Style.Font.Underline = false;
            this.Name1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Name1.Style.Visible = true;
            this.Name1.Value = "=Fields.[KName]";
            // 
            // ArtNr1
            // 
            this.ArtNr1.CanGrow = false;
            this.ArtNr1.CanShrink = false;
            this.ArtNr1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.022D), Telerik.Reporting.Drawing.Unit.Cm(3.598D));
            this.ArtNr1.Name = "ArtNr1";
            this.ArtNr1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.505D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.ArtNr1.Style.BackgroundColor = System.Drawing.Color.White;
            this.ArtNr1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.ArtNr1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.ArtNr1.Style.Color = System.Drawing.Color.Black;
            this.ArtNr1.Style.Font.Bold = false;
            this.ArtNr1.Style.Font.Italic = false;
            this.ArtNr1.Style.Font.Name = "Arial";
            this.ArtNr1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.ArtNr1.Style.Font.Strikeout = false;
            this.ArtNr1.Style.Font.Underline = false;
            this.ArtNr1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.ArtNr1.Style.Visible = true;
            this.ArtNr1.Value = "=Fields.[ArtNr]";
            // 
            // GenesysNr1
            // 
            this.GenesysNr1.CanGrow = false;
            this.GenesysNr1.CanShrink = false;
            this.GenesysNr1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.022D), Telerik.Reporting.Drawing.Unit.Cm(2.963D));
            this.GenesysNr1.Name = "GenesysNr1";
            this.GenesysNr1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.505D), Telerik.Reporting.Drawing.Unit.Cm(0.423D));
            this.GenesysNr1.Style.BackgroundColor = System.Drawing.Color.White;
            this.GenesysNr1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.GenesysNr1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.GenesysNr1.Style.Color = System.Drawing.Color.Black;
            this.GenesysNr1.Style.Font.Bold = false;
            this.GenesysNr1.Style.Font.Italic = false;
            this.GenesysNr1.Style.Font.Name = "Arial";
            this.GenesysNr1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.GenesysNr1.Style.Font.Strikeout = false;
            this.GenesysNr1.Style.Font.Underline = false;
            this.GenesysNr1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.GenesysNr1.Style.Visible = true;
            this.GenesysNr1.Value = "=Fields.[GenesysNr]";
            // 
            // Text9
            // 
            this.Text9.CanGrow = false;
            this.Text9.CanShrink = false;
            this.Text9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.102D), Telerik.Reporting.Drawing.Unit.Cm(4.233D));
            this.Text9.Name = "Text9";
            this.Text9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.54D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Text9.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text9.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text9.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text9.Style.Color = System.Drawing.Color.Black;
            this.Text9.Style.Font.Bold = false;
            this.Text9.Style.Font.Italic = false;
            this.Text9.Style.Font.Name = "Arial";
            this.Text9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text9.Style.Font.Strikeout = false;
            this.Text9.Style.Font.Underline = false;
            this.Text9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text9.Style.Visible = true;
            this.Text9.Value = "Erstellt am";
            // 
            // Text10
            // 
            this.Text10.CanGrow = false;
            this.Text10.CanShrink = false;
            this.Text10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.635D), Telerik.Reporting.Drawing.Unit.Cm(5.08D));
            this.Text10.Name = "Text10";
            this.Text10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.043D), Telerik.Reporting.Drawing.Unit.Cm(0.568D));
            this.Text10.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text10.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text10.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text10.Style.Color = System.Drawing.Color.Black;
            this.Text10.Style.Font.Bold = false;
            this.Text10.Style.Font.Italic = false;
            this.Text10.Style.Font.Name = "Arial";
            this.Text10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text10.Style.Font.Strikeout = false;
            this.Text10.Style.Font.Underline = true;
            this.Text10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text10.Style.Visible = true;
            this.Text10.Value = "Wiegeaufträge\nRocheAG, Sisseln GUPAS\n";
            // 
            // Besteller1
            // 
            this.Besteller1.CanGrow = false;
            this.Besteller1.CanShrink = false;
            this.Besteller1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.649D), Telerik.Reporting.Drawing.Unit.Cm(2.328D));
            this.Besteller1.Name = "Besteller1";
            this.Besteller1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.505D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.Besteller1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Besteller1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Besteller1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Besteller1.Style.Color = System.Drawing.Color.Black;
            this.Besteller1.Style.Font.Bold = false;
            this.Besteller1.Style.Font.Italic = false;
            this.Besteller1.Style.Font.Name = "Arial";
            this.Besteller1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Besteller1.Style.Font.Strikeout = false;
            this.Besteller1.Style.Font.Underline = false;
            this.Besteller1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Besteller1.Style.Visible = true;
            this.Besteller1.Value = "=Fields.[Besteller]";
            // 
            // Text20
            // 
            this.Text20.CanGrow = false;
            this.Text20.CanShrink = false;
            this.Text20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.102D), Telerik.Reporting.Drawing.Unit.Cm(2.963D));
            this.Text20.Name = "Text20";
            this.Text20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.905D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Text20.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text20.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text20.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text20.Style.Color = System.Drawing.Color.Black;
            this.Text20.Style.Font.Bold = false;
            this.Text20.Style.Font.Italic = false;
            this.Text20.Style.Font.Name = "Arial";
            this.Text20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text20.Style.Font.Strikeout = false;
            this.Text20.Style.Font.Underline = false;
            this.Text20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text20.Style.Visible = true;
            this.Text20.Value = "Ersteller";
            // 
            // ANr1
            // 
            this.ANr1.CanGrow = false;
            this.ANr1.CanShrink = false;
            this.ANr1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.022D), Telerik.Reporting.Drawing.Unit.Cm(1.058D));
            this.ANr1.Name = "ANr1";
            this.ANr1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.505D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.ANr1.Style.BackgroundColor = System.Drawing.Color.White;
            this.ANr1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.ANr1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.ANr1.Style.Color = System.Drawing.Color.Black;
            this.ANr1.Style.Font.Bold = false;
            this.ANr1.Style.Font.Italic = false;
            this.ANr1.Style.Font.Name = "Arial";
            this.ANr1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.ANr1.Style.Font.Strikeout = false;
            this.ANr1.Style.Font.Underline = false;
            this.ANr1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.ANr1.Style.Visible = true;
            this.ANr1.Value = "=Fields.[ANr]";
            // 
            // ZeitNew1
            // 
            this.ZeitNew1.CanGrow = false;
            this.ZeitNew1.CanShrink = false;
            this.ZeitNew1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.724D), Telerik.Reporting.Drawing.Unit.Cm(4.233D));
            this.ZeitNew1.Name = "ZeitNew1";
            this.ZeitNew1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.944D), Telerik.Reporting.Drawing.Unit.Cm(0.487D));
            this.ZeitNew1.Style.BackgroundColor = System.Drawing.Color.White;
            this.ZeitNew1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.ZeitNew1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.ZeitNew1.Style.Color = System.Drawing.Color.Black;
            this.ZeitNew1.Style.Font.Bold = false;
            this.ZeitNew1.Style.Font.Italic = false;
            this.ZeitNew1.Style.Font.Name = "Arial";
            this.ZeitNew1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.ZeitNew1.Style.Font.Strikeout = false;
            this.ZeitNew1.Style.Font.Underline = false;
            this.ZeitNew1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.ZeitNew1.Style.Visible = true;
            this.ZeitNew1.Value = "=Fields.[ZeitNew]";
            // 
            // Text22
            // 
            this.Text22.CanGrow = false;
            this.Text22.CanShrink = false;
            this.Text22.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.635D), Telerik.Reporting.Drawing.Unit.Cm(4.233D));
            this.Text22.Name = "Text22";
            this.Text22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.117D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Text22.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text22.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text22.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text22.Style.Color = System.Drawing.Color.Black;
            this.Text22.Style.Font.Bold = false;
            this.Text22.Style.Font.Italic = false;
            this.Text22.Style.Font.Name = "Arial";
            this.Text22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text22.Style.Font.Strikeout = false;
            this.Text22.Style.Font.Underline = false;
            this.Text22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text22.Style.Visible = true;
            this.Text22.Value = "MG Lot-Nr.";
            // 
            // Text36
            // 
            this.Text36.CanGrow = false;
            this.Text36.CanShrink = false;
            this.Text36.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(26.364D), Telerik.Reporting.Drawing.Unit.Cm(5.768D));
            this.Text36.Name = "Text36";
            this.Text36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.142D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Text36.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text36.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text36.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text36.Style.Color = System.Drawing.Color.Black;
            this.Text36.Style.Font.Bold = false;
            this.Text36.Style.Font.Italic = false;
            this.Text36.Style.Font.Name = "Arial";
            this.Text36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text36.Style.Font.Strikeout = false;
            this.Text36.Style.Font.Underline = false;
            this.Text36.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text36.Style.Visible = true;
            this.Text36.Value = "Verwieger";
            // 
            // Text35
            // 
            this.Text35.CanGrow = false;
            this.Text35.CanShrink = false;
            this.Text35.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(24.5D), Telerik.Reporting.Drawing.Unit.Cm(5.768D));
            this.Text35.Name = "Text35";
            this.Text35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.585D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Text35.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text35.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text35.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text35.Style.Color = System.Drawing.Color.Black;
            this.Text35.Style.Font.Bold = false;
            this.Text35.Style.Font.Italic = false;
            this.Text35.Style.Font.Name = "Arial";
            this.Text35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text35.Style.Font.Strikeout = false;
            this.Text35.Style.Font.Underline = false;
            this.Text35.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text35.Style.Visible = true;
            this.Text35.Value = "Waage";
            // 
            // Lot1
            // 
            this.Lot1.CanGrow = false;
            this.Lot1.CanShrink = false;
            this.Lot1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.022D), Telerik.Reporting.Drawing.Unit.Cm(4.233D));
            this.Lot1.Name = "Lot1";
            this.Lot1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.505D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Lot1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Lot1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Lot1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Lot1.Style.Color = System.Drawing.Color.Black;
            this.Lot1.Style.Font.Bold = false;
            this.Lot1.Style.Font.Italic = false;
            this.Lot1.Style.Font.Name = "Arial";
            this.Lot1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Lot1.Style.Font.Strikeout = false;
            this.Lot1.Style.Font.Underline = false;
            this.Lot1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Lot1.Style.Visible = true;
            this.Lot1.Value = "=Fields.[Lot]";
            // 
            // BArt1
            // 
            this.BArt1.CanGrow = false;
            this.BArt1.CanShrink = false;
            this.BArt1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.022D), Telerik.Reporting.Drawing.Unit.Cm(1.693D));
            this.BArt1.Name = "BArt1";
            this.BArt1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.054D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.BArt1.Style.BackgroundColor = System.Drawing.Color.White;
            this.BArt1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.BArt1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.BArt1.Style.Color = System.Drawing.Color.Black;
            this.BArt1.Style.Font.Bold = false;
            this.BArt1.Style.Font.Italic = false;
            this.BArt1.Style.Font.Name = "Arial";
            this.BArt1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.BArt1.Style.Font.Strikeout = false;
            this.BArt1.Style.Font.Underline = false;
            this.BArt1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.BArt1.Style.Visible = true;
            this.BArt1.Value = "=Fields.[BArt]";
            // 
            // Text24
            // 
            this.Text24.CanGrow = false;
            this.Text24.CanShrink = false;
            this.Text24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18D), Telerik.Reporting.Drawing.Unit.Cm(5.768D));
            this.Text24.Name = "Text24";
            this.Text24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.884D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Text24.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text24.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text24.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text24.Style.Color = System.Drawing.Color.Black;
            this.Text24.Style.Font.Bold = false;
            this.Text24.Style.Font.Italic = false;
            this.Text24.Style.Font.Name = "Arial";
            this.Text24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text24.Style.Font.Strikeout = false;
            this.Text24.Style.Font.Underline = false;
            this.Text24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text24.Style.Visible = true;
            this.Text24.Value = "Zeit";
            // 
            // Text30
            // 
            this.Text30.CanGrow = false;
            this.Text30.CanShrink = false;
            this.Text30.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.025D), Telerik.Reporting.Drawing.Unit.Cm(5.768D));
            this.Text30.Name = "Text30";
            this.Text30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.997D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Text30.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text30.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text30.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text30.Style.Color = System.Drawing.Color.Black;
            this.Text30.Style.Font.Bold = false;
            this.Text30.Style.Font.Italic = false;
            this.Text30.Style.Font.Name = "Arial";
            this.Text30.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text30.Style.Font.Strikeout = false;
            this.Text30.Style.Font.Underline = false;
            this.Text30.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Text30.Style.Visible = true;
            this.Text30.Value = "Ziel-Lot";
            // 
            // Text33
            // 
            this.Text33.CanGrow = false;
            this.Text33.CanShrink = false;
            this.Text33.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.984D), Telerik.Reporting.Drawing.Unit.Cm(5.768D));
            this.Text33.Name = "Text33";
            this.Text33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.159D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Text33.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text33.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text33.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text33.Style.Color = System.Drawing.Color.Black;
            this.Text33.Style.Font.Bold = false;
            this.Text33.Style.Font.Italic = false;
            this.Text33.Style.Font.Name = "Arial";
            this.Text33.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text33.Style.Font.Strikeout = false;
            this.Text33.Style.Font.Underline = false;
            this.Text33.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.Text33.Style.Visible = true;
            this.Text33.Value = "Soll";
            // 
            // Text34
            // 
            this.Text34.CanGrow = false;
            this.Text34.CanShrink = false;
            this.Text34.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.835D), Telerik.Reporting.Drawing.Unit.Cm(5.768D));
            this.Text34.Name = "Text34";
            this.Text34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.536D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Text34.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text34.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text34.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text34.Style.Color = System.Drawing.Color.Black;
            this.Text34.Style.Font.Bold = false;
            this.Text34.Style.Font.Italic = false;
            this.Text34.Style.Font.Name = "Arial";
            this.Text34.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text34.Style.Font.Strikeout = false;
            this.Text34.Style.Font.Underline = false;
            this.Text34.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.Text34.Style.Visible = true;
            this.Text34.Value = "Netto";
            // 
            // Text38
            // 
            this.Text38.CanGrow = false;
            this.Text38.CanShrink = false;
            this.Text38.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.9D), Telerik.Reporting.Drawing.Unit.Cm(5.768D));
            this.Text38.Name = "Text38";
            this.Text38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Text38.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text38.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text38.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text38.Style.Color = System.Drawing.Color.Black;
            this.Text38.Style.Font.Bold = false;
            this.Text38.Style.Font.Italic = false;
            this.Text38.Style.Font.Name = "Arial";
            this.Text38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text38.Style.Font.Strikeout = false;
            this.Text38.Style.Font.Underline = false;
            this.Text38.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.Text38.Style.Visible = true;
            this.Text38.TextWrap = true;
            this.Text38.Value = "Tara Gebinde";
            // 
            // Name3
            // 
            this.Name3.CanGrow = false;
            this.Name3.CanShrink = false;
            this.Name3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.649D), Telerik.Reporting.Drawing.Unit.Cm(2.963D));
            this.Name3.Name = "Name3";
            this.Name3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.505D), Telerik.Reporting.Drawing.Unit.Cm(0.529D));
            this.Name3.Style.BackgroundColor = System.Drawing.Color.White;
            this.Name3.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Name3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Name3.Style.Color = System.Drawing.Color.Black;
            this.Name3.Style.Font.Bold = false;
            this.Name3.Style.Font.Italic = false;
            this.Name3.Style.Font.Name = "Arial";
            this.Name3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Name3.Style.Font.Strikeout = false;
            this.Name3.Style.Font.Underline = false;
            this.Name3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Name3.Style.Visible = true;
            this.Name3.Value = "=Fields.[Name]";
            // 
            // Text13
            // 
            this.Text13.CanGrow = false;
            this.Text13.CanShrink = false;
            this.Text13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.676D), Telerik.Reporting.Drawing.Unit.Cm(5.768D));
            this.Text13.Name = "Text13";
            this.Text13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.84D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.Text13.Style.BackgroundColor = System.Drawing.Color.White;
            this.Text13.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Text13.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Text13.Style.Color = System.Drawing.Color.Black;
            this.Text13.Style.Font.Bold = false;
            this.Text13.Style.Font.Italic = false;
            this.Text13.Style.Font.Name = "Arial";
            this.Text13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.Text13.Style.Font.Strikeout = false;
            this.Text13.Style.Font.Underline = false;
            this.Text13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.Text13.Style.Visible = true;
            this.Text13.Value = "Gesamtbrutto";
            // 
            // Picture1
            // 
            this.Picture1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(21.802D), Telerik.Reporting.Drawing.Unit.Cm(0.212D));
            this.Picture1.MimeType = "";
            this.Picture1.Name = "Picture1";
            this.Picture1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.704D), Telerik.Reporting.Drawing.Unit.Cm(2.751D));
            this.Picture1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.Stretch;
            this.Picture1.Style.BackgroundColor = System.Drawing.Color.White;
            this.Picture1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.Picture1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Picture1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Picture1.Style.Visible = true;
            this.Picture1.Value = ((object)(resources.GetObject("Picture1.Value")));
            // 
            // shape1
            // 
            this.shape1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(4.939D));
            this.shape1.Name = "shape1";
            this.shape1.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            this.shape1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(28.71D), Telerik.Reporting.Drawing.Unit.Point(4D));
            this.shape1.Style.Color = System.Drawing.Color.Black;
            this.shape1.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.shape1.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.shape1.Style.Visible = true;
            // 
            // shape2
            // 
            this.shape2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.shape2.Name = "shape2";
            this.shape2.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            this.shape2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(28.71D), Telerik.Reporting.Drawing.Unit.Point(4D));
            this.shape2.Style.Color = System.Drawing.Color.Black;
            this.shape2.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.shape2.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.shape2.Style.Visible = true;
            // 
            // wTextBox
            // 
            this.wTextBox.CanGrow = false;
            this.wTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.425D), Telerik.Reporting.Drawing.Unit.Cm(6.2D));
            this.wTextBox.Name = "wTextBox";
            this.wTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.228D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.wTextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.wTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.wTextBox.Style.Visible = true;
            this.wTextBox.Value = "W-Nr.";
            // 
            // tara2TextBox
            // 
            this.tara2TextBox.CanGrow = false;
            this.tara2TextBox.CanShrink = false;
            formattingRule7.Filters.Add(new Telerik.Reporting.Filter("= Sum(Fields.Tara2)", Telerik.Reporting.FilterOperator.Equal, "=0"));
            formattingRule7.Style.Visible = false;
            this.tara2TextBox.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule7});
            this.tara2TextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.6D), Telerik.Reporting.Drawing.Unit.Cm(6.2D));
            this.tara2TextBox.Name = "tara2TextBox";
            this.tara2TextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.4D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.tara2TextBox.Style.BackgroundColor = System.Drawing.Color.White;
            this.tara2TextBox.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.tara2TextBox.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.tara2TextBox.Style.Color = System.Drawing.Color.Black;
            this.tara2TextBox.Style.Font.Bold = false;
            this.tara2TextBox.Style.Font.Italic = false;
            this.tara2TextBox.Style.Font.Name = "Arial";
            this.tara2TextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.tara2TextBox.Style.Font.Strikeout = false;
            this.tara2TextBox.Style.Font.Underline = false;
            this.tara2TextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.tara2TextBox.Style.Visible = true;
            this.tara2TextBox.TextWrap = true;
            this.tara2TextBox.Value = "Tara2";
            // 
            // textBox8
            // 
            this.textBox8.CanGrow = false;
            this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.425D), Telerik.Reporting.Drawing.Unit.Cm(5.768D));
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.228D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox8.Style.Visible = true;
            this.textBox8.Value = "Nr.";
            // 
            // textBox9
            // 
            this.textBox9.CanGrow = false;
            this.textBox9.CanShrink = false;
            formattingRule8.Filters.Add(new Telerik.Reporting.Filter("= Sum(Fields.Tara2)", Telerik.Reporting.FilterOperator.Equal, "=0"));
            formattingRule8.Style.Visible = false;
            this.textBox9.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule8});
            this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.676D), Telerik.Reporting.Drawing.Unit.Cm(6.2D));
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.84D), Telerik.Reporting.Drawing.Unit.Cm(0.39D));
            this.textBox9.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox9.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox9.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox9.Style.Color = System.Drawing.Color.Black;
            this.textBox9.Style.Font.Bold = false;
            this.textBox9.Style.Font.Italic = false;
            this.textBox9.Style.Font.Name = "Arial";
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox9.Style.Font.Strikeout = false;
            this.textBox9.Style.Font.Underline = false;
            this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox9.Style.Visible = true;
            this.textBox9.TextWrap = true;
            this.textBox9.Value = "Brutto2 ";
            // 
            // objectDataSource1
            // 
            this.objectDataSource1.DataSource = typeof(MT.GUPAS.Models.ProtoModel);
            this.objectDataSource1.Name = "objectDataSource1";
            // 
            // WProto
            // 
            this.DataSource = this.objectDataSource1;
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.ReportHeaderArea1,
            this.PageHeaderArea1,
            this.DetailArea1,
            this.ReportFooterArea1,
            this.PageFooterArea1});
            this.Name = "WProto";
            this.PageSettings.Landscape = true;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Cm(0.57D), Telerik.Reporting.Drawing.Unit.Cm(0.42D), Telerik.Reporting.Drawing.Unit.Cm(1.501D), Telerik.Reporting.Drawing.Unit.Cm(0.42D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Cm(28.71D);
            ((System.ComponentModel.ISupportInitialize)(this.wSubProto2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.ReportHeaderSection ReportHeaderArea1;
        private Telerik.Reporting.DetailSection DetailArea1;
        private Telerik.Reporting.Shape Line1;
        private Telerik.Reporting.TextBox Tag1;
        private Telerik.Reporting.TextBox Name4;
        private Telerik.Reporting.TextBox Name2;
        private Telerik.Reporting.SubReport Subreport1;
        private Telerik.Reporting.TextBox Text27;
        private Telerik.Reporting.TextBox Text28;
        private Telerik.Reporting.TextBox Zeit1;
        private Telerik.Reporting.TextBox VonEti1;
        private Telerik.Reporting.TextBox BisEti1;
        private Telerik.Reporting.TextBox Text29;
        private Telerik.Reporting.TextBox ZielLot1;
        private Telerik.Reporting.TextBox KorrNetto1;
        private Telerik.Reporting.TextBox Unit1;
        private Telerik.Reporting.TextBox Unit3;
        private Telerik.Reporting.TextBox KorrSoll1;
        private Telerik.Reporting.TextBox KorrTara1;
        private Telerik.Reporting.TextBox Unit4;
        private Telerik.Reporting.TextBox KorrBrutto1;
        private Telerik.Reporting.TextBox Nummer1;
        private Telerik.Reporting.TextBox hugo1;
        private Telerik.Reporting.ReportFooterSection ReportFooterArea1;
        private Telerik.Reporting.TextBox Text16;
        private Telerik.Reporting.TextBox Text17;
        private Telerik.Reporting.Shape Line2;
        private Telerik.Reporting.TextBox Text25;
        private Telerik.Reporting.TextBox Lot2;
        private Telerik.Reporting.TextBox Text26;
        private Telerik.Reporting.TextBox RTotal01;
        private Telerik.Reporting.PageFooterSection PageFooterArea1;
        private Telerik.Reporting.TextBox SeiteNvonM1;
        private Telerik.Reporting.PageHeaderSection PageHeaderArea1;
        private Telerik.Reporting.TextBox Text2;
        private Telerik.Reporting.TextBox Text1;
        private Telerik.Reporting.TextBox Text3;
        private Telerik.Reporting.TextBox Text4;
        private Telerik.Reporting.TextBox Text5;
        private Telerik.Reporting.TextBox Text6;
        private Telerik.Reporting.TextBox Text7;
        private Telerik.Reporting.TextBox Text8;
        private Telerik.Reporting.TextBox Name1;
        private Telerik.Reporting.TextBox ArtNr1;
        private Telerik.Reporting.TextBox GenesysNr1;
        private Telerik.Reporting.TextBox Text9;
        private Telerik.Reporting.TextBox Text10;
        private Telerik.Reporting.TextBox Besteller1;
        private Telerik.Reporting.TextBox Text20;
        private Telerik.Reporting.TextBox ANr1;
        private Telerik.Reporting.TextBox ZeitNew1;
        private Telerik.Reporting.TextBox Text22;
        private Telerik.Reporting.TextBox Text36;
        private Telerik.Reporting.TextBox Text35;
        private Telerik.Reporting.TextBox Lot1;
        private Telerik.Reporting.TextBox BArt1;
        private Telerik.Reporting.TextBox Text24;
        private Telerik.Reporting.TextBox Text30;
        private Telerik.Reporting.TextBox Text33;
        private Telerik.Reporting.TextBox Text34;
        private Telerik.Reporting.TextBox Text38;
        private Telerik.Reporting.TextBox Name3;
        private Telerik.Reporting.TextBox Text13;
        private Telerik.Reporting.PictureBox Picture1;
        private Telerik.Reporting.Shape shape1;
        private Telerik.Reporting.Shape shape2;
        private Telerik.Reporting.TextBox wTextBox;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox tara2TextBox;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.ObjectDataSource objectDataSource1;
        private WSubProto wSubProto2;
        private Telerik.Reporting.TextBox textBox10;
    }
}