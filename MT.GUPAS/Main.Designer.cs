﻿
namespace MT.GUPAS
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn1 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn2 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.office2019GrayTheme1 = new Telerik.WinControls.Themes.Office2019GrayTheme();
            this.mnuDatei = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuQuit = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuEtiDrucker = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuTabellen = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuArtikel = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuUser = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuAArt = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuToleranzen = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuAudit = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuSperre = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuJournal = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuInfo = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuEtikette = new Telerik.WinControls.UI.RadMenuItem();
            this.rdGridNeu = new Telerik.WinControls.UI.RadGridView();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.btActualNew = new System.Windows.Forms.Button();
            this.btDeleteOrder = new System.Windows.Forms.Button();
            this.btPrintOrder = new System.Windows.Forms.Button();
            this.btNewOrder = new System.Windows.Forms.Button();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.rdGridAlt = new Telerik.WinControls.UI.RadGridView();
            this.btClose = new System.Windows.Forms.Button();
            this.btOverview = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.radMenu1 = new Telerik.WinControls.UI.RadMenu();
            this.btActualOld = new System.Windows.Forms.Button();
            this.kopfNeuModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.rdGridNeu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdGridNeu.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rdGridAlt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdGridAlt.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kopfNeuModelBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // mnuDatei
            // 
            this.mnuDatei.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.mnuQuit,
            this.mnuEtiDrucker});
            this.mnuDatei.Name = "mnuDatei";
            this.mnuDatei.Text = "Datei";
            // 
            // mnuQuit
            // 
            this.mnuQuit.DescriptionFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mnuQuit.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mnuQuit.Name = "mnuQuit";
            this.mnuQuit.Text = "Quit";
            this.mnuQuit.Click += new System.EventHandler(this.mnuQuit_Click);
            // 
            // mnuEtiDrucker
            // 
            this.mnuEtiDrucker.DescriptionFont = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mnuEtiDrucker.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mnuEtiDrucker.Name = "mnuEtiDrucker";
            this.mnuEtiDrucker.Text = "Etikettendrucker";
            this.mnuEtiDrucker.Click += new System.EventHandler(this.radMenuItem1_Click);
            // 
            // mnuTabellen
            // 
            this.mnuTabellen.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.mnuArtikel,
            this.mnuUser,
            this.mnuAArt,
            this.mnuToleranzen,
            this.mnuAudit,
            this.mnuSperre});
            this.mnuTabellen.Name = "mnuTabellen";
            this.mnuTabellen.Text = "Tabellen";
            // 
            // mnuArtikel
            // 
            this.mnuArtikel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.mnuArtikel.Name = "mnuArtikel";
            this.mnuArtikel.Text = "Artikel";
            this.mnuArtikel.Click += new System.EventHandler(this.mnuArtikel_Click);
            // 
            // mnuUser
            // 
            this.mnuUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.mnuUser.Name = "mnuUser";
            this.mnuUser.Text = "Benutzer";
            this.mnuUser.Click += new System.EventHandler(this.mnuUser_Click);
            // 
            // mnuAArt
            // 
            this.mnuAArt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.mnuAArt.Name = "mnuAArt";
            this.mnuAArt.Text = "Auftragsart";
            this.mnuAArt.Click += new System.EventHandler(this.mnuAArt_Click);
            // 
            // mnuToleranzen
            // 
            this.mnuToleranzen.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mnuToleranzen.Name = "mnuToleranzen";
            this.mnuToleranzen.Text = "Toleranzen";
            this.mnuToleranzen.Click += new System.EventHandler(this.mnuToleranzen_Click);
            // 
            // mnuAudit
            // 
            this.mnuAudit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mnuAudit.Name = "mnuAudit";
            this.mnuAudit.Text = "Audit Trail";
            this.mnuAudit.Click += new System.EventHandler(this.mnuAudit_Click);
            // 
            // mnuSperre
            // 
            this.mnuSperre.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mnuSperre.Name = "mnuSperre";
            this.mnuSperre.Text = "Sperrzeit";
            this.mnuSperre.Click += new System.EventHandler(this.mnuSperre_Click);
            // 
            // mnuJournal
            // 
            this.mnuJournal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mnuJournal.Name = "mnuJournal";
            this.mnuJournal.Text = "Journal";
            this.mnuJournal.Click += new System.EventHandler(this.mnuJournal_Click);
            // 
            // mnuInfo
            // 
            this.mnuInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mnuInfo.Name = "mnuInfo";
            this.mnuInfo.Text = "Info";
            this.mnuInfo.Click += new System.EventHandler(this.radMenuItem5_Click);
            // 
            // mnuEtikette
            // 
            this.mnuEtikette.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mnuEtikette.Name = "mnuEtikette";
            this.mnuEtikette.Text = "Etikette";
            this.mnuEtikette.Click += new System.EventHandler(this.mnuEtikette_Click);
            // 
            // rdGridNeu
            // 
            this.rdGridNeu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdGridNeu.Location = new System.Drawing.Point(5, 30);
            // 
            // 
            // 
            this.rdGridNeu.MasterTemplate.AllowAddNewRow = false;
            this.rdGridNeu.MasterTemplate.AllowDragToGroup = false;
            gridViewTextBoxColumn1.FieldName = "Name";
            gridViewTextBoxColumn1.HeaderText = "Name";
            gridViewTextBoxColumn1.IsAutoGenerated = true;
            gridViewTextBoxColumn1.MinWidth = 50;
            gridViewTextBoxColumn1.Name = "Name";
            gridViewTextBoxColumn2.FieldName = "ANr";
            gridViewTextBoxColumn2.HeaderText = "ANr";
            gridViewTextBoxColumn2.IsAutoGenerated = true;
            gridViewTextBoxColumn2.MinWidth = 50;
            gridViewTextBoxColumn2.Name = "ANr";
            gridViewTextBoxColumn3.FieldName = "BArt";
            gridViewTextBoxColumn3.HeaderText = "BArt";
            gridViewTextBoxColumn3.IsAutoGenerated = true;
            gridViewTextBoxColumn3.MinWidth = 50;
            gridViewTextBoxColumn3.Name = "BArt";
            gridViewTextBoxColumn4.FieldName = "Besteller";
            gridViewTextBoxColumn4.HeaderText = "Besteller";
            gridViewTextBoxColumn4.IsAutoGenerated = true;
            gridViewTextBoxColumn4.MinWidth = 50;
            gridViewTextBoxColumn4.Name = "Besteller";
            gridViewDecimalColumn1.DataType = typeof(float);
            gridViewDecimalColumn1.FieldName = "Total";
            gridViewDecimalColumn1.HeaderText = "Total";
            gridViewDecimalColumn1.IsAutoGenerated = true;
            gridViewDecimalColumn1.MinWidth = 50;
            gridViewDecimalColumn1.Name = "Total";
            this.rdGridNeu.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewDecimalColumn1});
            this.rdGridNeu.MasterTemplate.DataSource = this.kopfNeuModelBindingSource;
            this.rdGridNeu.MasterTemplate.ShowRowHeaderColumn = false;
            this.rdGridNeu.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.rdGridNeu.Name = "rdGridNeu";
            this.rdGridNeu.ReadOnly = true;
            this.rdGridNeu.Size = new System.Drawing.Size(974, 176);
            this.rdGridNeu.TabIndex = 4;
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.radGroupBox1.Controls.Add(this.btActualNew);
            this.radGroupBox1.Controls.Add(this.btDeleteOrder);
            this.radGroupBox1.Controls.Add(this.btPrintOrder);
            this.radGroupBox1.Controls.Add(this.btNewOrder);
            this.radGroupBox1.Controls.Add(this.rdGridNeu);
            this.radGroupBox1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGroupBox1.HeaderMargin = new System.Windows.Forms.Padding(1);
            this.radGroupBox1.HeaderText = "Neue Aufträge";
            this.radGroupBox1.Location = new System.Drawing.Point(12, 37);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(1009, 276);
            this.radGroupBox1.TabIndex = 5;
            this.radGroupBox1.Text = "Neue Aufträge";
            ((Telerik.WinControls.UI.GroupBoxHeader)(this.radGroupBox1.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Segoe UI", 11.25F);
            ((Telerik.WinControls.UI.GroupBoxHeader)(this.radGroupBox1.GetChildAt(0).GetChildAt(1))).Margin = new System.Windows.Forms.Padding(1);
            // 
            // btActualNew
            // 
            this.btActualNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btActualNew.Location = new System.Drawing.Point(388, 221);
            this.btActualNew.Name = "btActualNew";
            this.btActualNew.Size = new System.Drawing.Size(179, 40);
            this.btActualNew.TabIndex = 9;
            this.btActualNew.Text = "Anzeige aktualisieren";
            this.btActualNew.UseVisualStyleBackColor = true;
            this.btActualNew.Click += new System.EventHandler(this.btActualNew_Click);
            // 
            // btDeleteOrder
            // 
            this.btDeleteOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDeleteOrder.Location = new System.Drawing.Point(844, 221);
            this.btDeleteOrder.Name = "btDeleteOrder";
            this.btDeleteOrder.Size = new System.Drawing.Size(135, 40);
            this.btDeleteOrder.TabIndex = 7;
            this.btDeleteOrder.Text = "Auftrag löschen";
            this.btDeleteOrder.UseVisualStyleBackColor = true;
            this.btDeleteOrder.Click += new System.EventHandler(this.btDeleteOrder_Click);
            // 
            // btPrintOrder
            // 
            this.btPrintOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btPrintOrder.Location = new System.Drawing.Point(177, 221);
            this.btPrintOrder.Name = "btPrintOrder";
            this.btPrintOrder.Size = new System.Drawing.Size(135, 40);
            this.btPrintOrder.TabIndex = 6;
            this.btPrintOrder.Text = "Auftrag drucken";
            this.btPrintOrder.UseVisualStyleBackColor = true;
            this.btPrintOrder.Click += new System.EventHandler(this.btPrintOrder_Click);
            // 
            // btNewOrder
            // 
            this.btNewOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btNewOrder.Location = new System.Drawing.Point(5, 221);
            this.btNewOrder.Name = "btNewOrder";
            this.btNewOrder.Size = new System.Drawing.Size(135, 40);
            this.btNewOrder.TabIndex = 5;
            this.btNewOrder.Text = "Neuer Auftrag";
            this.btNewOrder.UseVisualStyleBackColor = true;
            this.btNewOrder.Click += new System.EventHandler(this.btNewOrder_Click);
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.radGroupBox2.Controls.Add(this.rdGridAlt);
            this.radGroupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGroupBox2.HeaderMargin = new System.Windows.Forms.Padding(1);
            this.radGroupBox2.HeaderText = "Aufträge in Arbeit / Erledigt";
            this.radGroupBox2.Location = new System.Drawing.Point(12, 334);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Size = new System.Drawing.Size(1009, 244);
            this.radGroupBox2.TabIndex = 6;
            this.radGroupBox2.Text = "Aufträge in Arbeit / Erledigt";
            ((Telerik.WinControls.UI.GroupBoxHeader)(this.radGroupBox2.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Segoe UI", 11.25F);
            ((Telerik.WinControls.UI.GroupBoxHeader)(this.radGroupBox2.GetChildAt(0).GetChildAt(1))).Margin = new System.Windows.Forms.Padding(1);
            // 
            // rdGridAlt
            // 
            this.rdGridAlt.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdGridAlt.Location = new System.Drawing.Point(5, 30);
            // 
            // 
            // 
            this.rdGridAlt.MasterTemplate.AllowAddNewRow = false;
            this.rdGridAlt.MasterTemplate.AllowDragToGroup = false;
            this.rdGridAlt.MasterTemplate.AllowRowHeaderContextMenu = false;
            gridViewTextBoxColumn5.FieldName = "Name";
            gridViewTextBoxColumn5.HeaderText = "Name";
            gridViewTextBoxColumn5.IsAutoGenerated = true;
            gridViewTextBoxColumn5.MinWidth = 50;
            gridViewTextBoxColumn5.Name = "Name";
            gridViewTextBoxColumn6.FieldName = "ANr";
            gridViewTextBoxColumn6.HeaderText = "ANr";
            gridViewTextBoxColumn6.IsAutoGenerated = true;
            gridViewTextBoxColumn6.MinWidth = 50;
            gridViewTextBoxColumn6.Name = "ANr";
            gridViewTextBoxColumn7.FieldName = "BArt";
            gridViewTextBoxColumn7.HeaderText = "BArt";
            gridViewTextBoxColumn7.IsAutoGenerated = true;
            gridViewTextBoxColumn7.MinWidth = 50;
            gridViewTextBoxColumn7.Name = "BArt";
            gridViewTextBoxColumn8.FieldName = "Besteller";
            gridViewTextBoxColumn8.HeaderText = "Besteller";
            gridViewTextBoxColumn8.IsAutoGenerated = true;
            gridViewTextBoxColumn8.MinWidth = 50;
            gridViewTextBoxColumn8.Name = "Besteller";
            gridViewDecimalColumn2.DataType = typeof(float);
            gridViewDecimalColumn2.FieldName = "Total";
            gridViewDecimalColumn2.HeaderText = "Total";
            gridViewDecimalColumn2.IsAutoGenerated = true;
            gridViewDecimalColumn2.MinWidth = 50;
            gridViewDecimalColumn2.Name = "Total";
            this.rdGridAlt.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewDecimalColumn2});
            this.rdGridAlt.MasterTemplate.DataSource = this.kopfNeuModelBindingSource;
            this.rdGridAlt.MasterTemplate.ShowRowHeaderColumn = false;
            this.rdGridAlt.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.rdGridAlt.Name = "rdGridAlt";
            this.rdGridAlt.ReadOnly = true;
            this.rdGridAlt.Size = new System.Drawing.Size(974, 209);
            this.rdGridAlt.TabIndex = 4;
            this.rdGridAlt.RowFormatting += new Telerik.WinControls.UI.RowFormattingEventHandler(this.rdGridAlt_RowFormatting);
            // 
            // btClose
            // 
            this.btClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btClose.Location = new System.Drawing.Point(886, 590);
            this.btClose.Name = "btClose";
            this.btClose.Size = new System.Drawing.Size(135, 40);
            this.btClose.TabIndex = 8;
            this.btClose.Text = "Beenden";
            this.btClose.UseVisualStyleBackColor = true;
            this.btClose.Click += new System.EventHandler(this.btClose_Click);
            // 
            // btOverview
            // 
            this.btOverview.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btOverview.Location = new System.Drawing.Point(653, 590);
            this.btOverview.Name = "btOverview";
            this.btOverview.Size = new System.Drawing.Size(161, 40);
            this.btOverview.TabIndex = 8;
            this.btOverview.Text = "Auftragsübersicht";
            this.btOverview.UseVisualStyleBackColor = true;
            this.btOverview.Click += new System.EventHandler(this.btOverview_Click);
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(17, 590);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(152, 40);
            this.button5.TabIndex = 6;
            this.button5.Text = "Protokoll drucken";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // radMenu1
            // 
            this.radMenu1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radMenu1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.mnuDatei,
            this.mnuTabellen,
            this.mnuJournal,
            this.mnuEtikette,
            this.mnuInfo});
            this.radMenu1.Location = new System.Drawing.Point(0, 0);
            this.radMenu1.Name = "radMenu1";
            this.radMenu1.Size = new System.Drawing.Size(1042, 31);
            this.radMenu1.TabIndex = 0;
            this.radMenu1.ThemeName = "Office2019Gray";
            // 
            // btActualOld
            // 
            this.btActualOld.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btActualOld.Location = new System.Drawing.Point(400, 590);
            this.btActualOld.Name = "btActualOld";
            this.btActualOld.Size = new System.Drawing.Size(179, 40);
            this.btActualOld.TabIndex = 10;
            this.btActualOld.Text = "Anzeige aktualisieren";
            this.btActualOld.UseVisualStyleBackColor = true;
            this.btActualOld.Click += new System.EventHandler(this.btActualOld_Click);
            // 
            // kopfNeuModelBindingSource
            // 
            this.kopfNeuModelBindingSource.DataSource = typeof(MT.GUPAS.Models.KopfNeuModel);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1042, 642);
            this.Controls.Add(this.btActualOld);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.btOverview);
            this.Controls.Add(this.btClose);
            this.Controls.Add(this.radGroupBox2);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.radMenu1);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1050, 674);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1050, 674);
            this.Name = "Main";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.RootElement.MaxSize = new System.Drawing.Size(1050, 674);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GUAPS Abfüllung";
            this.ThemeName = "Office2019Gray";
            this.Load += new System.EventHandler(this.Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.rdGridNeu.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdGridNeu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rdGridAlt.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdGridAlt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kopfNeuModelBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.Themes.Office2019GrayTheme office2019GrayTheme1;
        private Telerik.WinControls.UI.RadMenuItem mnuDatei;
        private Telerik.WinControls.UI.RadMenuItem mnuTabellen;
        private Telerik.WinControls.UI.RadMenuItem mnuArtikel;
        private Telerik.WinControls.UI.RadMenuItem mnuAudit;
        private Telerik.WinControls.UI.RadMenuItem mnuInfo;
        private Telerik.WinControls.UI.RadMenuItem mnuQuit;
        private Telerik.WinControls.UI.RadMenuItem mnuUser;
        private Telerik.WinControls.UI.RadMenuItem mnuAArt;
        private Telerik.WinControls.UI.RadMenuItem mnuToleranzen;
        private Telerik.WinControls.UI.RadMenuItem mnuSperre;
        private Telerik.WinControls.UI.RadMenuItem mnuJournal;
        private Telerik.WinControls.UI.RadGridView rdGridNeu;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private System.Windows.Forms.BindingSource kopfNeuModelBindingSource;
        private System.Windows.Forms.Button btDeleteOrder;
        private System.Windows.Forms.Button btPrintOrder;
        private System.Windows.Forms.Button btNewOrder;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private System.Windows.Forms.Button button5;
        private Telerik.WinControls.UI.RadGridView rdGridAlt;
        private System.Windows.Forms.Button btOverview;
        private System.Windows.Forms.Button btClose;
        private Telerik.WinControls.UI.RadMenuItem mnuEtikette;
        private Telerik.WinControls.UI.RadMenuItem mnuEtiDrucker;
        private Telerik.WinControls.UI.RadMenu radMenu1;
        private System.Windows.Forms.Button btActualNew;
        private System.Windows.Forms.Button btActualOld;
    }
}