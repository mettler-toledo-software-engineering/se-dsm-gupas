﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MT.GUPAS.Models
{
    public class JournalModel
    {

        public DateTime Zeit { get; set; }
        public string ArtNr { get; set; }
        public string ArtikelName { get; set; }
        public Single Brutto { get; set; }
        public Single Netto { get; set; }
        public Single Tara { get; set; }
        public string Lot { get; set; }
        public string ANr { get; set; }
        public string BArt { get; set; }
        public string Abfueller { get; set; }
        public string Name { get; set; }
        public string Einheit { get; set; }
        [Browsable(false)]
        public string Unit { get; set; }
    }
}
