//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MT.GUPAS.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SystemM
    {
        public int Nummer { get; set; }
        public Nullable<double> Toleranz { get; set; }
        public string TLPName { get; set; }
        public int Benutzer { get; set; }
        public int BArt { get; set; }
        public int Komp { get; set; }
        public string WList { get; set; }
        public string WProto { get; set; }
        public string WEti { get; set; }
        public string WTrail { get; set; }
        public string WJour { get; set; }
        public int Sperrzeit { get; set; }
    }
}
