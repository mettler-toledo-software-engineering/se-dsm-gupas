﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MT.GUPAS.Models
{
    public class ListModel
    {
        public string KName { get; set; }
        public string ArtNr { get; set; }
        public string GenesysNr { get; set; }
        public string RName { get; set; }
        public string Besteller { get; set; }
        public string Name { get; set; }
        public string ANr { get; set; }
        public DateTime ZeitNew { get; set; }
        public string Lot { get; set; }
        public int KNummer { get; set; }
        public int LNummer { get; set; }
        public int DNummer { get; set; }
        public Single Soll { get; set; }
        public string BArt { get; set; }
        public string Gebinde { get; set; }

    }
}
