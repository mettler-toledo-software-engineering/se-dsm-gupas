﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MT.GUPAS.Models
{
    public class ProtoModel
    {

        public string KName { get; set; }
        public string ArtNr { get; set; }
        public string GenesysNr { get; set; }
        public string RName { get; set; }
        public string Besteller { get; set; }
        public string ANr { get; set; }
        public DateTime ZeitNew { get; set; }
        public string Lot { get; set; }
        public int LNummer { get; set; }
        public string Tag { get; set; }
        public string WName { get; set; }
        public Nullable<int> VonEti { get; set; }
        public Nullable<int> BisEti { get; set; }
        public int Status { get; set; }
        public int DNummer { get; set; }
        public string BArt { get; set; }
        public Single Soll { get; set; }
        public Single Brutto { get; set; }
        public Single Netto { get; set; }
        public Single Tara { get; set; }
        public Single Tara2 { get; set; }
        public DateTime Zeit { get; set; }
        public int Hand { get; set; }
        public int Precision { get; set; }
        public string Ziellot { get; set; }
        public string Unit { get; set; }
        public int AnzZeilen { get; set; }
        public string Name { get; set; }

    }
}
