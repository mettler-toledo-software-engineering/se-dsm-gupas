﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MT.GUPAS.Models
{
    public class KopfAltModel
    {
        public int Nummer { get; set; }
        [DisplayName("Auftragsnummer")]
        public string ANr
        {
            get { return anr; }
            set { anr = value.Trim(); }
        }
        private string anr;
        public string Name
        {
            get { return name; }
            set { name = value.Trim(); }
        }
        private string name;
        public string Besteller
        {
            get { return besteller; }
            set { besteller = value.Trim(); }
        }
        private string besteller;

        [DisplayName("Fertig [%]")]
        public int AnzZeilenTotal
        {
            get
            {
                return (anzZeilenTotal - AnzZeilen) * 100 / anzZeilenTotal;
            }
            set { anzZeilenTotal = value; }
        }
        private int anzZeilenTotal;

        [DisplayName("Beendet")]
        public bool Komplett { get; set; }
        [DisplayName("Noch zu dosieren")]
        public int AnzZeilen { get; set; }
        [DisplayName("Ausdrucke")]
        public int AnzPrint { get; set; }







    }
}
