﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MT.GUPAS.Models
{
    public class StaffelModel
    {
        public int Nummer { get; set; }
        public int Anzahl { get; set; }
        public Single Menge { get; set; }
        [DisplayName("# MG Geb.")]
        public int AnzMGGebinde { get; set; }
        public Single Total { get; set; }
    }
}
