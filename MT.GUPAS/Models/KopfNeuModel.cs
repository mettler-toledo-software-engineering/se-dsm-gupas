﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MT.GUPAS.Models
{
    public class KopfNeuModel
    {
        public int Nummer { get; set; }
        [DisplayName("Auftragsnummer")]
        public string ANr { get; set; }
        public string Name
        {
            get { return name; }
            set { name = value.Trim(); }
        }
        private string name;

        [DisplayName("Auftragsart")]
        public string BArt { get; set; }
        public string Besteller { get; set; }
        [DisplayName("Totalmenge [kg]")]
        public Single Total { get; set; }
        [DisplayName("Anzahl Dosierungen")]
        public int AnzZeilenTotal { get; set; }

    }
}
