﻿namespace MT.GUPAS.Infrastructure
{
    partial class InputBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InputBox));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btCancel = new System.Windows.Forms.Button();
            this.btPrint = new System.Windows.Forms.Button();
            this.txtLot = new System.Windows.Forms.TextBox();
            this.txtGebinde = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbArtikelname = new Telerik.WinControls.UI.RadDropDownList();
            this.kompBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cbArtNr = new Telerik.WinControls.UI.RadDropDownList();
            this.cbGenesysNr = new Telerik.WinControls.UI.RadDropDownList();
            ((System.ComponentModel.ISupportInitialize)(this.cbArtikelname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kompBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbArtNr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGenesysNr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Artikelname:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "Artikel-Nr.:";
            // 
            // btCancel
            // 
            this.btCancel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCancel.Location = new System.Drawing.Point(147, 300);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(95, 36);
            this.btCancel.TabIndex = 4;
            this.btCancel.Text = "Schliessen";
            this.btCancel.UseVisualStyleBackColor = true;
            this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
            // 
            // btPrint
            // 
            this.btPrint.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btPrint.Location = new System.Drawing.Point(15, 300);
            this.btPrint.Name = "btPrint";
            this.btPrint.Size = new System.Drawing.Size(95, 36);
            this.btPrint.TabIndex = 5;
            this.btPrint.Text = "Drucken";
            this.btPrint.UseVisualStyleBackColor = true;
            this.btPrint.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtLot
            // 
            this.txtLot.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLot.Location = new System.Drawing.Point(16, 198);
            this.txtLot.Name = "txtLot";
            this.txtLot.Size = new System.Drawing.Size(226, 29);
            this.txtLot.TabIndex = 6;
            // 
            // txtGebinde
            // 
            this.txtGebinde.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGebinde.Location = new System.Drawing.Point(16, 251);
            this.txtGebinde.Name = "txtGebinde";
            this.txtGebinde.Size = new System.Drawing.Size(226, 29);
            this.txtGebinde.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 230);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 21);
            this.label3.TabIndex = 9;
            this.label3.Text = "Gebinde-Nr:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 174);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 21);
            this.label4.TabIndex = 10;
            this.label4.Text = "Lot-Nr:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 122);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 21);
            this.label5.TabIndex = 11;
            this.label5.Text = "Genesys-Nr.:";
            // 
            // cbArtikelname
            // 
            this.cbArtikelname.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cbArtikelname.AutoSizeItems = true;
            this.cbArtikelname.DataSource = this.kompBindingSource;
            this.cbArtikelname.DisplayMember = "Name";
            this.cbArtikelname.DropDownAnimationEnabled = true;
            this.cbArtikelname.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbArtikelname.Location = new System.Drawing.Point(16, 33);
            this.cbArtikelname.Name = "cbArtikelname";
            this.cbArtikelname.Size = new System.Drawing.Size(226, 31);
            this.cbArtikelname.TabIndex = 12;
            this.cbArtikelname.ValueMember = "Nummer";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.cbArtikelname.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDown;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.cbArtikelname.GetChildAt(0))).Font = new System.Drawing.Font("Segoe UI", 12F);
            // 
            // kompBindingSource
            // 
            this.kompBindingSource.DataSource = typeof(MT.GUPAS.Models.Komp);
            // 
            // cbArtNr
            // 
            this.cbArtNr.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cbArtNr.AutoSizeItems = true;
            this.cbArtNr.DataSource = this.kompBindingSource;
            this.cbArtNr.DisplayMember = "ArtNr";
            this.cbArtNr.DropDownAnimationEnabled = true;
            this.cbArtNr.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbArtNr.Location = new System.Drawing.Point(16, 88);
            this.cbArtNr.Name = "cbArtNr";
            this.cbArtNr.Size = new System.Drawing.Size(226, 31);
            this.cbArtNr.TabIndex = 13;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.cbArtNr.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDown;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.cbArtNr.GetChildAt(0))).Font = new System.Drawing.Font("Segoe UI", 12F);
            // 
            // cbGenesysNr
            // 
            this.cbGenesysNr.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cbGenesysNr.AutoSizeItems = true;
            this.cbGenesysNr.DataSource = this.kompBindingSource;
            this.cbGenesysNr.DisplayMember = "GenesysNr";
            this.cbGenesysNr.DropDownAnimationEnabled = true;
            this.cbGenesysNr.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbGenesysNr.Location = new System.Drawing.Point(16, 143);
            this.cbGenesysNr.Name = "cbGenesysNr";
            this.cbGenesysNr.Size = new System.Drawing.Size(226, 31);
            this.cbGenesysNr.TabIndex = 14;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.cbGenesysNr.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDown;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.cbGenesysNr.GetChildAt(0))).Font = new System.Drawing.Font("Segoe UI", 12F);
            // 
            // InputBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(263, 358);
            this.Controls.Add(this.cbGenesysNr);
            this.Controls.Add(this.cbArtNr);
            this.Controls.Add(this.cbArtikelname);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtGebinde);
            this.Controls.Add(this.txtLot);
            this.Controls.Add(this.btPrint);
            this.Controls.Add(this.btCancel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InputBox";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.RootElement.MaxSize = new System.Drawing.Size(0, 0);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "InputBox";
            this.Load += new System.EventHandler(this.InputBox_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cbArtikelname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kompBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbArtNr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGenesysNr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btCancel;
        private System.Windows.Forms.Button btPrint;
        private System.Windows.Forms.TextBox txtLot;
        private System.Windows.Forms.TextBox txtGebinde;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private Telerik.WinControls.UI.RadDropDownList cbArtikelname;
        private Telerik.WinControls.UI.RadDropDownList cbArtNr;
        private Telerik.WinControls.UI.RadDropDownList cbGenesysNr;
        private System.Windows.Forms.BindingSource kompBindingSource;
    }
}
