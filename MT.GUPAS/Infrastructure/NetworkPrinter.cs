﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;


namespace MT.GUPAS.Infrastructure
{
    public class NetworkPrinter
    {

        private string _ipAddress;

        private int _port;
        private TcpClient _client;


        public NetworkPrinter(string ipAdress, int port)
        {
            _port = port;
            _ipAddress = ipAdress;
        }

        public PrintState CheckIfPrinterIsOnline()
        {
            PrintState result = PrintState.PrinterOffline;
            try
            {
                var ping = new Ping();
                var reply = ping.Send(_ipAddress, 500);
                result = reply?.Status == IPStatus.Success ? PrintState.PrinterOnline : PrintState.PrinterOffline;
            }
            catch (Exception ex)
            {
                return PrintState.PrinterOffline;
            }

            return result;
        }


        public PrintState Print(string template)
        {
            try
            {
                using (_client = new TcpClient(_ipAddress, _port))
                {
                    var message = Encoding.UTF8.GetBytes(template);
                    var stream = _client.GetStream();
                    stream.Write(message, 0, message.Length);
                    stream.Close();
                    _client.Close();
                    return PrintState.PrintDone;
                }
            }
            catch 
            {
                return PrintState.PrintFailed;
            }
        }
    }
    public enum PrintState
    {
        PrintDone,
        PrintFailed,
        PrintFailedTemplate,
        PrinterOffline,
        PrinterOnline,
        PrinterNoData
    }
}
