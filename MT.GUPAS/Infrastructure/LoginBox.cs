﻿using MT.GUPAS.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace MT.GUPAS.Infrastructure
{
    public partial class LoginBox : Telerik.WinControls.UI.RadForm
    {

        private RocheDataEntities rocheDBContext;
        private List<User> rocheUser = new List<User>();
        private SystemM system = null;
        private bool sameUser;
        public LoginBox(bool SameUser)

        {
            InitializeComponent();
            sameUser = SameUser;
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void InputBox_Load(object sender, EventArgs e)
        {
            rocheDBContext = new RocheDataEntities();

            rocheUser = rocheDBContext.Users.Where(f => f.Level > 0).OrderBy(d => d.Name).ToList();
            system = rocheDBContext.SystemMs.First();
            Globals.LogoutTime = Convert.ToUInt32(system.Sperrzeit * 5000); // = msec
           
            cbUser.DisplayMember = "Name";
            cbUser.DataSource = rocheUser;
            cbUser.Enabled = !sameUser;
        
            foreach (var row in cbUser.Items)
            {
                row.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            }

            User user = rocheUser.FirstOrDefault(d => d.Nummer == system.Benutzer);
            if (user != null)
                cbUser.SelectedIndex = cbUser.FindString(user.Name);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (cbUser.SelectedText != null)
            {
                User result = rocheUser.Find(d => d.Name.Trim() == cbUser.SelectedItem.Text.Trim());
                if (result.PW.Trim() == tbUnten.Text.Trim())
                {
                    system.Benutzer = result.Nummer;
                    rocheDBContext.SaveChanges();
                    Globals.User = result;
                    DialogResult = DialogResult.OK;
                    Close();
                }
                else
                    MessageBox.Show("Passwort falsch!", "Anmeldung", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
