﻿using MT.GUPAS.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MT.GUPAS.Infrastructure
{
    public class ManageAuditTail
    {

        private static Dictionary<AuditKlasse, string> KlasseZ = new Dictionary<AuditKlasse, string>()
        {
            {AuditKlasse.Auftrag,"Auftrag"},
            {AuditKlasse.Protokoll,"Protokoll"},
            {AuditKlasse.Tabelle,"Tabelle"},
            {AuditKlasse.Applikation,"Tabelle"}
        };

        private static Dictionary<AuditAction, string> ActionZ = new Dictionary<AuditAction, string>()
        {
            {AuditAction.Erstellen,"Erstellen"},
            {AuditAction.Loeschen,"Loeschen" },
            {AuditAction.Beenden,"Beenden" },
            {AuditAction.Ausdrucken,"Ausdrucken" },
            {AuditAction.Starten,"Starten" },
            {AuditAction.Aendern,"Aendern" }
            
        };

        private static readonly RocheDataEntities rocheDBContext = new RocheDataEntities();

        public static void WriteAuditTrial(AuditKlasse a, AuditAction b, string Para1, string Para2)
        {

            AuditTrail at = new AuditTrail()
            {
                Ersteller = Globals.User.Name.Trim(),
                Zeit = DateTime.Now,
                Klasse = KlasseZ[a],
                Aktion = ActionZ[b],
                Para = Para1.Trim().Substring(0,Math.Min(25,Para1.Trim().Length)),
                Para2 = Para2.Trim().Substring(0, Math.Min(50, Para2.Trim().Length))

            };
            rocheDBContext.AuditTrails.Add(at);
            rocheDBContext.SaveChanges();
        }


    }


    public enum AuditKlasse
    {
        Auftrag,
        Protokoll,
        Tabelle,
        Applikation
    }
    public enum AuditAction
    {
        Erstellen,
        Loeschen,
        Beenden,
        Ausdrucken,
        Starten,
        Aendern
    }

}


