﻿using MT.GUPAS.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MT.GUPAS.Infrastructure
{
    public partial class InputBox : Telerik.WinControls.UI.RadForm
    {
        private Komp _komp;
        private RocheDataEntities rocheDBContext;
        private DbSet<Komp> rocheKomp;


        public InputBox(Komp komp)
        {
            InitializeComponent();
            _komp = komp;
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void InputBox_Load(object sender, EventArgs e)
        {
            Text = _komp.Name;
            rocheDBContext = new RocheDataEntities();
            rocheKomp = rocheDBContext.Komps;
            List<Komp> x = rocheKomp.Where(d => d.Active > 0).OrderBy(f => f.Name).ToList();
            foreach (var item in x)
            {
                item.Name = item.Name?.Trim();
                item.ArtNr = item.ArtNr?.Trim();
                item.GenesysNr = item.GenesysNr?.Trim();
            }

            this.kompBindingSource.DataSource = x;
            if (_komp.ArtNr != null)
                cbArtNr.SelectedIndex = cbArtNr.FindString(_komp.ArtNr.Trim());
            else if (_komp.GenesysNr != null)
                cbGenesysNr.SelectedIndex = cbGenesysNr.FindString(_komp.GenesysNr.Trim());
            else
                cbArtikelname.SelectedIndex = cbArtikelname.FindString(_komp.Name.Trim());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<string> printData = new List<string>();
            SystemM system = rocheDBContext.SystemMs.First();
            NetworkPrinter printer = new NetworkPrinter(system.TLPName.Trim(), 9100);
            int artNummer = Convert.ToInt32(cbArtikelname.SelectedValue);

            _komp = rocheDBContext.Komps.FirstOrDefault(d => d.Nummer == artNummer);

            if ((_komp == null) || (txtGebinde.Text.Length == 0) || (txtLot.Text.Length == 0))
            {
                MessageBox.Show($"Eingabefelder sind leer!");
                return;
            }

            if (printer.CheckIfPrinterIsOnline() == PrintState.PrinterOffline)
                MessageBox.Show($"Etikettendrucker nicht gefunden!");
            else
            {

                string fileName = Path.Combine(Application.StartupPath, "GUPAS_PC.txt");
                string templateData = "";

                try
                {
                    if (File.Exists(fileName))
                    {
                        templateData = File.ReadAllText(fileName, Encoding.GetEncoding(1252));
                    }
                    else
                    {
                        MessageBox.Show($"Templatedatei nicht gefunden!");
                    }

                    templateData = templateData.Replace("{ALOT}", txtLot.Text.Trim());
                    if ((_komp.GenesysNr != null) && (_komp.GenesysNr.Trim().Length > 0))
                    {
                        templateData = templateData.Replace("{KGNR}", _komp.GenesysNr.Trim());
                        templateData = templateData.Replace("{KGNRB}", _komp.GenesysNr.Trim());
                    }
                    else
                    {
                        templateData = templateData.Replace("{KGNR}", "N/A");
                        templateData = templateData.Replace("{KGNRB}", "{KILL}");
                    }
                    if ((_komp.ArtNr != null) && (_komp.ArtNr.Trim().Length > 0))
                    {
                        templateData = templateData.Replace("{KNR}", _komp.ArtNr.Trim());
                        templateData = templateData.Replace("{KNRB}", _komp.ArtNr.Trim());
                    }
                    else
                    {
                        templateData = templateData.Replace("{KNR}", "N/A");
                        templateData = templateData.Replace("{KNRB}", "{KILL}");
                    }

                    templateData = templateData.Replace("{KNAME}", _komp.Name.Trim());
                    templateData = templateData.Replace("{ERST}", Globals.User.Name.Trim());
                    templateData = templateData.Replace("{GEB}", txtGebinde.Text.Trim());
                    templateData = templateData.Replace("{DATUM}", DateTime.Now.ToString("dd.MM.yy HH:mm"));

                    printData.Clear();
                    printData.AddRange(templateData.Split('\n'));
                    int i = printData.FindIndex(x => x.Contains("{KILL}"));
                    if (i > -1)
                        printData.RemoveAt(i);
                    printer.Print(string.Join("\n", printData));

                }
                catch 
                {
                    MessageBox.Show("Templatedatei konnte nicht konfiguriert werden!");
                }
            }
        }
    }
}


