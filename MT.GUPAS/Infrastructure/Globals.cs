﻿using MT.GUPAS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MT.GUPAS.Infrastructure
{
    static class Globals
    {
        public static User User { get; set; }
        public static uint LogoutTime { get; set; }
       
    }
}
