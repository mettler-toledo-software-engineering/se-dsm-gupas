﻿using MT.GUPAS.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace MT.GUPAS.Infrastructure
{
    public partial class InputBoxS : Telerik.WinControls.UI.RadForm
    {
        private string title;
        private string label;
        private string preset;
        private bool numeric;
        private bool zeroAllowed;

        public InputBoxS(string Title, string Label, string Preset, bool Numeric, bool ZeroAllowed = false)

        {
            InitializeComponent();
            title = Title;
            label = Label;
            numeric = Numeric;
            zeroAllowed = ZeroAllowed;
            preset = Preset;
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Tag = "";
            Close();

        }

        private void InputBox_Load(object sender, EventArgs e)
        {
            Text = title;
            lblInput.Text = label;
            tbOben.Text = preset;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            double result = 0;
            if (numeric == true)
            {
                try
                {
                    result = Double.Parse(tbOben.Text);
                    if ((result == 0) && (zeroAllowed == false))
                    {
                        MessageBox.Show($"Bitte geben Sie die {title} korrekt ein.", "Zielmenge", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show($"Bitte geben Sie die {title} als Zahl ein.", "Zielmenge", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    tbOben.Text = "";
                    return;
                }
            }
            Tag = tbOben.Text;
            DialogResult = DialogResult.OK;
            Close();

        }
    }
}
