﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MT.GUPAS.Logic
{
    public enum AccessRight
    {
        Gesperrt = 0,
        IND930Uer = 1,
        PCUser = 3,
        PCAdmin = 7,
        Admin = 255

    }

    public static class AccessRightHelper
    {
        static public  Dictionary<int, string>  AccessRight = new Dictionary<int, string>();

        static  AccessRightHelper()
        {
            AccessRight.Add(0, "Gesperrt");
            AccessRight.Add(1, "IND930User");
            AccessRight.Add(3, "PCUser");
            AccessRight.Add(7, "PCAdmin");
            AccessRight.Add(255, "Admin");

        }

    }
}
