﻿using System;
using System.Data.Entity;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using MT.GUPAS.Infrastructure;
using MT.GUPAS.Logic;
using MT.GUPAS.Models;
using Telerik.WinControls.UI;

namespace MT.GUPAS.Pages
{
    [SuppressMessage("ReSharper", "SpecifyACultureInStringConversionExplicitly")]
    [SuppressMessage("ReSharper", "LocalizableElement")]
    public partial class UserEdit : RadForm
    {
        private readonly RocheDataEntities _context;
        private readonly User _currentElement;
        private readonly User _element;
        private readonly DbSet<User> _elements;
        private string _wertAlt;


        public UserEdit(RocheDataEntities context, User element)
        {
            InitializeComponent();

            cbAccessRight.DisplayMember = "Value";
            cbAccessRight.ValueMember = "Key";
            cbAccessRight.DataSource = new BindingSource(AccessRightHelper.AccessRight, null);
            _element = element;
            _context = context;
            _elements = _context.Users;
            _currentElement = element == null ? new User() : _elements.Find(element.Nummer);
            LoadElement();
        }

        private void LoadElement()
        {
            tbUserId.Text = _currentElement.Account.ToString();
            tbName.Text = _wertAlt = _currentElement.Name.Trim();
            tbTag.Text = _currentElement.Tag.Trim();
            tbPW1.Text = _currentElement.PW.Trim();
            tbPW2.Text = _currentElement.PW.Trim();
            cbAccessRight.SelectedValue = _currentElement.Level;
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private PasswordScore CheckingPasswordStrength(string password)
        {
            var score = 1;
            if (password.Length < 1)
                return PasswordScore.Blank;
            if (password.Length < 4)
                return PasswordScore.VeryWeak;

            if (password.Length >= 8)
                score++;
            if (password.Length >= 12)
                score++;
            if (Regex.IsMatch(password, @"[0-9]+(\.[0-9][0-9]?)?",
                    RegexOptions.ECMAScript)) //number only //"^\d+$" if you need to match more than one digit.
                score++;
            if (Regex.IsMatch(password, @"^(?=.*[a-z])(?=.*[A-Z]).+$",
                    RegexOptions.ECMAScript)) //both, lower and upper case
                score++;
            if (Regex.IsMatch(password, @"[!,@,#,$,%,^,&,*,?,_,~,-,£,(,)]", RegexOptions.ECMAScript)) //^[A-Z]+$
                score++;
            return (PasswordScore)score;
        }

        #region Buttons

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (CheckEmptyFields())
            {
                MessageBox.Show("Es müssem alle Felder Eingaben enthalten!", "Felder", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                return;
            }


            if (tbPW1.Text.Trim() != tbPW2.Text.Trim())
            {
                MessageBox.Show("Passwort stimmen nicht überein!", "Passwort", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                return;
            }

            if (CheckingPasswordStrength(tbPW1.Text.Trim()) < PasswordScore.Strong)

            {
                MessageBox.Show("Passwort ist zu schwach (mind.8 Zeichen, Gross, Klein, Ziffern)", "Passwort",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (_element == null && _elements.ToList().Find(d => d.Account.ToString().Trim() == tbUserId.Text.Trim()) !=
                null)
            {
                MessageBox.Show("User-ID bereits vorhanden!", "User-ID", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                return;
            }

            if (_element == null)
            {
                _elements.Add(_currentElement);
                ManageAuditTail.WriteAuditTrial(AuditKlasse.Tabelle, AuditAction.Erstellen, "User: Alt N/A",
                    $"Neu {_currentElement.Name}");
            }
            else
            {
                ManageAuditTail.WriteAuditTrial(AuditKlasse.Tabelle, AuditAction.Aendern, $"User: Alt {_wertAlt}",
                    $"Neu {_currentElement.Name}");
            }

            _currentElement.Level2 = AccessRightHelper.AccessRight[_currentElement.Level];
            _context.SaveChanges();
            Close();
        }

        private bool CheckEmptyFields()
        {
            return tbUserId.Text == "" || tbName.Text == "" || tbTag.Text == "" || tbPW2.Text == "" || tbPW1.Text == "";
        }

        #endregion

        #region Leave (Lost Focus)

        private void tbNumber_TextChanged(object sender, EventArgs e)
        {
            var result = 0;
            if (int.TryParse(tbUserId.Text, out result))
                _currentElement.Account = result;
        }

        private void tbDescription_TextChanged(object sender, EventArgs e)
        {
            _currentElement.Name = tbName.Text.Trim();
        }

        private void tbSize_TextChanged(object sender, EventArgs e)
        {
            _currentElement.Tag = tbTag.Text.Trim();
        }

        private void tbPW1_TextChanged(object sender, EventArgs e)
        {
            _currentElement.PW = tbPW1.Text.Trim();
        }

        private void cbAccessRight_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_currentElement != null) _currentElement.Level = Convert.ToInt32(cbAccessRight.SelectedValue);
        }

        #endregion
    }
}