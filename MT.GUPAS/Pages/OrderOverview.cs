﻿using System;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using MT.GUPAS.Models;
using Telerik.WinControls.UI;

namespace MT.GUPAS.Pages
{
    public partial class OrderOverview : RadForm
    {
        private readonly string mySqlAKopf =
            @"SELECT TOP(@Anzahl)  AKopf.Nummer, AKopf.ANr, Komp.Name, AKopf.Besteller,Akopf.Komplett, AKopf.AnzZeilenTotal,AKopf.AnzZeilen
                                    FROM RocheData.dbo.AKopf
                                    INNER JOIN Komp ON AKopf.MG = Komp.Nummer
                                    
                                    ORDER BY ZeitNew DESC";

        private readonly RocheDataEntities rocheDBContext;

        // WHERE AnzZeilen <> AnzZeilenTotal
        public OrderOverview()
        {
            InitializeComponent();
            rocheDBContext = new RocheDataEntities();
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void OrderOverview_Load(object sender, EventArgs e)
        {
            ReloadGridView();
        }


        private void ReloadGridView()
        {
            rdGridAuftrag.DataSource = null;
            rdGridAuftrag.DataSource = rocheDBContext.Database
                .SqlQuery<KopfAltModel>(mySqlAKopf, new SqlParameter("@Anzahl", 25)).ToList();


            foreach (GridViewColumn column in rdGridAuftrag.Columns)
            {
                column.AutoSizeMode = BestFitColumnMode.AllCells;
                column.BestFit();
                column.TextAlignment = ContentAlignment.MiddleLeft;
            }

            rdGridAuftrag.ClearSelection();
            foreach (GridViewColumn column in radGridLot.Columns)
            {
                column.AutoSizeMode = BestFitColumnMode.AllCells;
                column.BestFit();
                column.TextAlignment = ContentAlignment.MiddleLeft;
            }

            radGridLot.ClearSelection();
            foreach (GridViewColumn column in rdGridDetail.Columns)
            {
                column.AutoSizeMode = BestFitColumnMode.AllCells;
                column.BestFit();
                column.TextAlignment = ContentAlignment.MiddleLeft;
            }

            rdGridDetail.ClearSelection();
            foreach (GridViewColumn column in radGridGebinde.Columns)
            {
                column.AutoSizeMode = BestFitColumnMode.AllCells;
                column.BestFit();
                column.TextAlignment = ContentAlignment.MiddleLeft;
            }

            radGridGebinde.ClearSelection();
        }

        private void rdGridAuftrag_CurrentRowChanged(object sender, CurrentRowChangedEventArgs e)
        {
            if (e.CurrentRow != null && e.CurrentRow is GridViewDataRowInfo)
            {
                var kopf = e.CurrentRow.DataBoundItem as KopfAltModel;
                radGridLot.DataSource = rocheDBContext.Database
                    .SqlQuery<ALot>("SELECT * From ALot WHERE Auftrag = @Auftrag",
                        new SqlParameter("@Auftrag", kopf.Nummer)).ToList();
            }
        }

        private void radGridLot_CurrentRowChanged(object sender, CurrentRowChangedEventArgs e)
        {
            if (e.CurrentRow != null && e.CurrentRow is GridViewDataRowInfo)
            {
                var lot = e.CurrentRow.DataBoundItem as ALot;
                rdGridDetail.DataSource = rocheDBContext.Database
                    .SqlQuery<ADetail>("SELECT * From ADetail WHERE Lot = @Lot", new SqlParameter("@Lot", lot.Nummer))
                    .ToList();
            }
        }

        private void rdGridDetail_CurrentRowChanged(object sender, CurrentRowChangedEventArgs e)
        {
            if (e.CurrentRow != null && e.CurrentRow is GridViewDataRowInfo)
            {
                var detail = e.CurrentRow.DataBoundItem as ADetail;
                radGridGebinde.DataSource = rocheDBContext.Database
                    .SqlQuery<Gebinde>("SELECT *, Gebinde as Gebinde1 From Gebinde WHERE  Detail = @Detail",
                        new SqlParameter("@Detail", detail.Nummer)).ToList();
            }
        }
    }
}