﻿namespace MT.GUPAS.Pages
{
    partial class OrderOverview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn1 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn2 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn3 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn4 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            this.btClose = new System.Windows.Forms.Button();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.rdGridAuftrag = new Telerik.WinControls.UI.RadGridView();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.radGridLot = new Telerik.WinControls.UI.RadGridView();
            this.frame = new Telerik.WinControls.UI.RadGroupBox();
            this.rdGridDetail = new Telerik.WinControls.UI.RadGridView();
            this.radGroupBox4 = new Telerik.WinControls.UI.RadGroupBox();
            this.radGridGebinde = new Telerik.WinControls.UI.RadGridView();
            this.aKopfBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rdGridAuftrag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdGridAuftrag.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridLot.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frame)).BeginInit();
            this.frame.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rdGridDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdGridDetail.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox4)).BeginInit();
            this.radGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridGebinde)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridGebinde.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aKopfBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // btClose
            // 
            this.btClose.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btClose.Location = new System.Drawing.Point(880, 725);
            this.btClose.Name = "btClose";
            this.btClose.Size = new System.Drawing.Size(191, 40);
            this.btClose.TabIndex = 11;
            this.btClose.Text = "Schliessen";
            this.btClose.UseVisualStyleBackColor = true;
            this.btClose.Click += new System.EventHandler(this.btClose_Click);
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.radGroupBox1.Controls.Add(this.rdGridAuftrag);
            this.radGroupBox1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGroupBox1.HeaderMargin = new System.Windows.Forms.Padding(1);
            this.radGroupBox1.HeaderText = "Aufträge";
            this.radGroupBox1.Location = new System.Drawing.Point(12, 2);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(1080, 170);
            this.radGroupBox1.TabIndex = 12;
            this.radGroupBox1.Text = "Aufträge";
            ((Telerik.WinControls.UI.GroupBoxHeader)(this.radGroupBox1.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Segoe UI", 11.25F);
            ((Telerik.WinControls.UI.GroupBoxHeader)(this.radGroupBox1.GetChildAt(0).GetChildAt(1))).Margin = new System.Windows.Forms.Padding(1);
            // 
            // rdGridAuftrag
            // 
            this.rdGridAuftrag.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdGridAuftrag.Location = new System.Drawing.Point(5, 30);
            // 
            // 
            // 
            this.rdGridAuftrag.MasterTemplate.AllowAddNewRow = false;
            this.rdGridAuftrag.MasterTemplate.AllowDragToGroup = false;
            gridViewTextBoxColumn1.FieldName = "ANr";
            gridViewTextBoxColumn1.HeaderText = "ANr";
            gridViewTextBoxColumn1.IsAutoGenerated = true;
            gridViewTextBoxColumn1.MinWidth = 50;
            gridViewTextBoxColumn1.Name = "ANr";
            gridViewTextBoxColumn2.FieldName = "BArt";
            gridViewTextBoxColumn2.HeaderText = "BArt";
            gridViewTextBoxColumn2.IsAutoGenerated = true;
            gridViewTextBoxColumn2.MinWidth = 50;
            gridViewTextBoxColumn2.Name = "BArt";
            gridViewTextBoxColumn3.FieldName = "Besteller";
            gridViewTextBoxColumn3.HeaderText = "Besteller";
            gridViewTextBoxColumn3.IsAutoGenerated = true;
            gridViewTextBoxColumn3.MinWidth = 50;
            gridViewTextBoxColumn3.Name = "Besteller";
            gridViewDecimalColumn1.DataType = typeof(System.Nullable<float>);
            gridViewDecimalColumn1.FieldName = "Total";
            gridViewDecimalColumn1.HeaderText = "Total";
            gridViewDecimalColumn1.IsAutoGenerated = true;
            gridViewDecimalColumn1.MinWidth = 50;
            gridViewDecimalColumn1.Name = "Total";
            this.rdGridAuftrag.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewDecimalColumn1});
            this.rdGridAuftrag.MasterTemplate.DataSource = this.aKopfBindingSource;
            this.rdGridAuftrag.MasterTemplate.ShowRowHeaderColumn = false;
            this.rdGridAuftrag.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.rdGridAuftrag.Name = "rdGridAuftrag";
            this.rdGridAuftrag.ReadOnly = true;
            this.rdGridAuftrag.Size = new System.Drawing.Size(1054, 130);
            this.rdGridAuftrag.TabIndex = 4;
            this.rdGridAuftrag.CurrentRowChanged += new Telerik.WinControls.UI.CurrentRowChangedEventHandler(this.rdGridAuftrag_CurrentRowChanged);
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.radGroupBox2.Controls.Add(this.radGridLot);
            this.radGroupBox2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGroupBox2.HeaderMargin = new System.Windows.Forms.Padding(1);
            this.radGroupBox2.HeaderText = "Lot";
            this.radGroupBox2.Location = new System.Drawing.Point(12, 178);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Size = new System.Drawing.Size(1080, 170);
            this.radGroupBox2.TabIndex = 13;
            this.radGroupBox2.Text = "Lot";
            ((Telerik.WinControls.UI.GroupBoxHeader)(this.radGroupBox2.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Segoe UI", 11.25F);
            ((Telerik.WinControls.UI.GroupBoxHeader)(this.radGroupBox2.GetChildAt(0).GetChildAt(1))).Margin = new System.Windows.Forms.Padding(1);
            // 
            // radGridLot
            // 
            this.radGridLot.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGridLot.Location = new System.Drawing.Point(5, 30);
            // 
            // 
            // 
            this.radGridLot.MasterTemplate.AllowAddNewRow = false;
            this.radGridLot.MasterTemplate.AllowDragToGroup = false;
            gridViewTextBoxColumn4.FieldName = "Name";
            gridViewTextBoxColumn4.HeaderText = "Name";
            gridViewTextBoxColumn4.IsAutoGenerated = true;
            gridViewTextBoxColumn4.MinWidth = 50;
            gridViewTextBoxColumn4.Name = "Name";
            gridViewTextBoxColumn5.FieldName = "ANr";
            gridViewTextBoxColumn5.HeaderText = "ANr";
            gridViewTextBoxColumn5.IsAutoGenerated = true;
            gridViewTextBoxColumn5.MinWidth = 50;
            gridViewTextBoxColumn5.Name = "ANr";
            gridViewTextBoxColumn6.FieldName = "BArt";
            gridViewTextBoxColumn6.HeaderText = "BArt";
            gridViewTextBoxColumn6.IsAutoGenerated = true;
            gridViewTextBoxColumn6.MinWidth = 50;
            gridViewTextBoxColumn6.Name = "BArt";
            gridViewTextBoxColumn7.FieldName = "Besteller";
            gridViewTextBoxColumn7.HeaderText = "Besteller";
            gridViewTextBoxColumn7.IsAutoGenerated = true;
            gridViewTextBoxColumn7.MinWidth = 50;
            gridViewTextBoxColumn7.Name = "Besteller";
            gridViewDecimalColumn2.DataType = typeof(float);
            gridViewDecimalColumn2.FieldName = "Total";
            gridViewDecimalColumn2.HeaderText = "Total";
            gridViewDecimalColumn2.IsAutoGenerated = true;
            gridViewDecimalColumn2.MinWidth = 50;
            gridViewDecimalColumn2.Name = "Total";
            this.radGridLot.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewDecimalColumn2});
            this.radGridLot.MasterTemplate.ShowRowHeaderColumn = false;
            this.radGridLot.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.radGridLot.Name = "radGridLot";
            this.radGridLot.ReadOnly = true;
            this.radGridLot.Size = new System.Drawing.Size(1054, 130);
            this.radGridLot.TabIndex = 4;
            this.radGridLot.CurrentRowChanged += new Telerik.WinControls.UI.CurrentRowChangedEventHandler(this.radGridLot_CurrentRowChanged);
            // 
            // frame
            // 
            this.frame.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frame.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.frame.Controls.Add(this.rdGridDetail);
            this.frame.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frame.HeaderMargin = new System.Windows.Forms.Padding(1);
            this.frame.HeaderText = "Auftragsdetail";
            this.frame.Location = new System.Drawing.Point(12, 354);
            this.frame.Name = "frame";
            this.frame.Size = new System.Drawing.Size(1080, 170);
            this.frame.TabIndex = 14;
            this.frame.Text = "Auftragsdetail";
            ((Telerik.WinControls.UI.GroupBoxHeader)(this.frame.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Segoe UI", 11.25F);
            ((Telerik.WinControls.UI.GroupBoxHeader)(this.frame.GetChildAt(0).GetChildAt(1))).Margin = new System.Windows.Forms.Padding(1);
            // 
            // rdGridDetail
            // 
            this.rdGridDetail.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdGridDetail.Location = new System.Drawing.Point(5, 30);
            // 
            // 
            // 
            this.rdGridDetail.MasterTemplate.AllowAddNewRow = false;
            this.rdGridDetail.MasterTemplate.AllowDragToGroup = false;
            gridViewTextBoxColumn8.FieldName = "Name";
            gridViewTextBoxColumn8.HeaderText = "Name";
            gridViewTextBoxColumn8.IsAutoGenerated = true;
            gridViewTextBoxColumn8.MinWidth = 50;
            gridViewTextBoxColumn8.Name = "Name";
            gridViewTextBoxColumn9.FieldName = "ANr";
            gridViewTextBoxColumn9.HeaderText = "ANr";
            gridViewTextBoxColumn9.IsAutoGenerated = true;
            gridViewTextBoxColumn9.MinWidth = 50;
            gridViewTextBoxColumn9.Name = "ANr";
            gridViewTextBoxColumn10.FieldName = "BArt";
            gridViewTextBoxColumn10.HeaderText = "BArt";
            gridViewTextBoxColumn10.IsAutoGenerated = true;
            gridViewTextBoxColumn10.MinWidth = 50;
            gridViewTextBoxColumn10.Name = "BArt";
            gridViewTextBoxColumn11.FieldName = "Besteller";
            gridViewTextBoxColumn11.HeaderText = "Besteller";
            gridViewTextBoxColumn11.IsAutoGenerated = true;
            gridViewTextBoxColumn11.MinWidth = 50;
            gridViewTextBoxColumn11.Name = "Besteller";
            gridViewDecimalColumn3.DataType = typeof(float);
            gridViewDecimalColumn3.FieldName = "Total";
            gridViewDecimalColumn3.HeaderText = "Total";
            gridViewDecimalColumn3.IsAutoGenerated = true;
            gridViewDecimalColumn3.MinWidth = 50;
            gridViewDecimalColumn3.Name = "Total";
            this.rdGridDetail.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewDecimalColumn3});
            this.rdGridDetail.MasterTemplate.ShowRowHeaderColumn = false;
            this.rdGridDetail.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.rdGridDetail.Name = "rdGridDetail";
            this.rdGridDetail.ReadOnly = true;
            this.rdGridDetail.Size = new System.Drawing.Size(1054, 130);
            this.rdGridDetail.TabIndex = 4;
            this.rdGridDetail.CurrentRowChanged += new Telerik.WinControls.UI.CurrentRowChangedEventHandler(this.rdGridDetail_CurrentRowChanged);
            // 
            // radGroupBox4
            // 
            this.radGroupBox4.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.radGroupBox4.Controls.Add(this.radGridGebinde);
            this.radGroupBox4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGroupBox4.HeaderMargin = new System.Windows.Forms.Padding(1);
            this.radGroupBox4.HeaderText = "Gebinde";
            this.radGroupBox4.Location = new System.Drawing.Point(12, 530);
            this.radGroupBox4.Name = "radGroupBox4";
            this.radGroupBox4.Size = new System.Drawing.Size(1080, 170);
            this.radGroupBox4.TabIndex = 15;
            this.radGroupBox4.Text = "Gebinde";
            ((Telerik.WinControls.UI.GroupBoxHeader)(this.radGroupBox4.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Segoe UI", 11.25F);
            ((Telerik.WinControls.UI.GroupBoxHeader)(this.radGroupBox4.GetChildAt(0).GetChildAt(1))).Margin = new System.Windows.Forms.Padding(1);
            // 
            // radGridGebinde
            // 
            this.radGridGebinde.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGridGebinde.Location = new System.Drawing.Point(5, 30);
            // 
            // 
            // 
            this.radGridGebinde.MasterTemplate.AllowAddNewRow = false;
            this.radGridGebinde.MasterTemplate.AllowDragToGroup = false;
            gridViewTextBoxColumn12.FieldName = "Name";
            gridViewTextBoxColumn12.HeaderText = "Name";
            gridViewTextBoxColumn12.IsAutoGenerated = true;
            gridViewTextBoxColumn12.MinWidth = 50;
            gridViewTextBoxColumn12.Name = "Name";
            gridViewTextBoxColumn13.FieldName = "ANr";
            gridViewTextBoxColumn13.HeaderText = "ANr";
            gridViewTextBoxColumn13.IsAutoGenerated = true;
            gridViewTextBoxColumn13.MinWidth = 50;
            gridViewTextBoxColumn13.Name = "ANr";
            gridViewTextBoxColumn14.FieldName = "BArt";
            gridViewTextBoxColumn14.HeaderText = "BArt";
            gridViewTextBoxColumn14.IsAutoGenerated = true;
            gridViewTextBoxColumn14.MinWidth = 50;
            gridViewTextBoxColumn14.Name = "BArt";
            gridViewTextBoxColumn15.FieldName = "Besteller";
            gridViewTextBoxColumn15.HeaderText = "Besteller";
            gridViewTextBoxColumn15.IsAutoGenerated = true;
            gridViewTextBoxColumn15.MinWidth = 50;
            gridViewTextBoxColumn15.Name = "Besteller";
            gridViewDecimalColumn4.DataType = typeof(float);
            gridViewDecimalColumn4.FieldName = "Total";
            gridViewDecimalColumn4.HeaderText = "Total";
            gridViewDecimalColumn4.IsAutoGenerated = true;
            gridViewDecimalColumn4.MinWidth = 50;
            gridViewDecimalColumn4.Name = "Total";
            this.radGridGebinde.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewDecimalColumn4});
            this.radGridGebinde.MasterTemplate.ShowRowHeaderColumn = false;
            this.radGridGebinde.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.radGridGebinde.Name = "radGridGebinde";
            this.radGridGebinde.ReadOnly = true;
            this.radGridGebinde.Size = new System.Drawing.Size(1054, 130);
            this.radGridGebinde.TabIndex = 4;
            // 
            // aKopfBindingSource
            // 
            this.aKopfBindingSource.DataSource = typeof(MT.GUPAS.Models.AKopf);
            // 
            // OrderOverview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1104, 794);
            this.Controls.Add(this.radGroupBox4);
            this.Controls.Add(this.frame);
            this.Controls.Add(this.radGroupBox2);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.btClose);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1112, 827);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1112, 827);
            this.Name = "OrderOverview";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.RootElement.MaxSize = new System.Drawing.Size(1112, 827);
            this.Text = "Auftragsübersicht";
            this.Load += new System.EventHandler(this.OrderOverview_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rdGridAuftrag.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdGridAuftrag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridLot.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frame)).EndInit();
            this.frame.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rdGridDetail.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdGridDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox4)).EndInit();
            this.radGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridGebinde.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridGebinde)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aKopfBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btClose;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadGridView rdGridAuftrag;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadGridView radGridLot;
        private Telerik.WinControls.UI.RadGroupBox frame;
        private Telerik.WinControls.UI.RadGridView rdGridDetail;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox4;
        private Telerik.WinControls.UI.RadGridView radGridGebinde;
        private System.Windows.Forms.BindingSource aKopfBindingSource;
    }
}
