﻿using System;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using MT.GUPAS.Infrastructure;
using MT.GUPAS.Logic;
using MT.GUPAS.Models;
using MT.GUPAS.Reporting;
using Telerik.Reporting;
using Telerik.WinControls.UI;

namespace MT.GUPAS.Pages
{
    public partial class UserForm : RadForm
    {
        private RocheDataEntities rocheDBContext;
        private DbSet<User> rocheUser;

        public UserForm()
        {
            InitializeComponent();
        }

        private void btNew_Click(object sender, EventArgs e)
        {
            var ab = new UserEdit(rocheDBContext, null);
            ab.ShowDialog();
            gridUser.DataSource = null;
            gridUser.DataSource = rocheUser.OrderBy(d => d.Name).ToList();
            gridUser.ClearSelection();
        }

        private async void UserForm_Load(object sender, EventArgs e)
        {
            rocheDBContext = new RocheDataEntities();
            rocheUser = rocheDBContext.Users;
            await rocheUser.ForEachAsync(item => { item.Level2 = AccessRightHelper.AccessRight[item.Level]; });
            bindingSource1.DataSource = rocheUser.OrderBy(d => d.Name).ToList();
            ReloadGridView();
        }

        private void btDelete_Click(object sender, EventArgs e)
        {
            var current = gridUser.CurrentRow.DataBoundItem as User;

            if (current != null)
            {
                var result = rocheDBContext.Database.SqlQuery<AKopf>("SELECT * FROM AKOPF WHERE Ersteller = @Ersteller",
                    new SqlParameter("@Ersteller", current.Nummer)).ToList();

                if (result.Count == 0)
                {
                    var res = MessageBox.Show(this, "Wollen Sie diesen Eintrag löschen?", "Löschen",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (res == DialogResult.Yes)
                    {
                        rocheUser.Remove(current);
                        rocheDBContext.SaveChanges();
                        ManageAuditTail.WriteAuditTrial(AuditKlasse.Tabelle, AuditAction.Loeschen,
                            $"User: {current.Name}", "");
                        ReloadGridView();
                    }
                }
                else
                {
                    MessageBox.Show(this, "Eintrag kann nicht gelöscht werden, da der Artikel schon verwendet wird!",
                        "Löschen", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void btEdit_Click(object sender, EventArgs e)
        {
            var current = gridUser.CurrentRow.DataBoundItem as User;

            if (current != null)
            {
                var ab = new UserEdit(rocheDBContext, current);
                ab.ShowDialog();
                ReloadGridView();
            }
        }

        private void ReloadGridView()
        {
            gridUser.Columns.Clear();
            gridUser.DataSource = null;
            gridUser.DataSource = rocheUser.OrderBy(d => d.Name).ToList();

            foreach (GridViewColumn column in gridUser.Columns)
            {
                column.AutoSizeMode = BestFitColumnMode.AllCells;
                column.BestFit();
                column.TextAlignment = ContentAlignment.MiddleLeft;
            }

            gridUser.ClearSelection();
        }


        private void btClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void tbPrint_Click(object sender, EventArgs e)
        {
            var source = new InstanceReportSource();
            var report = new WUser();

            report.ReportParameters["Ersteller"].Value = Globals.User.Name.Trim();
            foreach (var item in gridUser.Rows)
            {
            }

            report.DataSource = bindingSource1;
            source.ReportDocument = report;

            var hugo = new ReportViewer(source);
            hugo.Text = "Benutzer Liste";
            hugo.ShowDialog();
            ManageAuditTail.WriteAuditTrial(AuditKlasse.Protokoll, AuditAction.Ausdrucken, "Benutzer-Liste", "");
        }
    }
}