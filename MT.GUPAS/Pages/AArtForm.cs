﻿using System;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using MT.GUPAS.Infrastructure;
using MT.GUPAS.Models;
using Telerik.WinControls.UI;

namespace MT.GUPAS.Pages
{
    public partial class AArtForm : RadForm
    {
        private DbSet<Bestellart> rocheAArt;
        private RocheDataEntities rocheDBContext;

        public AArtForm()
        {
            InitializeComponent();
        }

        private void btNew_Click(object sender, EventArgs e)
        {
            var ab = new AArtEdit(rocheDBContext, null);
            ab.ShowDialog();
            gridUser.DataSource = null;
            gridUser.DataSource = rocheAArt.ToList();
            gridUser.ClearSelection();
        }

        private void UserForm_Load(object sender, EventArgs e)
        {
            rocheDBContext = new RocheDataEntities();
            rocheAArt = rocheDBContext.Bestellarts;
            bindingSource1.DataSource = rocheAArt.ToList();
            ReloadGridView();
        }

        private void btDelete_Click(object sender, EventArgs e)
        {
            var res = MessageBox.Show(this, "Wollen Sie diesen Eintrag löschen?", "Löschen", MessageBoxButtons.YesNo,
                MessageBoxIcon.Question);

            if (res == DialogResult.Yes)
            {
                var current = gridUser.CurrentRow.DataBoundItem as Bestellart;

                if (current != null)
                    rocheAArt.Remove(current);
                rocheDBContext.SaveChanges();
                ManageAuditTail.WriteAuditTrial(AuditKlasse.Tabelle, AuditAction.Loeschen, $"AArt: {current.Wert}", "");
                ReloadGridView();
            }
        }

        private void btEdit_Click(object sender, EventArgs e)
        {
            var current = gridUser.CurrentRow.DataBoundItem as Bestellart;

            if (current != null)
            {
                var ab = new AArtEdit(rocheDBContext, current);
                ab.ShowDialog();
                ReloadGridView();
            }
        }

        private void ReloadGridView()
        {
            gridUser.DataSource = null;
            gridUser.DataSource = rocheAArt.ToList();
            foreach (GridViewColumn column in gridUser.Columns)
            {
                column.AutoSizeMode = BestFitColumnMode.AllCells;
                column.BestFit();
                column.TextAlignment = ContentAlignment.MiddleLeft;
            }

            gridUser.ClearSelection();
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}