﻿using System;
using System.Data.Entity;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Windows.Forms;
using MT.GUPAS.Infrastructure;
using MT.GUPAS.Models;
using Telerik.WinControls.UI;

namespace MT.GUPAS.Pages
{
    [SuppressMessage("ReSharper", "SpecifyACultureInStringConversionExplicitly")]
    [SuppressMessage("ReSharper", "LocalizableElement")]
    public partial class AArtEdit : RadForm
    {
        private readonly RocheDataEntities _context;
        private readonly Bestellart _currentElement;
        private readonly Bestellart _element;
        private readonly DbSet<Bestellart> _elements;
        private string _wertAlt;

        public AArtEdit(RocheDataEntities context, Bestellart element)
        {
            InitializeComponent();
            _element = element;
            _context = context;
            _elements = _context.Bestellarts;
            _currentElement = element == null ? new Bestellart() : _elements.Find(element.Nummer);

            LoadElement();
        }

        private void LoadElement()
        {
            tbBezeichung.Text = _wertAlt = _currentElement.Wert.Trim();
        }

        #region Leave (Lost Focus)

        private void tbBezeichung_TextChanged(object sender, EventArgs e)
        {
            _currentElement.Wert = tbBezeichung.Text.Trim();
        }

        #endregion

        private void btCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        #region Buttons

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (CheckEmptyFields())
            {
                MessageBox.Show("Es müssem alle Felder Eingaben enthalten!", "Felder", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                return;
            }


            if (_element == null && _elements.ToList().Find(d => d.Wert.Trim() == tbBezeichung.Text.Trim()) != null)
            {
                MessageBox.Show("Bestellart bereits vorhanden!", "Bestellart", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                return;
            }


            if (_element == null)
            {
                _elements.Add(_currentElement);
                ManageAuditTail.WriteAuditTrial(AuditKlasse.Tabelle, AuditAction.Erstellen, "AArt: Alt N/A",
                    $"Neu {_currentElement.Wert}");
            }
            else
            {
                ManageAuditTail.WriteAuditTrial(AuditKlasse.Tabelle, AuditAction.Aendern, $"AArt: Alt {_wertAlt}",
                    $"Neu {_currentElement.Wert}");
            }


            _context.SaveChanges();
            Close();
        }

        private bool CheckEmptyFields()
        {
            return tbBezeichung.Text == "";
        }

        #endregion
    }
}