﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using MT.GUPAS.Infrastructure;
using MT.GUPAS.Models;
using Telerik.WinControls.UI;

namespace MT.GUPAS.Pages
{
    public partial class cbEtikette : RadForm
    {
        private DbSet<Bestellart> rocheBestellart;
        private RocheDataEntities rocheDBContext;
        private DbSet<Komp> rocheKomp;
        private readonly Dictionary<int, string> rooms = new Dictionary<int, string>();
        private SystemM system;

        public cbEtikette()
        {
            InitializeComponent();
        }

        private void NewOrderForm_Load(object sender, EventArgs e)
        {
            rocheDBContext = new RocheDataEntities();
            rocheKomp = rocheDBContext.Komps;
            var x = rocheKomp.Where(d => d.Active > 0).OrderBy(f => f.Name).ToList();
            foreach (var item in x)
            {
                item.Name = item.Name?.Trim();
                item.ArtNr = item.ArtNr?.Trim();
                item.GenesysNr = item.GenesysNr?.Trim();
            }

            //this.kompBindingSource.DataSource = rocheKomp.Where(d => d.Active > 0).OrderBy(f => f.Name).ToList();
            kompBindingSource.DataSource = x;
            ddArtNr.AutoCompleteDataSource = kompBindingSource;
            ddArtNr.AutoCompleteDisplayMember = "ArtNr";

            rocheBestellart = rocheDBContext.Bestellarts;
            bestellartBindingSource.DataSource = rocheBestellart.ToList();
            system = rocheDBContext.SystemMs.First();
            ReloadGridViewNew();
            rooms.Add(1, "Kabine 2/2");
            rooms.Add(2, "Kabine 2/4");
            rooms.Add(3, "Beide");
            rooms.Add(4, "Kabine 14");
            cbRooms.DataSource = new BindingSource(rooms, null);
            cbRooms.DisplayMember = "Value";
            cbRooms.ValueMember = "Key";
            cbRooms.SelectedIndex = 2;

            var bArt = rocheBestellart.FirstOrDefault(d => d.Nummer == system.BArt);
            if (bArt != null)
                cbArt.SelectedIndex = cbArt.FindString(bArt.Wert);

            var komp = rocheKomp.FirstOrDefault(d => d.Nummer == system.Komp);
            if (komp != null)
                cbArtikelname.SelectedIndex = cbArtikelname.FindString(komp.Name);
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            var i = rocheDBContext.Staffels.Count();
            if (i > 0)
            {
                var res = MessageBox.Show(this, "Eingabe abbrechen?", "Beenden", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question);

                if (res == DialogResult.Yes)
                {
                    rocheDBContext.Staffels.RemoveRange(rocheDBContext.Staffels);
                    rocheDBContext.StaffelGebindes.RemoveRange(rocheDBContext.StaffelGebindes);
                    rocheDBContext.SaveChanges();
                    Close();
                }
            }
            else
            {
                Close();
            }
        }

        private void btReset_Click(object sender, EventArgs e)
        {
            txtLotMG.Text = "";
            txtAnzahl.Text = "0";
            txtMenge.Text = "0.0";
            cbAutoLot.Checked = true;
            cbGebindeNr.Checked = false;
            cbRooms.SelectedIndex = 2;
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            double Menge = 0;
            double Total = 0;
            var Anzahl = 0;
            var GebindeNr = 9999999;
            var first = true;
            Staffel st = null;
            StaffelGebinde sg = null;

            do
            {
                if (double.TryParse(txtMenge.Text, out Menge))
                {
                    if (Menge == 0)
                    {
                        var usf = new InputBoxS("Zielmenge", "Zielmenge [kg]", "", true);
                        if (usf.ShowDialog() == DialogResult.OK)
                        {
                            txtMenge.Text = Convert.ToString(usf.Tag);
                            Menge = double.Parse(txtMenge.Text);
                        }
                        else
                        {
                            return;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Bitte geben Sie die Zielmenge als Zahl ein.", "Zielmenge", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    txtMenge.Text = "0";
                    return;
                }
            } while (Menge == 0);

            do
            {
                if (int.TryParse(txtAnzahl.Text, out Anzahl))
                {
                    if (Anzahl == 0)
                    {
                        var usf = new InputBoxS("Anzahl Gebinde", "Anzahl", "", true);
                        if (usf.ShowDialog() == DialogResult.OK)
                        {
                            txtAnzahl.Text = Convert.ToString(usf.Tag);
                            Anzahl = int.Parse(txtAnzahl.Text);
                        }
                        else
                        {
                            return;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Bitte geben Sie die Anzahl als Zahl ein.", "Anzahl", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    txtAnzahl.Text = "0";
                    return;
                }
            } while (Anzahl == 0);


            while (txtLotMG.Text.Length == 0 || txtLotMG.Text == "0")
            {
                var usf = new InputBoxS("Lot-Nr. des MG", "Lot-Nr. des MG", "", false);
                if (usf.ShowDialog() == DialogResult.OK)
                    txtLotMG.Text = Convert.ToString(usf.Tag).ToUpper();
                else
                    return;
            }


            while (GebindeNr > 0)
            {
                if (cbGebindeNr.Checked == false)
                {
                    var usf = new InputBoxS("Gebindenummer", "Nr. oder 0 für Abschluss eingeben !", "", true, true);
                    if (usf.ShowDialog() == DialogResult.OK)
                        GebindeNr = Convert.ToInt32(usf.Tag);
                    else
                        return;

                    if (GebindeNr == 0 && first)
                    {
                        MessageBox.Show(
                            "Sie müssen mindestens 1 Gebinde-Nr. angeben\noder die entsprechende Checkbox setzen !");
                        return;
                    }
                }

                if (first)
                {
                    first = false;
                    st = new Staffel
                    {
                        Menge = Convert.ToSingle(Menge),
                        Anzahl = Anzahl,
                        LotMG = txtLotMG.Text.Trim().ToUpper(),
                        LotZG = cbAutoLot.Checked ? "*" : txtLotZG.Text.Trim().ToUpper(),
                        Total = Convert.ToSingle(Menge * Anzahl),
                        AnzMGGebinde = 0
                    };
                    rocheDBContext.Staffels.Add(st);
                    rocheDBContext.SaveChanges();
                }

                if (GebindeNr != 0 || cbGebindeNr.Checked)
                {
                    sg = new StaffelGebinde
                    {
                        Staffel = st.Nummer,
                        Gebinde = cbGebindeNr.Checked ? "*" : GebindeNr.ToString().Trim()
                    };
                    st.AnzMGGebinde++;
                    rocheDBContext.StaffelGebindes.Add(sg);
                    rocheDBContext.SaveChanges();

                    Total = rocheDBContext.Staffels.Sum(d => d.Total);
                    txtTotal.Text = Total.ToString("N7");
                    txtTotal.Tag = rocheDBContext.Staffels.Sum(d => d.Anzahl);

                    if (cbGebindeNr.Checked) GebindeNr = 0;
                }
                //      txtAnzahl.Text = "0"; txtMenge.Text = "0.0"; 
            }

            ReloadGridViewNew();
        }

        private void ReloadGridViewNew()
        {
            dgStaffel.DataSource = null;
            //dgStaffel.DataSource = rocheDBContext.Database.SqlQuery<KopfNeuModel>(mySqlAKopfNeuList).ToList();
            dgStaffel.DataSource = rocheDBContext.Database
                .SqlQuery<StaffelModel>("SELECT Nummer, Anzahl, Menge, Total, AnzMGGebinde FROM Staffel").ToList();
            if (dgStaffel.Rows.Count > 0)
                foreach (GridViewColumn column in dgStaffel.Columns)
                {
                    column.AutoSizeMode = BestFitColumnMode.AllCells;
                    column.BestFit();
                    column.TextAlignment = ContentAlignment.MiddleRight;
                }
        }

        private void btDelete_Click(object sender, EventArgs e)
        {
            double Total = 0;
            if (rocheDBContext.Staffels.Count() > 0)
            {
                var current = dgStaffel.CurrentRow.DataBoundItem as StaffelModel;
                if (current != null)
                {
                    var res = MessageBox.Show(this, "Wollen Sie diesen Eintrag löschen?", "Löschen",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (res == DialogResult.Yes)
                    {
                        var st = rocheDBContext.Staffels.Find(current.Nummer);
                        var sg = rocheDBContext.StaffelGebindes.Where(d => d.Staffel == st.Nummer).ToList();
                        foreach (var item in sg) rocheDBContext.StaffelGebindes.Remove(item);
                        rocheDBContext.Staffels.Remove(st);
                        rocheDBContext.SaveChanges();
                        if (rocheDBContext.Staffels.Count() > 0)
                        {
                            Total = rocheDBContext.Staffels.Sum(d => d.Total);
                            txtTotal.Text = Total.ToString("N7");
                            txtTotal.Tag = rocheDBContext.Staffels.Sum(d => d.Anzahl);
                        }
                    }
                }
            }
            else
            {
                txtTotal.Text = "0.0";
                txtTotal.Tag = 0;
            }

            ReloadGridViewNew();
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            var system = rocheDBContext.SystemMs.First();

            if (txtANr.Text.Length == 0 || txtANr.Text == "0")
            {
                MessageBox.Show("Auftragsnummer fehlt", "Auftragsnummer", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                return;
            }

            if (rocheDBContext.Staffels.Count() == 0)
            {
                MessageBox.Show("Staffelung fehlt", "Staffelung", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            var kopf = new AKopf
            {
                ANr = txtANr.Text,
                MG = Convert.ToInt32(cbArtikelname.SelectedValue),
                BArt = cbArt.Text.Trim(),
                Besteller = txtClient.Text.Trim(),
                Ersteller = Globals.User.Nummer,
                AutoEti = cbEti.Checked ? 1 : 0,
                ZeitNew = DateTime.Now,
                Total = Convert.ToSingle(txtTotal.Text),
                Kabine = Convert.ToInt32(cbRooms.SelectedValue),
                Komplett = false,
                AnzZeilen = Convert.ToInt32(txtTotal.Tag),
                AnzZeilenTotal = Convert.ToInt32(txtTotal.Tag),
                EtiketteNr = 1,
                AnzPrint = 0
            };
            rocheDBContext.AKopfs.Add(kopf);
            rocheDBContext.SaveChanges();
            ManageAuditTail.WriteAuditTrial(AuditKlasse.Auftrag, AuditAction.Erstellen, $"Auftrag# {kopf.Nummer}", "");
            var staffel = rocheDBContext.Staffels.ToList();
            var lastLot = "";
            ALot lot = null;
            foreach (var item in staffel)
            {
                if (lastLot != item.LotMG)
                {
                    lastLot = item.LotMG;
                    lot = new ALot
                    {
                        Lot = item.LotMG.Trim(),
                        Auftrag = kopf.Nummer
                    };
                    rocheDBContext.ALots.Add(lot);
                    rocheDBContext.SaveChanges();
                }

                for (var m = 0; m < item.Anzahl; m++)
                {
                    var ad = new ADetail
                    {
                        Lot = lot.Nummer,
                        Soll = item.Menge,
                        SollM = GetMTol(item.Menge),
                        SollP = GetPTol(item.Menge),
                        ZielLot = item.LotZG.Trim()
                    };
                    rocheDBContext.ADetails.Add(ad);
                    rocheDBContext.SaveChanges();

                    var sg = rocheDBContext.StaffelGebindes.Where(d => d.Staffel == item.Nummer).ToList();
                    foreach (var item2 in sg)
                    {
                        var gb = new Gebinde
                        {
                            Detail = ad.Nummer,
                            Gebinde1 = item2.Gebinde.Trim(),
                            Status = 2,
                            Menge = 0
                        };
                        rocheDBContext.Gebindes.Add(gb);
                    }

                    rocheDBContext.SaveChanges();
                }
            }

            rocheDBContext.Staffels.RemoveRange(rocheDBContext.Staffels);
            rocheDBContext.StaffelGebindes.RemoveRange(rocheDBContext.StaffelGebindes);
            rocheDBContext.SaveChanges();
            var llot = rocheDBContext.ALots.Where(d => d.Auftrag == kopf.Nummer).ToList();
            foreach (var item3 in llot)
            {
                var ma = new Main();
                ma.PrintWlist(item3.Nummer);
            }

            txtANr.Tag = 1;
            system.BArt = Convert.ToInt32(cbArt.SelectedValue);
            system.Komp = Convert.ToInt32(cbArtikelname.SelectedValue);
            rocheDBContext.SaveChanges();

            Close();
        }

        private float GetMTol(float soll)
        {
            return soll;
        }

        private float GetPTol(float soll)
        {
            var tol = rocheDBContext.Toleranzs.OrderBy(r => r.LowLimit).FirstOrDefault(d => d.HighLimit > soll);
            if (tol == null)
                return soll;
            return soll + Convert.ToSingle(tol.Wert);
        }
    }
}