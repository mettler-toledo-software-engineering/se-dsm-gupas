﻿using System;
using System.Data.Entity;
using System.Diagnostics.CodeAnalysis;
using System.Windows.Forms;
using MT.GUPAS.Infrastructure;
using MT.GUPAS.Models;
using Telerik.WinControls.UI;

namespace MT.GUPAS.Pages
{
    [SuppressMessage("ReSharper", "SpecifyACultureInStringConversionExplicitly")]
    [SuppressMessage("ReSharper", "LocalizableElement")]
    public partial class TolEdit : RadForm
    {
        private readonly RocheDataEntities _context;
        private readonly Toleranz _currentElement;
        private bool _altAktiv;
        private string _altHigh;
        private string _altWert;
        private Toleranz _element;
        private readonly DbSet<Toleranz> _elements;

        public TolEdit(RocheDataEntities context, Toleranz element)
        {
            InitializeComponent();


            _element = element;
            _context = context;
            _elements = _context.Toleranzs;
            _currentElement = element == null ? new Toleranz() : _elements.Find(element.Nummer);
            LoadElement();
        }


        private void LoadElement()
        {
            tbNummer.Text = _currentElement.Nummer.ToString();
            tbVon.Text = _currentElement.LowLimit.ToString();
            tbBis.Text = _altHigh = _currentElement.HighLimit.ToString();
            tbToleranz.Text = _altWert = _currentElement.Wert.ToString();
            cbActive.Checked = _altAktiv = _currentElement.InUse;
        }

        #region Leave (Lost Focus)

        private void cbActive_CheckStateChanged(object sender, EventArgs e)
        {
            _currentElement.InUse = cbActive.Checked;
        }

        #endregion

        private void btCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void tbBis_Leave(object sender, EventArgs e)
        {
            double result;
            if (double.TryParse(tbBis.Text, out result))
            {
                if (_currentElement.Nummer < 10)
                {
                    var nextElement = _elements.Find(_currentElement.Nummer + 1);
                    if (nextElement.HighLimit < result)
                    {
                        MessageBox.Show(
                            " Der eingegebene 'Bis'-Wert übersteigt den 'Bis'-Wert der nächsten Stufe, bitte korrigieren!",
                            "Limiten", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }

                    if (result <= _currentElement.LowLimit)
                    {
                        MessageBox.Show(
                            " Der eingegebene 'Bis'-Wert ist kleiner als der 'Von'-Wert, bitte korrigieren!", "Limiten",
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }

                    _currentElement.HighLimit = result;
                    nextElement.LowLimit = _currentElement.HighLimit;
                }
            }
            else
            {
                MessageBox.Show("Bitte ein numerischer Wert eingeben!", "Validation", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        #region Buttons

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (CheckEmptyFields())
            {
                MessageBox.Show("Es müssem alle Felder Eingaben enthalten!", "Felder", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                return;
            }

            ManageAuditTail.WriteAuditTrial(AuditKlasse.Tabelle, AuditAction.Aendern,
                $"Tol {_currentElement.Nummer}, High : Alt {_altHigh}", $"Neu {_currentElement.HighLimit}");
            ManageAuditTail.WriteAuditTrial(AuditKlasse.Tabelle, AuditAction.Aendern,
                $"Tol {_currentElement.Nummer}, Wert : Alt {_altWert}", $"Neu {_currentElement.Wert}");
            ManageAuditTail.WriteAuditTrial(AuditKlasse.Tabelle, AuditAction.Aendern,
                $"Tol {_currentElement.Nummer}, Aktiv : Alt {_altAktiv}", $"Neu {_currentElement.InUse}");
            _context.SaveChanges();
            Close();
        }

        private bool CheckEmptyFields()
        {
            return (tbNummer.Text == "" && tbVon.Text == "") || tbBis.Text == "";
        }

        #endregion
    }
}