﻿using System;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using MT.GUPAS.Infrastructure;
using MT.GUPAS.Models;
using Telerik.WinControls.UI;

namespace MT.GUPAS.Pages
{
    public partial class ArtikelForm : RadForm
    {
        private RocheDataEntities rocheDBContext;
        private DbSet<Komp> rocheKomp;

        public ArtikelForm()
        {
            InitializeComponent();
        }

        private void btNew_Click(object sender, EventArgs e)
        {
            var ab = new ArtikelEdit(rocheDBContext, null);
            ab.ShowDialog();
            gridUser.DataSource = null;
            gridUser.DataSource = rocheKomp.ToList();
            gridUser.ClearSelection();
        }

        private void UserForm_Load(object sender, EventArgs e)
        {
            rocheDBContext = new RocheDataEntities();
            rocheKomp = rocheDBContext.Komps;
            bindingSource1.DataSource = rocheKomp.ToList();
            ReloadGridView();
        }

        private void btDelete_Click(object sender, EventArgs e)
        {
            var current = gridUser.CurrentRow.DataBoundItem as Komp;

            if (current != null)
            {
                var result = rocheDBContext.Database.SqlQuery<AKopf>("SELECT * FROM AKOPF WHERE MG = @MG",
                    new SqlParameter("@MG", current.Nummer)).ToList();

                if (result.Count == 0)
                {
                    var res = MessageBox.Show(this, "Wollen Sie diesen Eintrag löschen?", "Löschen",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (res == DialogResult.Yes)
                    {
                        rocheKomp.Remove(current);
                        rocheDBContext.SaveChanges();
                        ManageAuditTail.WriteAuditTrial(AuditKlasse.Tabelle, AuditAction.Loeschen,
                            $"Artikel: {current.Name}", "");
                        ReloadGridView();
                    }
                }
                else
                {
                    MessageBox.Show(this, "Eintrag kann nicht gelöscht werden, da der Artikel schon verwendet wird!",
                        "Löschen", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void btEdit_Click(object sender, EventArgs e)
        {
            var current = gridUser.CurrentRow.DataBoundItem as Komp;

            if (current != null)
            {
                var ab = new ArtikelEdit(rocheDBContext, current);
                ab.ShowDialog();
                ReloadGridView();
            }
        }

        private void ReloadGridView()
        {
            gridUser.DataSource = null;
            gridUser.DataSource = rocheKomp.ToList();
            foreach (GridViewColumn column in gridUser.Columns)
            {
                column.AutoSizeMode = BestFitColumnMode.AllCells;
                column.BestFit();
                column.TextAlignment = ContentAlignment.MiddleLeft;
            }

            gridUser.ClearSelection();
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btPrint_Click(object sender, EventArgs e)
        {
            var current = gridUser.CurrentRow.DataBoundItem as Komp;
            if (current != null)
            {
                var box = new InputBox(current);
                box.ShowDialog();
            }
        }
    }
}