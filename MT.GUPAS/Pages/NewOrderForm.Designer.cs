﻿namespace MT.GUPAS.Pages
{
    partial class cbEtikette
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.ddArtNr = new Telerik.WinControls.UI.RadDropDownList();
            this.radDropDownList2 = new Telerik.WinControls.UI.RadDropDownList();
            this.cbArtikelname = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.cbRooms = new Telerik.WinControls.UI.RadDropDownList();
            this.cbArt = new Telerik.WinControls.UI.RadDropDownList();
            this.cbEti = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.txtClient = new Telerik.WinControls.UI.RadTextBox();
            this.txtANr = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radGroupBox3 = new Telerik.WinControls.UI.RadGroupBox();
            this.btDelete = new System.Windows.Forms.Button();
            this.dgStaffel = new Telerik.WinControls.UI.RadGridView();
            this.btAdd = new System.Windows.Forms.Button();
            this.btReset = new System.Windows.Forms.Button();
            this.txtTotal = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.cbGebindeNr = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.txtMenge = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.txtAnzahl = new Telerik.WinControls.UI.RadTextBox();
            this.cbAutoLot = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.txtLotZG = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.txtLotMG = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.btCancel = new System.Windows.Forms.Button();
            this.btSave = new System.Windows.Forms.Button();
            this.bestellartBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.kompBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.staffelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cbEtiketteBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddArtNr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbArtikelname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbRooms)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbArt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEti)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtANr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).BeginInit();
            this.radGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgStaffel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgStaffel.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGebindeNr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMenge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAnzahl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAutoLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLotZG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLotMG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bestellartBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kompBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.staffelBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEtiketteBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.ddArtNr);
            this.radGroupBox1.Controls.Add(this.radDropDownList2);
            this.radGroupBox1.Controls.Add(this.cbArtikelname);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGroupBox1.HeaderMargin = new System.Windows.Forms.Padding(1);
            this.radGroupBox1.HeaderText = "Muttergebindedaten";
            this.radGroupBox1.Location = new System.Drawing.Point(23, 12);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(354, 196);
            this.radGroupBox1.TabIndex = 0;
            this.radGroupBox1.Text = "Muttergebindedaten";
            // 
            // ddArtNr
            // 
            this.ddArtNr.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.ddArtNr.AutoSizeItems = true;
            this.ddArtNr.CaseSensitive = true;
            this.ddArtNr.DataSource = this.kompBindingSource;
            this.ddArtNr.DisplayMember = "ArtNr";
            this.ddArtNr.DropDownAnimationEnabled = true;
            this.ddArtNr.EnableGestures = false;
            this.ddArtNr.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddArtNr.Location = new System.Drawing.Point(20, 112);
            this.ddArtNr.Name = "ddArtNr";
            this.ddArtNr.NullText = "Artikelnummer";
            this.ddArtNr.Size = new System.Drawing.Size(141, 27);
            this.ddArtNr.TabIndex = 6;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddArtNr.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDown;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddArtNr.GetChildAt(0))).CaseSensitive = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddArtNr.GetChildAt(0))).Font = new System.Drawing.Font("Segoe UI", 12F);
            // 
            // radDropDownList2
            // 
            this.radDropDownList2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.radDropDownList2.AutoSizeItems = true;
            this.radDropDownList2.DataSource = this.kompBindingSource;
            this.radDropDownList2.DisplayMember = "GenesysNr";
            this.radDropDownList2.DropDownAnimationEnabled = true;
            this.radDropDownList2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList2.Location = new System.Drawing.Point(191, 112);
            this.radDropDownList2.Name = "radDropDownList2";
            this.radDropDownList2.NullText = "Genesysnummer";
            this.radDropDownList2.Size = new System.Drawing.Size(141, 27);
            this.radDropDownList2.TabIndex = 7;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList2.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDown;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList2.GetChildAt(0))).Font = new System.Drawing.Font("Segoe UI", 12F);
            // 
            // cbArtikelname
            // 
            this.cbArtikelname.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cbArtikelname.AutoSizeItems = true;
            this.cbArtikelname.DataSource = this.kompBindingSource;
            this.cbArtikelname.DisplayMember = "Name";
            this.cbArtikelname.DropDownAnimationEnabled = true;
            this.cbArtikelname.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbArtikelname.Location = new System.Drawing.Point(20, 44);
            this.cbArtikelname.Name = "cbArtikelname";
            this.cbArtikelname.Size = new System.Drawing.Size(312, 27);
            this.cbArtikelname.TabIndex = 1;
            this.cbArtikelname.ValueMember = "Nummer";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.cbArtikelname.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDown;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.cbArtikelname.GetChildAt(0))).Font = new System.Drawing.Font("Segoe UI", 12F);
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel3.Location = new System.Drawing.Point(191, 81);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(99, 25);
            this.radLabel3.TabIndex = 5;
            this.radLabel3.Text = "Genesys-Nr.:";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.Location = new System.Drawing.Point(20, 81);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(86, 25);
            this.radLabel2.TabIndex = 4;
            this.radLabel2.Text = "Artikel-Nr.:";
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.Location = new System.Drawing.Point(20, 21);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(99, 25);
            this.radLabel1.TabIndex = 3;
            this.radLabel1.Text = "Artikelname:";
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.Controls.Add(this.cbRooms);
            this.radGroupBox2.Controls.Add(this.cbArt);
            this.radGroupBox2.Controls.Add(this.cbEti);
            this.radGroupBox2.Controls.Add(this.radLabel4);
            this.radGroupBox2.Controls.Add(this.txtClient);
            this.radGroupBox2.Controls.Add(this.txtANr);
            this.radGroupBox2.Controls.Add(this.radLabel7);
            this.radGroupBox2.Controls.Add(this.radLabel5);
            this.radGroupBox2.Controls.Add(this.radLabel6);
            this.radGroupBox2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGroupBox2.HeaderMargin = new System.Windows.Forms.Padding(1);
            this.radGroupBox2.HeaderText = "Auftragsdaten";
            this.radGroupBox2.Location = new System.Drawing.Point(414, 12);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Size = new System.Drawing.Size(420, 196);
            this.radGroupBox2.TabIndex = 1;
            this.radGroupBox2.Text = "Auftragsdaten";
            // 
            // cbRooms
            // 
            this.cbRooms.AutoSizeItems = true;
            this.cbRooms.DropDownAnimationEnabled = true;
            this.cbRooms.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cbRooms.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbRooms.Location = new System.Drawing.Point(212, 111);
            this.cbRooms.Name = "cbRooms";
            this.cbRooms.Size = new System.Drawing.Size(170, 27);
            this.cbRooms.TabIndex = 14;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.cbRooms.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.cbRooms.GetChildAt(0))).Font = new System.Drawing.Font("Segoe UI", 12F);
            // 
            // cbArt
            // 
            this.cbArt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbArt.AutoSizeItems = true;
            this.cbArt.DataMember = null;
            this.cbArt.DataSource = this.bestellartBindingSource;
            this.cbArt.DisplayMember = "Wert";
            this.cbArt.DropDownAnimationEnabled = true;
            this.cbArt.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cbArt.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbArt.Location = new System.Drawing.Point(212, 44);
            this.cbArt.Name = "cbArt";
            this.cbArt.Size = new System.Drawing.Size(170, 27);
            this.cbArt.TabIndex = 13;
            this.cbArt.ValueMember = "Nummer";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.cbArt.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.cbArt.GetChildAt(0))).Font = new System.Drawing.Font("Segoe UI", 12F);
            // 
            // cbEti
            // 
            this.cbEti.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbEti.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbEti.Location = new System.Drawing.Point(17, 150);
            this.cbEti.Name = "cbEti";
            this.cbEti.Size = new System.Drawing.Size(242, 25);
            this.cbEti.TabIndex = 12;
            this.cbEti.Text = "Etikette automatisch erzeugen";
            this.cbEti.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel4.Location = new System.Drawing.Point(212, 81);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(61, 25);
            this.radLabel4.TabIndex = 10;
            this.radLabel4.Text = "Kabine:";
            // 
            // txtClient
            // 
            this.txtClient.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtClient.Location = new System.Drawing.Point(17, 111);
            this.txtClient.Name = "txtClient";
            this.txtClient.Size = new System.Drawing.Size(174, 27);
            this.txtClient.TabIndex = 9;
            this.txtClient.Text = "None";
            this.txtClient.MaxLength = 20;
            // 
            // txtANr
            // 
            this.txtANr.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtANr.Location = new System.Drawing.Point(17, 44);
            this.txtANr.Name = "txtANr";
            this.txtANr.Size = new System.Drawing.Size(174, 27);
            this.txtANr.TabIndex = 1;
            // 
            // radLabel7
            // 
            this.radLabel7.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel7.Location = new System.Drawing.Point(17, 21);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(134, 25);
            this.radLabel7.TabIndex = 7;
            this.radLabel7.Text = "Auftragsnummer:";
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel5.Location = new System.Drawing.Point(17, 81);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(108, 25);
            this.radLabel5.TabIndex = 4;
            this.radLabel5.Text = "Auftraggeber:";
            // 
            // radLabel6
            // 
            this.radLabel6.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel6.Location = new System.Drawing.Point(212, 21);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(93, 25);
            this.radLabel6.TabIndex = 3;
            this.radLabel6.Text = "Auftragsart:";
            // 
            // radGroupBox3
            // 
            this.radGroupBox3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox3.Controls.Add(this.btDelete);
            this.radGroupBox3.Controls.Add(this.dgStaffel);
            this.radGroupBox3.Controls.Add(this.btAdd);
            this.radGroupBox3.Controls.Add(this.btReset);
            this.radGroupBox3.Controls.Add(this.txtTotal);
            this.radGroupBox3.Controls.Add(this.radLabel15);
            this.radGroupBox3.Controls.Add(this.cbGebindeNr);
            this.radGroupBox3.Controls.Add(this.radLabel14);
            this.radGroupBox3.Controls.Add(this.txtMenge);
            this.radGroupBox3.Controls.Add(this.radLabel13);
            this.radGroupBox3.Controls.Add(this.txtAnzahl);
            this.radGroupBox3.Controls.Add(this.cbAutoLot);
            this.radGroupBox3.Controls.Add(this.radLabel12);
            this.radGroupBox3.Controls.Add(this.txtLotZG);
            this.radGroupBox3.Controls.Add(this.radLabel11);
            this.radGroupBox3.Controls.Add(this.txtLotMG);
            this.radGroupBox3.Controls.Add(this.radLabel10);
            this.radGroupBox3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGroupBox3.HeaderMargin = new System.Windows.Forms.Padding(1);
            this.radGroupBox3.HeaderText = "Auftragsdaten";
            this.radGroupBox3.Location = new System.Drawing.Point(23, 219);
            this.radGroupBox3.Name = "radGroupBox3";
            this.radGroupBox3.Size = new System.Drawing.Size(811, 315);
            this.radGroupBox3.TabIndex = 2;
            this.radGroupBox3.Text = "Auftragsdaten";
            // 
            // btDelete
            // 
            this.btDelete.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDelete.Location = new System.Drawing.Point(674, 204);
            this.btDelete.Name = "btDelete";
            this.btDelete.Size = new System.Drawing.Size(116, 41);
            this.btDelete.TabIndex = 22;
            this.btDelete.Text = "Löschen";
            this.btDelete.UseVisualStyleBackColor = true;
            this.btDelete.Click += new System.EventHandler(this.btDelete_Click);
            // 
            // dgStaffel
            // 
            this.dgStaffel.Location = new System.Drawing.Point(17, 49);
            // 
            // 
            // 
            this.dgStaffel.MasterTemplate.AllowAddNewRow = false;
            this.dgStaffel.MasterTemplate.AllowColumnReorder = false;
            this.dgStaffel.MasterTemplate.AllowDeleteRow = false;
            this.dgStaffel.MasterTemplate.AllowDragToGroup = false;
            this.dgStaffel.MasterTemplate.ShowRowHeaderColumn = false;
            this.dgStaffel.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.dgStaffel.Name = "dgStaffel";
            this.dgStaffel.ReadOnly = true;
            this.dgStaffel.Size = new System.Drawing.Size(370, 252);
            this.dgStaffel.TabIndex = 24;
            // 
            // btAdd
            // 
            this.btAdd.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAdd.Location = new System.Drawing.Point(674, 144);
            this.btAdd.Name = "btAdd";
            this.btAdd.Size = new System.Drawing.Size(116, 43);
            this.btAdd.TabIndex = 23;
            this.btAdd.Text = "Hinzufügen";
            this.btAdd.UseVisualStyleBackColor = true;
            this.btAdd.Click += new System.EventHandler(this.btAdd_Click);
            // 
            // btReset
            // 
            this.btReset.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btReset.Location = new System.Drawing.Point(674, 262);
            this.btReset.Name = "btReset";
            this.btReset.Size = new System.Drawing.Size(116, 43);
            this.btReset.TabIndex = 21;
            this.btReset.Text = "Reset";
            this.btReset.UseVisualStyleBackColor = true;
            this.btReset.Click += new System.EventHandler(this.btReset_Click);
            // 
            // txtTotal
            // 
            this.txtTotal.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotal.Location = new System.Drawing.Point(408, 274);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(174, 27);
            this.txtTotal.TabIndex = 19;
            this.txtTotal.Text = "0";
            // 
            // radLabel15
            // 
            this.radLabel15.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel15.Location = new System.Drawing.Point(408, 247);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(146, 25);
            this.radLabel15.TabIndex = 20;
            this.radLabel15.Text = "Gesamtmenge [kg]";
            // 
            // cbGebindeNr
            // 
            this.cbGebindeNr.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbGebindeNr.Location = new System.Drawing.Point(408, 208);
            this.cbGebindeNr.Name = "cbGebindeNr";
            this.cbGebindeNr.Size = new System.Drawing.Size(259, 25);
            this.cbGebindeNr.TabIndex = 18;
            this.cbGebindeNr.Text = "Keine MG Gebinde-Nr. eingeben";
            // 
            // radLabel14
            // 
            this.radLabel14.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel14.Location = new System.Drawing.Point(408, 138);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(181, 25);
            this.radLabel14.TabIndex = 17;
            this.radLabel14.Text = "Menge Zielgebinde [kg]";
            // 
            // txtMenge
            // 
            this.txtMenge.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMenge.Location = new System.Drawing.Point(408, 169);
            this.txtMenge.Name = "txtMenge";
            this.txtMenge.Size = new System.Drawing.Size(174, 27);
            this.txtMenge.TabIndex = 16;
            this.txtMenge.Text = "0.0";
            // 
            // radLabel13
            // 
            this.radLabel13.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel13.Location = new System.Drawing.Point(408, 74);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(147, 25);
            this.radLabel13.TabIndex = 15;
            this.radLabel13.Text = "Anzahl Zielgebinde";
            // 
            // txtAnzahl
            // 
            this.txtAnzahl.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAnzahl.Location = new System.Drawing.Point(408, 101);
            this.txtAnzahl.Name = "txtAnzahl";
            this.txtAnzahl.Size = new System.Drawing.Size(174, 27);
            this.txtAnzahl.TabIndex = 14;
            this.txtAnzahl.Text = "0";
            // 
            // cbAutoLot
            // 
            this.cbAutoLot.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAutoLot.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbAutoLot.Location = new System.Drawing.Point(603, 74);
            this.cbAutoLot.Name = "cbAutoLot";
            this.cbAutoLot.Size = new System.Drawing.Size(140, 25);
            this.cbAutoLot.TabIndex = 13;
            this.cbAutoLot.Text = "Lot-Nr. von MG ";
            this.cbAutoLot.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radLabel12
            // 
            this.radLabel12.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel12.Location = new System.Drawing.Point(599, 14);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(186, 25);
            this.radLabel12.TabIndex = 12;
            this.radLabel12.Text = "Lot-Nr. des Zielgebindes";
            // 
            // txtLotZG
            // 
            this.txtLotZG.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLotZG.Location = new System.Drawing.Point(599, 41);
            this.txtLotZG.Name = "txtLotZG";
            this.txtLotZG.Size = new System.Drawing.Size(191, 27);
            this.txtLotZG.TabIndex = 11;
            // 
            // radLabel11
            // 
            this.radLabel11.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel11.Location = new System.Drawing.Point(408, 14);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(119, 25);
            this.radLabel11.TabIndex = 10;
            this.radLabel11.Text = "Lot-Nr. des MG";
            // 
            // txtLotMG
            // 
            this.txtLotMG.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLotMG.Location = new System.Drawing.Point(408, 41);
            this.txtLotMG.Name = "txtLotMG";
            this.txtLotMG.Size = new System.Drawing.Size(174, 27);
            this.txtLotMG.TabIndex = 9;
            // 
            // radLabel10
            // 
            this.radLabel10.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel10.Location = new System.Drawing.Point(17, 21);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(85, 25);
            this.radLabel10.TabIndex = 3;
            this.radLabel10.Text = "Staffelung:";
            // 
            // btCancel
            // 
            this.btCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCancel.Location = new System.Drawing.Point(697, 543);
            this.btCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(133, 37);
            this.btCancel.TabIndex = 23;
            this.btCancel.Text = "Abbrechen";
            this.btCancel.UseVisualStyleBackColor = true;
            this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
            // 
            // btSave
            // 
            this.btSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSave.Location = new System.Drawing.Point(514, 543);
            this.btSave.Margin = new System.Windows.Forms.Padding(4);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(133, 37);
            this.btSave.TabIndex = 24;
            this.btSave.Text = "Speichern";
            this.btSave.UseVisualStyleBackColor = true;
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // bestellartBindingSource
            // 
            this.bestellartBindingSource.DataSource = typeof(MT.GUPAS.Models.Bestellart);
            // 
            // kompBindingSource
            // 
            this.kompBindingSource.DataSource = typeof(MT.GUPAS.Models.Komp);
            // 
            // staffelBindingSource
            // 
            this.staffelBindingSource.DataSource = typeof(MT.GUPAS.Models.Staffel);
            // 
            // cbEtiketteBindingSource
            // 
            this.cbEtiketteBindingSource.DataSource = typeof(MT.GUPAS.Pages.cbEtikette);
            // 
            // cbEtikette
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(872, 596);
            this.Controls.Add(this.btSave);
            this.Controls.Add(this.btCancel);
            this.Controls.Add(this.radGroupBox3);
            this.Controls.Add(this.radGroupBox2);
            this.Controls.Add(this.radGroupBox1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(880, 626);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(880, 626);
            this.Name = "cbEtikette";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.RootElement.MaxSize = new System.Drawing.Size(880, 626);
            this.Text = "Neuer Auftrag";
            this.Load += new System.EventHandler(this.NewOrderForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddArtNr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbArtikelname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            this.radGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbRooms)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbArt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEti)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtANr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).EndInit();
            this.radGroupBox3.ResumeLayout(false);
            this.radGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgStaffel.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgStaffel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGebindeNr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMenge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAnzahl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAutoLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLotZG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLotMG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bestellartBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kompBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.staffelBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbEtiketteBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private System.Windows.Forms.BindingSource kompBindingSource;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadTextBox txtClient;
        private Telerik.WinControls.UI.RadTextBox txtANr;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private System.Windows.Forms.BindingSource bestellartBindingSource;
        private Telerik.WinControls.UI.RadCheckBox cbEti;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox3;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadTextBox txtTotal;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        private Telerik.WinControls.UI.RadCheckBox cbGebindeNr;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadTextBox txtMenge;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadTextBox txtAnzahl;
        private Telerik.WinControls.UI.RadCheckBox cbAutoLot;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadTextBox txtLotZG;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadTextBox txtLotMG;
        private System.Windows.Forms.Button btAdd;
        private System.Windows.Forms.Button btDelete;
        private System.Windows.Forms.Button btReset;
        private System.Windows.Forms.Button btCancel;
        private System.Windows.Forms.Button btSave;
        private System.Windows.Forms.BindingSource staffelBindingSource;
        private Telerik.WinControls.UI.RadGridView dgStaffel;
        private Telerik.WinControls.UI.RadDropDownList cbArtikelname;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList2;
        private Telerik.WinControls.UI.RadDropDownList ddArtNr;
        private Telerik.WinControls.UI.RadDropDownList cbRooms;
        private Telerik.WinControls.UI.RadDropDownList cbArt;
        private System.Windows.Forms.BindingSource cbEtiketteBindingSource;
    }
}
