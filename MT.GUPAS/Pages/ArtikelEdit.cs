﻿using System;
using System.Data.Entity;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Windows.Forms;
using MT.GUPAS.Infrastructure;
using MT.GUPAS.Models;
using Telerik.WinControls.UI;

namespace MT.GUPAS.Pages
{
    [SuppressMessage("ReSharper", "SpecifyACultureInStringConversionExplicitly")]
    [SuppressMessage("ReSharper", "LocalizableElement")]
    public partial class ArtikelEdit : RadForm
    {
        private readonly RocheDataEntities _context;
        private readonly Komp _currentElement;
        private readonly Komp _element;
        private readonly DbSet<Komp> _elements;
        private string _wertAlt;

        public ArtikelEdit(RocheDataEntities context, Komp element)
        {
            InitializeComponent();


            _element = element;
            _context = context;
            _elements = _context.Komps;
            _currentElement = element == null ? new Komp() : _elements.Find(element.Nummer);
            LoadElement();
        }


        private void LoadElement()
        {
            tbArtNr.Text = _currentElement.ArtNr.Trim();
            tbGenesysNr.Text = _currentElement.GenesysNr.Trim();
            tbName.Text = _wertAlt = _currentElement.Name.Trim();
            cbActive.Checked = _currentElement.Active == 1;
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        #region Buttons

        private void btnSave_Click(object sender, EventArgs e)
        {
            decimal result;

            if (CheckEmptyFields())
            {
                MessageBox.Show("Es müssem alle Felder Eingaben enthalten!", "Felder", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                return;
            }

            if (_element == null &&
                _elements.ToList().Find(d => d.Name.ToUpper().Trim() == tbName.Text.ToUpper().Trim()) != null)
            {
                MessageBox.Show("Artikel bereits vorhanden!", "Artikel", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                return;
            }


            if (!decimal.TryParse(tbArtNr.Text.RemoveWhitespace().Trim(), out result))
            {
                MessageBox.Show("Artikel-Nr. muss numerisch sein!", "Artikel", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                return;
            }

            if (!decimal.TryParse(tbGenesysNr.Text.RemoveWhitespace().Trim(), out result))
            {
                MessageBox.Show("Genesys-Nr. muss numerisch sein!", "Artikel", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                return;
            }

            if (_element == null)
            {
                _elements.Add(_currentElement);
                ManageAuditTail.WriteAuditTrial(AuditKlasse.Tabelle, AuditAction.Erstellen, "Artikel: Alt N/A",
                    $"Neu {_currentElement.Name}");
            }
            else
            {
                ManageAuditTail.WriteAuditTrial(AuditKlasse.Tabelle, AuditAction.Aendern, $"Artikel: Alt {_wertAlt}",
                    $"Neu {_currentElement.Name}");
            }

            _context.SaveChanges();
            Close();
        }

        private bool CheckEmptyFields()
        {
            return (tbArtNr.Text == "" && tbGenesysNr.Text == "") || tbName.Text == "";
        }

        #endregion

        #region Leave (Lost Focus)

        private void tbArtNr_TextChanged(object sender, EventArgs e)
        {
            _currentElement.ArtNr = tbArtNr.Text.Trim();
        }

        private void tbGenesysNr_TextChanged(object sender, EventArgs e)
        {
            _currentElement.GenesysNr = tbGenesysNr.Text.Trim();
        }

        private void tbName_TextChanged(object sender, EventArgs e)
        {
            _currentElement.Name = tbName.Text.ToUpper();
        }

        private void cbActive_CheckStateChanged(object sender, EventArgs e)
        {
            _currentElement.Active = cbActive.Checked ? 1 : 0;
        }

        #endregion
    }
}