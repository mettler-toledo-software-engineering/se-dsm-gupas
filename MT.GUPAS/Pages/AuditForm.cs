﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using MT.GUPAS.Models;
using System.Linq;
using System.Data.Entity;
using Telerik.WinControls.UI;
using MT.GUPAS.Reporting;
using Telerik.Reporting;
using MT.GUPAS.Infrastructure;

namespace MT.GUPAS.Pages
{
    public partial class AuditForm : Telerik.WinControls.UI.RadForm
    {
        private RocheDataEntities rocheDBContext;
        private List<AuditTrail> rocheAudit;
        private string aktPerson;
        private string dtFilter;
        public AuditForm()
        {
            InitializeComponent();

        }

        private void UserForm_Load(object sender, EventArgs e)
        {
            List<User> user = new List<User>();
            rocheDBContext = new RocheDataEntities();
            aktPerson = "Alle";
            dtFilter = DateTime.Now.Date.ToString("dd.MM.yyyy");
            user = rocheDBContext.Users.OrderBy(d=> d.Name).ToList();
            user.Insert(0, new User() { Name = "Alle" });
            dpPerson.DisplayMember = "Name";
            dpPerson.DataSource = user;
            ReloadGridView();
        }

        private void ReloadGridView()
        {
            DateTime von = Convert.ToDateTime(dtFilter);
            DateTime bis = Convert.ToDateTime(dtFilter + " 23:59:59");

            gridUser.DataSource = null;
            if (aktPerson == "Alle")
                rocheAudit = rocheDBContext.AuditTrails.
               Where(d => (d.Zeit > von) && (d.Zeit < bis)).ToList();
            else
                rocheAudit = rocheDBContext.AuditTrails.
                    Where(d => (d.Ersteller.Trim() == aktPerson.Trim()) &&
                         (d.Zeit > von) && (d.Zeit < bis)).ToList();

            gridUser.DataSource = rocheAudit;

            foreach (GridViewColumn column in gridUser.Columns)
            {
                column.AutoSizeMode = BestFitColumnMode.AllCells;
                column.BestFit();
                column.TextAlignment = ContentAlignment.MiddleLeft;
            }
            gridUser.ClearSelection();
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dpPerson_SelectedValueChanged(object sender, EventArgs e)
        {

            if (dpPerson.SelectedText != null)
                aktPerson = dpPerson.SelectedText.Trim();
            ReloadGridView();
        }

        private void dtWann_Leave(object sender, EventArgs e)
        {
            dtFilter = dtWann.Value.Date.ToString("dd.MM.yyyy");
            ReloadGridView();
        }

        private void tbPrint_Click(object sender, EventArgs e)
        {
            InstanceReportSource source = new InstanceReportSource();
            WAudit report = new WAudit();

            report.ReportParameters["Ersteller"].Value = Globals.User.Name.Trim();
            report.DataSource = rocheAudit;
            source.ReportDocument = report;

            var hugo = new ReportViewer(source);
            hugo.Text = "Audit Trail";
            hugo.ShowDialog();
            ManageAuditTail.WriteAuditTrial(AuditKlasse.Protokoll, AuditAction.Ausdrucken, $"Audit-Trail", "");
        }
    }
}



