﻿using System;
using System.Linq;
using System.Windows.Forms;
using MT.GUPAS.Infrastructure;
using MT.GUPAS.Models;
using Telerik.WinControls.UI;

namespace MT.GUPAS.Pages
{
    public partial class SperrForm : RadForm
    {
        private SystemM _element;

        private RocheDataEntities _rocheDBContext;
        private string _wertAlt;

        public SperrForm()
        {
            InitializeComponent();
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void SperrForm_Load(object sender, EventArgs e)
        {
            _rocheDBContext = new RocheDataEntities();
            _element = _rocheDBContext.SystemMs.First();
            tbEntry.Text = _wertAlt = (_element.Sperrzeit / 12).ToString();
        }

        private void btOk_Click(object sender, EventArgs e)
        {
            int result;

            if (int.TryParse(tbEntry.Text, out result))
            {
                _element.Sperrzeit = result * 12;
                Globals.LogoutTime = Convert.ToUInt32(_element.Sperrzeit * 5000);
                _rocheDBContext.SaveChanges();
                ManageAuditTail.WriteAuditTrial(AuditKlasse.Tabelle, AuditAction.Aendern, $"Sperrzeit: Alt {_wertAlt}",
                    $"Neu {tbEntry.Text}");
                Close();
            }
            else
            {
                MessageBox.Show("Eingabewert unzulässig!", "Eingabefehler", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
            }
        }
    }
}