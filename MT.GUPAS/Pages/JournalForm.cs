﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using MT.GUPAS.Infrastructure;
using MT.GUPAS.Models;
using MT.GUPAS.Reporting;
using Telerik.Reporting;
using Telerik.WinControls.UI;

namespace MT.GUPAS.Pages
{
    public partial class JournalForm : RadForm
    {
        private readonly string mySqlQuery =
            @"SELECT ADetail.Zeit, Komp.ArtNr,Komp.Name AS Artikelname, ADetail.Brutto, ADetail.Netto, ADetail.Tara,ALot.Lot, AKopf.ANr, AKopf.BArt,[User].Name AS Abfueller,Kabine.Name, 'kg' as Einheit, Waagen.Unit
                            FROM  Waagen Waagen 
                            INNER JOIN  Kabine Kabine 
                            INNER JOIN  Komp Komp 
                            INNER JOIN  ALot ALot 
                            INNER JOIN  [User] [User] 
                            INNER JOIN ADetail ADetail ON [User].Nummer = ADetail.Verwieger ON ALot.Nummer = ADetail.Lot 
                            INNER JOIN AKopf AKopf ON ALot.Auftrag = AKopf.Nummer ON Komp.Nummer = AKopf.MG ON Kabine.Nummer = ADetail.Kabine ON Waagen.Nummer = ADetail.Waage
                            Where (Zeit > @sqlVon AND Zeit < @sqlBis)
                            ORDER BY ADetail.Zeit";

        private RocheDataEntities rocheDBContext;
        private List<JournalModel> rocheJournal;


        public JournalForm()
        {
            InitializeComponent();
        }

        private void UserForm_Load(object sender, EventArgs e)
        {
            rocheDBContext = new RocheDataEntities();

            dtVon.Value = DateTime.Now.AddDays(-1);
            dtBis.Value = DateTime.Now.Date;

            ReloadGridView();
        }

        private void ReloadGridView()
        {
            var sqlVon = dtVon.Value.ToString("yyyy-MM-dd 00:00:00.000");
            var sqlBis = dtBis.Value.ToString("yyyy-MM-dd 23:59:59.999");

            gridUser.DataSource = null;

            rocheJournal = rocheDBContext.Database.SqlQuery<JournalModel>(mySqlQuery,
                new SqlParameter("@sqlVon", sqlVon), new SqlParameter("@sqlbis", sqlBis)).ToList();
            gridUser.DataSource = rocheJournal;

            foreach (GridViewColumn column in gridUser.Columns)
            {

                column.AutoSizeMode = BestFitColumnMode.AllCells;
                column.BestFit();
                column.TextAlignment = ContentAlignment.MiddleLeft;
            }

            gridUser.ClearSelection();
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            Close();
        }


        private void tbPrint_Click(object sender, EventArgs e)
        {
            var source = new InstanceReportSource();
            var report = new WJournal();

            report.DataSource = rocheJournal;
            source.ReportDocument = report;

            var hugo = new ReportViewer(source);
            //hugo.Pa = report.PageSettings.PaperSize.Width.Value
            hugo.Text = "Journal";
            hugo.ShowDialog();
            ManageAuditTail.WriteAuditTrial(AuditKlasse.Protokoll, AuditAction.Ausdrucken, "Journal", "");
        }

        private void dtVon_Leave(object sender, EventArgs e)
        {
            ReloadGridView();
        }

        private void dtBis_Leave(object sender, EventArgs e)
        {
            ReloadGridView();
        }
    }
}