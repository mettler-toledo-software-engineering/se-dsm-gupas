﻿using System;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using MT.GUPAS.Models;
using Telerik.WinControls.UI;

namespace MT.GUPAS.Pages
{
    public partial class TolForm : RadForm
    {
        private RocheDataEntities rocheDBContext;
        private DbSet<Toleranz> rocheTol;

        public TolForm()
        {
            InitializeComponent();
        }


        private void UserForm_Load(object sender, EventArgs e)
        {
            rocheDBContext = new RocheDataEntities();
            rocheTol = rocheDBContext.Toleranzs;
            bindingSource1.DataSource = rocheTol.ToList();
            ReloadGridView();
        }


        private void btEdit_Click(object sender, EventArgs e)
        {
            var current = gridUser.CurrentRow.DataBoundItem as Toleranz;

            if (current != null)
            {
                var ab = new TolEdit(rocheDBContext, current);
                ab.ShowDialog();
                ReloadGridView();
            }
        }

        private void ReloadGridView()
        {
            gridUser.DataSource = null;
            gridUser.DataSource = rocheTol.ToList();
            foreach (GridViewColumn column in gridUser.Columns)
            {
                column.AutoSizeMode = BestFitColumnMode.AllCells;
                column.BestFit();
                column.TextAlignment = ContentAlignment.MiddleLeft;
            }

            gridUser.ClearSelection();
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}